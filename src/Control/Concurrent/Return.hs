{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | Give another thread a way to send a value back to us.
module Control.Concurrent.Return
    ( newReturn
    )
where

import Control.Concurrent.MVar
import Control.Monad

-- | Produce a pair of IO actions:
--
-- 1. Setter to give another thread, where it would be called at most once to
--    send us a value
-- 2. Action that waits until the value arrives
newReturn :: IO (a -> IO (), IO a)
newReturn = do
    mvar <- newEmptyMVar
    return (putReturn mvar, readMVar mvar)
    where
    putReturn mvar val = do
        success <- tryPutMVar mvar val
        unless success $ error "newReturn: putReturn: MVar is full"
