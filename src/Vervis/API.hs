{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}

module Vervis.API
    ( acceptC
    --, addBundleC
    , applyC
    --, noteC
    , createNoteC
    , createPatchTrackerC
    , createRepositoryC
    , createTicketTrackerC
    , followC
    , inviteC
    , offerTicketC
    --, offerDepC
    , resolveC
    , undoC
    )
where

import Control.Applicative
import Control.Exception hiding (Handler, try)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Align
import Data.Barbie
import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable
import Data.Foldable
import Data.Functor
import Data.Functor.Identity
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.These
import Data.Time.Clock
import Data.Traversable
import Database.Persist hiding (deleteBy)
import Database.Persist.Sql hiding (deleteBy)
import Network.HTTP.Client
import System.Directory
import System.Exit
import System.FilePath
import System.IO.Temp
import System.Process.Typed
import Text.Blaze.Html.Renderer.Text
import Yesod.Core hiding (logError, logWarn, logInfo, logDebug)
import Yesod.Persist.Core

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Lazy as TL

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Patch (..), Ticket, Follow, Repo (..), ActorLocal (..), ActorDetail (..), Actor (..))
import Web.Text
import Yesod.ActivityPub
import Yesod.Actor
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import qualified Data.Git.Local as G (createRepo)
import qualified Data.Text.UTF8.Local as TU
import qualified Darcs.Local.Repository as D (createRepo)

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Cloth
import Vervis.Darcs
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Follow
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Fetch
import Vervis.Foundation
import Vervis.Git
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.Workflow
import Vervis.Model.Ticket
import Vervis.Path
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Persist.Follow
import Vervis.Persist.Ticket
import Vervis.Recipient
import Vervis.RemoteActorStore
import Vervis.Settings
import Vervis.Query
import Vervis.Ticket
import Vervis.Web.Delivery
import Vervis.Web.Repo

verifyResourceAddressed
    :: (MonadSite m, YesodHashids (SiteEnv m))
    => RecipientRoutes -> GrantResourceBy Key -> ExceptT Text m ()
verifyResourceAddressed localRecips resource = do
    resourceHash <- hashGrantResource resource
    fromMaybeE (verify resourceHash) "Local resource not addressed"
    where
    verify (GrantResourceRepo r) = do
        routes <- lookup r $ recipRepos localRecips
        guard $ routeRepo routes
    verify (GrantResourceDeck d) = do
        routes <- lookup d $ recipDecks localRecips
        guard $ routeDeck $ familyDeck routes
    verify (GrantResourceLoom l) = do
        routes <- lookup l $ recipLooms localRecips
        guard $ routeLoom $ familyLoom routes

verifyRemoteAddressed
    :: Monad m => [(Host, NonEmpty LocalURI)] -> FedURI -> ExceptT Text m ()
verifyRemoteAddressed remoteRecips u =
    fromMaybeE (verify u) "Given remote entity not addressed"
    where
    verify (ObjURI h lu) = do
        lus <- lookup h remoteRecips
        guard $ lu `elem` lus

acceptC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Accept URIMode
    -> ExceptT Text Handler OutboxItemId
acceptC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action accept = do
    error "acceptC temporarily disabled due to actor refactoring"
{-
    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    acceptee <- parseAccept accept

    now <- liftIO getCurrentTime
    senderHash <- encodeKeyHashid senderPersonID

    (obiidAccept, deliverHttpAccept, deliverHttpGrant) <- runDBExcept $ do

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite to a local resource
        maybeCollab <-
            case accepteeDB of
                Left (actorByKey, actorEntity, itemID) -> do
                    maybeSender <-
                        lift $ getValBy $ UniqueCollabInviterLocalInvite itemID
                    return $
                        (,Left (actorByKey, actorEntity)) . collabInviterLocalCollab <$> maybeSender
                Right remoteActivityID -> do
                    maybeSender <-
                        lift $ getValBy $ UniqueCollabInviterRemoteInvite remoteActivityID
                    for maybeSender $ \ (CollabInviterRemote collab actorID _) -> do
                        actor <- lift $ getJust actorID
                        lift $
                            (collab,) . Right . (,remoteActorFollowers actor) <$>
                                getRemoteActorURI actor

        maybeCollabMore <- for maybeCollab $ \ (fulfillsID, collabSender) -> do

            -- Verify that Accept sender is the Collab recipient
            CollabFulfillsInvite collabID <- lift $ getJust fulfillsID
            recip <-
                lift $
                requireEitherAlt
                    (getBy $ UniqueCollabRecipLocal collabID)
                    (getBy $ UniqueCollabRecipRemote collabID)
                    "Found Collab with no recip"
                    "Found Collab with multiple recips"
            recipID <-
                case recip of
                    Left (Entity crlid crl)
                        | collabRecipLocalPerson crl == senderPersonID -> return crlid
                    _ -> throwE "Accepting an Invite whose recipient is someone else"

            -- Verify the Collab isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            verifyNothingE maybeEnabled "Collab already enabled by the local topic"

            -- Verify that Grant sender and resource are addressed by the Accept
            topic <- lift $ getCollabTopic collabID
            verifyResourceAddressed localRecips topic
            bitraverse_
                (verifySenderAddressed localRecips . fst)
                (verifyRemoteAddressed remoteRecips . fst)
                collabSender

            return (collabID, fulfillsID, recipID, topic, collabSender)

        -- Record the Accept on the Collab
        acceptID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        for_ maybeCollabMore $ \ (_, fulfillsID, recipID, _, _) -> do
            maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
            unless (isNothing maybeAccept) $ do
                lift $ delete acceptID
                throwE "This Collab already has an Accept by recip"

        -- Insert the Accept activity to author's outbox
        _luAccept <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) acceptID action

        -- Deliver the Accept activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpAccept <- do
            sieve <- do
                let maybeTopicActor = (\ (_, _, _, t, _) -> t) <$> maybeCollabMore
                    maybeCollabSender = (\ (_, _, _, _, s) -> s) <$> maybeCollabMore
                maybeTopicHash <- traverse hashGrantResource maybeTopicActor
                maybeSenderHash <-
                    case maybeCollabSender of
                        Just (Left (actor, _)) -> Just <$> hashLocalActor actor
                        _ -> pure Nothing
                let sieveActors = catMaybes
                        [ grantResourceLocalActor <$> maybeTopicHash
                        , maybeSenderHash
                        ]
                    sieveStages = catMaybes
                        [ Just $ LocalStagePersonFollowers senderHash
                        , localActorFollowers . grantResourceLocalActor <$> maybeTopicHash
                        , localActorFollowers <$> maybeSenderHash
                        ]
                return $ makeRecipientSet sieveActors sieveStages
            let localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts acceptID action

        -- If resource is local, approve the Collab and deliver a Grant
        deliverHttpGrant <- for maybeCollabMore $ \ (collabID, _, _, resource, sender) -> do

            -- If resource is local, verify it has received the Accept
            resourceByEntity <- getGrantResource resource "getGrantResource"
            let resourceActorID =
                    grantResourceActorID $ bmap (Identity . entityVal) resourceByEntity
            verifyActorHasItem resourceActorID acceptID "Local topic didn't receive the Accept"

            -- If Collab sender is local, verify it has received the Accept
            case sender of
                Left (_, Entity actorID _) ->
                    verifyActorHasItem actorID acceptID "Local Collab sender didn't receive the Accept"
                Right _ -> pure ()

            -- Approve the Collab in the DB
            resourceOutbox <-
                lift $ actorOutbox <$> getJust resourceActorID
            grantID <- lift $ insertEmptyOutboxItem resourceOutbox now
            lift $ insert_ $ CollabEnable collabID grantID

            -- Insert the Grant to resource's outbox
            (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant) <-
                lift . lift $ prepareGrant senderHash sender resource
            _luGrant <- lift $ updateOutboxItem (grantResourceLocalActor resource) grantID actionGrant

            -- Deliver the Grant to local recipients, and schedule delivery
            -- for unavailable remote recipients
            resourceHash <-
                grantResourceLocalActor <$> hashGrantResource resource
            deliverActivityDB
                resourceHash resourceActorID localRecipsGrant remoteRecipsGrant
                fwdHostsGrant grantID actionGrant

        -- Return instructions for HTTP delivery to remote recipients
        return
            ( acceptID
            , deliverHttpAccept
            , deliverHttpGrant
            )

    -- Launch asynchronous HTTP delivery of Accept and Grant
    lift $ do
        forkWorker "acceptC: async HTTP Accept delivery" deliverHttpAccept
        for_ deliverHttpGrant $
            forkWorker "acceptC: async HTTP Grant delivery"

    return obiidAccept

    where

    verifySenderAddressed localRecips actor = do
        actorByHash <- hashLocalActor actor
        unless (actorIsAddressed localRecips actorByHash) $
            throwE "Collab sender not addressed"

    prepareGrant
        :: KeyHashid Person
        -> Either (LocalActorBy Key, Entity Actor) (FedURI, Maybe LocalURI)
        -> GrantResourceBy Key
        -> Handler
            ( AP.Action URIMode
            , RecipientRoutes
            , [(Host, NonEmpty LocalURI)]
            , [Host]
            )
    prepareGrant recipHash sender topic = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        topicHash <-
            grantResourceLocalActor <$> hashGrantResource topic
        senderHash <- bitraverse (hashLocalActor . fst) pure sender

        let audSender =
                case senderHash of
                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                    Right (ObjURI h lu, followers) ->
                        AudRemote h [lu] (maybeToList followers)
            audRecip =
                AudLocal [LocalActorPerson recipHash] [LocalStagePersonFollowers recipHash]
            audTopic =
                AudLocal [] [localActorFollowers topicHash]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audRecip, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = Action
                { actionCapability = Nothing
                , actionSummary    = Nothing
                , actionAudience   = Audience recips [] [] [] [] []
                , actionFulfills   = [AP.acceptObject accept]
                , actionSpecific   = GrantActivity Grant
                    { grantObject  = Left RoleAdmin
                    , grantContext = encodeRouteLocal $ renderLocalActor topicHash
                    , grantTarget  = encodeRouteHome $ PersonR recipHash
                    , grantResult  = Nothing
                    , grantStart   = Nothing
                    , grantEnd     = Nothing
                    , grantAllows  = Invoke
                    , grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)
-}

addBundleC
    :: Entity Person
    -> Maybe HTML
    -> Audience URIMode
    -> NonEmpty (AP.Patch URIMode)
    -> FedURI
    -> ExceptT Text Handler OutboxItemId
addBundleC (Entity pidUser personUser) summary audience patches uTarget = do
    error "addBundleC temporarily disabled"


{-

    ticket <- do
        t <- parseWorkItem "Target" uTarget
        bitraverse
            (\ wi ->
                case wi of
                    WorkItemTicket _ _ -> throwE "Target is a deck ticket"
                    WorkItemCloth loom cloth -> return (loom, cloth)
            )
            pure
            t
    (typ, diffs) <- do
        ((typ, diff) :| rest) <-
            for patches $ \ (AP.Patch mlocal attrib mpub typ content) -> do
                verifyNothingE mlocal "Patch with 'id'"
                attribHash <- do
                    route <- fromMaybeE (decodeRouteLocal attrib) "Patch attrib not a valid route"
                    case route of
                        PersonR person -> return person
                        _ -> throwE "Patch attrib not a person route"
                userHash <- encodeKeyHashid pidUser
                unless (attribHash == userHash) $
                    throwE "Add and Patch attrib mismatch"
                verifyNothingE mpub "Patch has 'published'"
                return (typ, content)
        let (typs, diffs) = unzip rest
        unless (all (== typ) typs) $ throwE "Patches of different media types"
        return (typ, diff :| diffs)
    ParsedAudience localRecips remoteRecips blinded fwdHosts <- do
        mrecips <- parseAudience audience
        fromMaybeE mrecips "Add Bundle with no recipients"
    federation <- asksSite $ appFederation . appSettings
    unless (federation || null remoteRecips) $
        throwE "Federation disabled, but remote recipients specified"
    verifyHosterRecip localRecips "Ticket" ticket
    now <- liftIO getCurrentTime
    authorHash <- encodeKeyHashid pidUser

    (obiidAdd, docAdd, remotesHttpAdd, maybeAccept) <- runDBExcept $ do
        (obiid, doc, luAdd) <- lift $ insertAddToOutbox now blinded
        remotesHttpAdd <- do
            sieve <- do
                (clothA, clothS) <- clothRecipSieve ticket
                return $
                    makeRecipientSet
                        clothA
                        (LocalStagePersonFollowers authorHash : clothS)
            moreRemoteRecips <-
                lift $
                    deliverLocal'
                        True
                        (LocalActorPerson authorHash)
                        (personInbox personUser)
                        obiid
                        (localRecipSieve sieve False localRecips)
            unless (federation || null moreRemoteRecips) $
                throwE "Federation disabled, but recipient collection remote members found"
            lift $ deliverRemoteDB fwdHosts obiid remoteRecips moreRemoteRecips
        maccept <-
            case ticket of
                Right _ -> return Nothing
                Left (loomID, clothID) -> Just <$> do
                    loom <- do
                        maybeLoom <- lift $ get loomID
                        fromMaybeE maybeLoom "No such loom"
                    _ <- do
                        maybeCloth <- lift $ get clothID
                        fromMaybeE maybeCloth "No such cloth"

                    repo <- lift $ getJust $ loomRepo loom
                    unless (repoVcs repo == patchMediaTypeVCS typ) $
                        lift $ throwE "Patch type and repo VCS mismatch"
                    actorLoom <- lift $ getJust $ loomActor loom
                    acceptID <- lift $ insertEmptyOutboxItem (actorOutbox actorLoom) now

                    bundleID <- lift $ insert $ Bundle clothID
                    lift $ insertMany_ $ NE.toList $ NE.map (Patch bnid now typ) diffs

                    (docAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept) <-
                        lift $ insertAccept loomID (actorOutbox actorLoom) clothID obiid acceptID bundleID
                    knownRemoteRecipsAccept <-
                        lift $
                        deliverLocal'
                            False
                            (LocalActorLoom loomHash)
                            (actorInbox actorLoom)
                            acceptID
                            localRecipsAccept
                    lift $ (acceptID,docAccept,fwdHostsAccept,) <$>
                        deliverRemoteDB fwdHostsAccept acceptID remoteRecipsAccept knownRemoteRecipsAccept
        return (obiid, doc, remotesHttpAdd, maccept)
    lift $ do
        forkWorker "addBundleC: async HTTP Offer delivery" $
            deliverRemoteHttp' fwdHosts obiidAdd docAdd remotesHttpAdd
        for_ maybeAccept $ \ (obiidAccept, docAccept, fwdHostsAccept, remotesHttpAccept) ->
            forkWorker "addBundleC: async HTTP Accept delivery" $
                deliverRemoteHttp' fwdHostsAccept obiidAccept docAccept remotesHttpAccept
    return obiidAdd
    where
    verifyHosterRecip _           _    (Right _)            = return ()
    verifyHosterRecip localRecips name (Left (loom, cloth)) = do
        loomHash <- encodeKeyHashid loom
        clothHash <- encodeKeyHashid cloth
        let verify = do
                loomRecips <- lookup loomHash $ recipLooms localRecips
                guard $ leafLoom $ familyLoom $ loomRecips
        fromMaybeE verify $
            name <> " ticket hoster actor isn't listed as a recipient"

    insertAddToOutbox now blinded = do
        let obid = personOutbox personUser
        hLocal <- asksSite siteInstanceHost
        obiid <- insertEmptyOutboxItem obid now
        encodeRouteLocal <- getEncodeRouteLocal
        obikhid <- encodeKeyHashid obiid
        authorHash <- encodeKeyHashid pidUser
        let luAct = encodeRouteLocal $ PersonOutboxItemR authorHash obikhid
            doc = Doc hLocal Activity
                { activityId       = Just luAct
                , activityActor    = encodeRouteLocal $ PersonR authorHash
                , activityCapability = Nothing
                , activitySummary  = summary
                , activityAudience = blinded
                , activitySpecific =
                    AddActivity $ AP.Add (Right $ AddBundle patches) uTarget
                }
        update obiid [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return (obiid, doc, luAct)

    clothRecipSieve (Left (loomID, clothID)) = do
        loomHash <- encodeKeyHashid loomID
        clothHash <- encodeKeyHashid clothID
        return
            ( [LocalActorLoom loomHash]
            , [ LocalStageLoomFollowers loomHash
              , LocalStageClothFollowers loomHash clothHash
              ]
            )
    clothRecipSieve (Right _) = return ([], [])

    insertAccept loomID outboxID clothID addID acceptID bundleID = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost

        loomHash <- encodeKeyHashid loomID
        outboxHash <- encodeKeyHashid outboxID
        clothHash <- encodeKeyHashid clothID
        addHash <- encodeKeyHashid addID
        acceptHash <- encodeKeyHashid acceptID
        bundleHash <- encodeKeyHashid bundleID

        let actors =
                [ LocalActorPerson authorHash
                , LocalActorLoom loomHash
                ]
            stages =
                [ LocalStagePersonFollowers authorHash
                , LocalStageLoomFollowers loomHash
                , LocalStageClothFollowers loomHash clothHash
                ]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [AudLocal actors stages]

            recips = map encodeRouteHome audLocal ++ audRemote
            doc = Doc hLocal Activity
                { activityId       =
                    Just $ encodeRouteLocal $
                        LoomOutboxItemR loomHash outboxHash acceptHash
                , activityActor    = encodeRouteLocal $ LoomR loomHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = Audience recips [] [] [] [] []
                , activitySpecific = AcceptActivity Accept
                    { acceptObject =
                        encodeRouteHome $ PersonOutboxItemR authorHash addHash
                    , acceptResult =
                        Just $ encodeRouteLocal $
                            BundleR loomHash clothHash bundleHash
                    }
                }

        update acceptID [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return (doc, recipientSet, remoteActors, fwdHosts)
-}

applyC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Apply URIMode
    -> ExceptT Text Handler OutboxItemId
applyC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action apply = do

    -- Check input
    maybeLocalTarget <- checkApplyLocalLoom apply
    capID <- fromMaybeE maybeCap "No capability provided"

    -- Verify that the bundle's loom is addressed
    for_ maybeLocalTarget $ \ (_, _, loomID, _, _) -> do
        loomHash <- encodeKeyHashid loomID
        unless (actorIsAddressed localRecips $ LocalActorLoom loomHash) $
            throwE "Bundle's loom not addressed by the Apply"

    maybeLocalTargetDB <- for maybeLocalTarget $
        \ (repoID, maybeBranch, loomID, clothID, bundleID) -> runDBExcept $ do

            -- Find the repo and the bundle in our DB, and verify that the loom
            -- hosting the bundle is willing to accept the request from sender
            -- to apply this specific bundle to this repo/branch
            (loom, ticketID, diffs) <-
                checkApplyDB
                    (Left senderPersonID)
                    capID
                    (repoID, maybeBranch)
                    (loomID, clothID, bundleID)

            return
                (Entity loomID loom, clothID, ticketID, repoID, maybeBranch, diffs)

    -- Apply patches
    for_ maybeLocalTargetDB $
        \ (_, _, _, repoID, maybeBranch, diffs) ->
            applyPatches repoID maybeBranch diffs

    senderHash <- encodeKeyHashid senderPersonID
    now <- liftIO getCurrentTime

    (applyID, deliverHttpApply, maybeDeliverHttpAccept) <- runDBExcept $ do

        -- Insert Apply to sender's outbox
        applyID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        luApply <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) applyID action

        -- Deliver the Apply activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpApply <- do
            sieve <- do
                hashLoom <- getEncodeKeyHashid
                hashCloth <- getEncodeKeyHashid
                let maybeLoom =
                        maybeLocalTargetDB <&>
                            \ (Entity loomID _, clothID, _, _, _, _) ->
                                (hashLoom loomID, hashCloth clothID)
                    sieveActors = catMaybes
                        [ LocalActorLoom . fst <$> maybeLoom
                        ]
                    sieveStages = catMaybes
                        [ LocalStageLoomFollowers . fst <$> maybeLoom
                        , uncurry LocalStageClothFollowers <$> maybeLoom
                        , Just $ LocalStagePersonFollowers senderHash
                        ]
                return $ makeRecipientSet sieveActors sieveStages
            let localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts applyID action

        -- Verify that the loom has received the Apply, resolve the Ticket in
        -- DB, and publish Accept
        maybeDeliverHttpAccept <- for maybeLocalTargetDB $ \ (Entity loomID loom, clothID, ticketID, _repoID, _mb, _diffs) -> do

            -- Verify that loom received the Apply
            let loomActorID = loomActor loom
            verifyActorHasItem loomActorID applyID "Local loom didn't receive the Apply"

            -- Mark ticket in DB as resolved by the Apply
            acceptID <- lift $ do
                actor <- getJust loomActorID
                insertEmptyOutboxItem (actorOutbox actor) now
            lift $ insertResolve ticketID applyID acceptID

            -- Insert an Accept activity to loom's outbox
            loomHash <- encodeKeyHashid loomID
            clothHash <- encodeKeyHashid clothID
            let acceptRecipActors = [LocalActorPerson senderHash]
                acceptRecipStages =
                    [ LocalStageLoomFollowers loomHash
                    , LocalStageClothFollowers loomHash clothHash
                    , LocalStagePersonFollowers senderHash
                    ]
            actionAccept <- prepareAccept luApply acceptRecipActors acceptRecipStages
            _ <- lift $ updateOutboxItem (LocalActorLoom loomID) acceptID actionAccept

            -- Deliver the Accept activity to local recipients, and schedule
            -- delivery for unavailable remote recipients
            let localRecipsAccept =
                    makeRecipientSet acceptRecipActors acceptRecipStages
            deliverActivityDB
                (LocalActorLoom loomHash) loomActorID localRecipsAccept [] []
                acceptID actionAccept

        -- Return instructions for HTTP delivery or Apply and Accept to remote
        -- recipients
        return
            ( applyID
            , deliverHttpApply
            , maybeDeliverHttpAccept
            )

    -- Launch asynchronous HTTP delivery of Apply and Accept
    lift $ do
        forkWorker "applyC: async HTTP Apply delivery" deliverHttpApply
        for_ maybeDeliverHttpAccept $
            forkWorker "applyC: async HTTP Accept delivery"

    return applyID

    where

    insertResolve ticketID applyID acceptID = do
        trid <- insert TicketResolve
            { ticketResolveTicket = ticketID
            , ticketResolveAccept = acceptID
            }
        insert_ TicketResolveLocal
            { ticketResolveLocalTicket   = trid
            , ticketResolveLocalActivity = applyID
            }

    prepareAccept luApply actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return AP.Action
            { AP.actionCapability = Nothing
            , AP.actionSummary    = Nothing
            , AP.actionAudience   = Audience recips [] [] [] [] []
            , AP.actionFulfills   = []
            , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                { AP.acceptObject   = ObjURI hLocal luApply
                , AP.acceptResult   = Nothing
                }
            }

parseComment :: LocalURI -> ExceptT Text Handler (PersonId, LocalMessageId)
parseComment luParent = do
    route <- case decodeRouteLocal luParent of
        Nothing -> throwE "Not a local route"
        Just r -> return r
    case route of
        PersonMessageR personHash messageHash ->
            (,) <$> decodeKeyHashidE personHash "Invalid person hashid"
                <*> decodeKeyHashidE messageHash "Invalid local message hashid"
        _ -> throwE "Not a local message route"

{-
noteC
    :: Entity Person
    -> Note URIMode
    -> ExceptT Text Handler OutboxItemId
noteC eperson@(Entity personID person) note = do
    personHash <- encodeKeyHashid personID
    let username = personUsername person
    summary <-
        renderHTML <$>
            withUrlRenderer
                [hamlet|
                    <p>
                      <a href=@{PersonR personHash}>~#{username2text username}
                      $maybe uContext <- noteContext note
                        \ commented under a #
                        <a href="#{renderObjURI uContext}">topic</a>.
                      $nothing
                        \ commented.
                |]
    createNoteC eperson (Just summary) (noteAudience note) note Nothing
-}

createNoteC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> Note URIMode
    -> Maybe FedURI
    -> ExceptT Text Handler OutboxItemId
createNoteC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action note muTarget = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    Comment maybeParent topic source content <- do
        (authorPersonID, comment) <- parseNewLocalCommentOld note
        unless (authorPersonID == senderPersonID) $
            throwE "Note attributed to someone else"
        return comment
    verifyNothingE muTarget "'target' not supported in Create Note"

    senderHash <- encodeKeyHashid senderPersonID
    now <- liftIO getCurrentTime

    -- If topic is local, verify that its managing actor is addressed
    -- If topic is remote, verify recipient(s) of the same host exist
    verifyTopicAddressed topic

    (createID, deliverHttpCreate) <- runDBExcept $ do

        -- If topic is local, find in DB; if remote, find or insert
        -- If parent is local, find in DB; if remote, find or insert
        (discussionID, meparent) <- getTopicAndParent topic maybeParent

        -- Insert comment to DB and nsert the Create activity to author's
        -- outbox
        createID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        lmid <- lift $ insertMessage now content source createID discussionID meparent
        actionCreate <- lift . lift $ prepareCreate now senderHash lmid
        _luCreate <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) createID actionCreate

        -- Deliver the Create activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpCreate <- do
            sieve <- do
                maybeTopicAudience <-
                    case topic of
                        Left t ->
                            Just <$>
                                bitraverse hashLocalActor hashLocalStage
                                    (commentTopicAudience t)
                        Right _ -> pure Nothing
                let actors = maybeToList $ fst <$> maybeTopicAudience
                    stages =
                        LocalStagePersonFollowers senderHash :
                        case maybeTopicAudience of
                            Nothing -> []
                            Just (actor, followers) ->
                                [localActorFollowers actor, followers]
                return $ makeRecipientSet actors stages
            let localRecipsFinal =
                    localRecipSieve' sieve True False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts createID actionCreate

        -- Return instructions for HTTP delivery to remote recipients
        return (createID, deliverHttpCreate)

    -- Launch asynchronous HTTP delivery
    lift $ forkWorker "createNoteC: async HTTP delivery" deliverHttpCreate
    return createID

    where

    verifyTopicAddressed (Right (ObjURI h _)) =
        unless (any ((== h) . fst) remoteRecips) $
            throwE "Topic is remote but no recipients of that host are listed"
    verifyTopicAddressed (Left topic) = do
        actorByHash <- hashLocalActor $ commentTopicManagingActor topic
        unless (actorIsAddressed localRecips actorByHash) $
            throwE "Local topic's managing actor isn't listed as a recipient"

    getTopicAndParent (Left context) mparent = do
        discussionID <-
            case context of
                CommentTopicTicket deckID ticketID -> do
                    (_, _, Entity _ t, _, _) <- do
                        mticket <- lift $ getTicket deckID ticketID
                        fromMaybeE mticket "Note context no such local deck-hosted ticket"
                    return $ ticketDiscuss t
                CommentTopicCloth loomID clothID -> do
                    (_, _, Entity _ t, _, _, _) <- do
                        mcloth <- lift $ getCloth loomID clothID
                        fromMaybeE mcloth "Note context no such local loom-hosted ticket"
                    return $ ticketDiscuss t
        mmidParent <- for mparent $ \ parent ->
            case parent of
                Left msg -> getLocalParentMessageId discussionID msg
                Right (ObjURI hParent luParent) -> do
                    mrm <- lift $ runMaybeT $ do
                        iid <- MaybeT $ getKeyBy $ UniqueInstance hParent
                        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid luParent
                        MaybeT $ getValBy $ UniqueRemoteMessageIdent roid
                    rm <- fromMaybeE mrm "Remote parent unknown locally"
                    let mid = remoteMessageRest rm
                    m <- lift $ getJust mid
                    unless (messageRoot m == discussionID) $
                        throwE "Remote parent belongs to a different discussion"
                    return mid
        return (discussionID, Left <$> mmidParent)

    getTopicAndParent (Right u@(ObjURI h lu)) mparent = do
        (rd, rdnew) <- lift $ do
            iid <- either entityKey id <$> insertBy' (Instance h)
            roid <- either entityKey id <$> insertBy' (RemoteObject iid lu)
            mrd <- getValBy $ UniqueRemoteDiscussionIdent roid
            case mrd of
                Just rd -> return (rd, False)
                Nothing -> do
                    did <- insert Discussion
                    (rd, rdnew) <- valAndNew <$> insertByEntity' (RemoteDiscussion roid did)
                    unless rdnew $ delete did
                    return (rd, rdnew)
        let discussionID = remoteDiscussionDiscuss rd
        meparent <- for mparent $ \ parent ->
            case parent of
                Left msg -> do
                    when rdnew $ throwE "Local parent inexistent, RemoteDiscussion is new"
                    Left <$> getLocalParentMessageId discussionID msg
                Right uParent@(ObjURI hParent luParent) -> do
                    mrm <- lift $ runMaybeT $ do
                        iid <- MaybeT $ getKeyBy $ UniqueInstance hParent
                        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid luParent
                        MaybeT $ getValBy $ UniqueRemoteMessageIdent roid
                    case mrm of
                        Nothing -> return $ Right uParent
                        Just rm -> Left <$> do
                            let mid = remoteMessageRest rm
                            m <- lift $ getJust mid
                            unless (messageRoot m == discussionID) $
                                throwE "Remote parent belongs to a different discussion"
                            return mid
        return (discussionID, meparent)

    insertMessage now content source obiidCreate did meparent = do
        mid <- insert Message
            { messageCreated = now
            , messageSource  = source
            , messageContent = content
            , messageParent  =
                case meparent of
                    Just (Left midParent) -> Just midParent
                    _                     -> Nothing
            , messageRoot    = did
            }
        insert LocalMessage
            { localMessageAuthor         = personActor senderPerson
            , localMessageRest           = mid
            , localMessageCreate         = obiidCreate
            , localMessageUnlinkedParent =
                case meparent of
                    Just (Right uParent) -> Just uParent
                    _                    -> Nothing
            }

    prepareCreate now senderHash messageID = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksSite siteInstanceHost
        messageHash <- encodeKeyHashid messageID
        let luId = encodeRouteLocal $ PersonMessageR senderHash messageHash
            note' = note
                { AP.noteId        = Just luId
                , AP.notePublished = Just now
                , AP.noteAudience  = emptyAudience
                }
        return action { AP.actionSpecific = AP.CreateActivity $ AP.Create (AP.CreateNote hLocal note') Nothing }

createPatchTrackerC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.ActorDetail
    -> NonEmpty FedURI
    -> Maybe (Host, AP.ActorLocal URIMode)
    -> Maybe FedURI
    -> ExceptT Text Handler OutboxItemId
createPatchTrackerC (Entity pidUser personUser) senderActor maybeCap localRecips remoteRecips fwdHosts action detail repos mlocal muTarget = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    verifyNothingE mlocal "'id' not allowed in new PatchTracker to create"
    (name, msummary) <- parseDetail detail
    repoID <- parseRepo repos
    senderHash <- encodeKeyHashid pidUser
    now <- liftIO getCurrentTime
    verifyNothingE muTarget "'target' not supported in Create PatchTracker"

    (obiid, deliverHttpCreate, deliverHttpGrant) <- runDBExcept $ do

        -- Find the specified repo in DB
        _ <- getE repoID "No such repo in DB"

        -- Make sure the repo has a single, full-access collab, granted to the
        -- sender of this Create
        maybeApproved <- lift $ runMaybeT $ do
            collabs <- lift $ selectList [CollabTopicRepoRepo ==. repoID] []
            collabID <- 
                case collabs of
                    [Entity _ c] -> return $ collabTopicRepoCollab c
                    _ -> mzero
            CollabRecipLocal _ recipID <-
                MaybeT $ getValBy $ UniqueCollabRecipLocal collabID
            guard $ recipID == pidUser
            _ <- MaybeT $ getBy $ UniqueCollabEnable collabID
            _ <- MaybeT $ getBy $ UniqueCollabFulfillsLocalTopicCreation collabID
            return ()
        unless (isJust maybeApproved) $
            throwE "Repo's collabs unexpected state"

        -- Insert new loom to DB
        obiidCreate <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        (loomID, Entity loomActorID loomActor) <-
            lift $ insertLoom now name msummary obiidCreate repoID

        -- Insert the Create activity to author's outbox
        loomHash <- encodeKeyHashid loomID
        repoHash <- encodeKeyHashid repoID
        actionCreate <- prepareCreate name msummary loomHash repoHash
        _luCreate <- lift $ updateOutboxItem (LocalActorPerson pidUser) obiidCreate actionCreate

        -- Deliver the Create activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpCreate <- do
            let sieve =
                    makeRecipientSet
                        [LocalActorRepo repoHash]
                        [ LocalStagePersonFollowers senderHash
                        , LocalStageRepoFollowers repoHash
                        ]
                localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor personUser)
                localRecipsFinal remoteRecips fwdHosts obiidCreate actionCreate

        -- Insert collaboration access for loom's creator
        let loomOutboxID = actorOutbox loomActor
        obiidGrant <- lift $ insertEmptyOutboxItem loomOutboxID now
        lift $ insertCollab loomID obiidGrant

        -- Insert a Grant activity to loom's outbox
        let grantRecipActors = [LocalActorPerson senderHash]
            grantRecipStages = [LocalStagePersonFollowers senderHash]
        actionGrant <-
            prepareGrant senderHash loomHash obiidCreate grantRecipActors grantRecipStages
        _luGrant <- lift $ updateOutboxItem (LocalActorLoom loomID) obiidGrant actionGrant

        -- Deliver the Grant activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpGrant <- do
            let localRecipsGrant =
                    makeRecipientSet grantRecipActors grantRecipStages
            deliverActivityDB
                (LocalActorLoom loomHash) loomActorID localRecipsGrant [] []
                obiidGrant actionGrant

        -- Insert follow record
        obiidFollow <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        obiidAccept <- lift $ insertEmptyOutboxItem loomOutboxID now
        lift $ insert_ $ Follow (personActor personUser) (actorFollowers loomActor) True obiidFollow obiidAccept

        -- Insert a Follow activity to sender's outbox, and an Accept to the
        -- loom's outbox
        luFollow <- lift $ insertFollowToOutbox senderHash loomHash obiidFollow
        lift $ insertAcceptToOutbox senderHash loomHash obiidAccept luFollow

        -- Deliver the Follow and Accept by simply manually inserting them to
        -- loom and sender inboxes respectively
        lift $ do
            ibiidF <- insert $ InboxItem False now
            insert_ $ InboxItemLocal (actorInbox loomActor) obiidFollow ibiidF
            ibiidA <- insert $ InboxItem False now
            insert_ $ InboxItemLocal (actorInbox senderActor) obiidAccept ibiidA

        -- Return instructions for HTTP delivery to remote recipients
        return (obiidCreate, deliverHttpCreate, deliverHttpGrant)

    -- Launch asynchronous HTTP delivery of Create and Grant
    lift $ do
        forkWorker "createPatchTrackerC: async HTTP Create delivery" deliverHttpCreate
        forkWorker "createPatchTrackerC: async HTTP Grant delivery" deliverHttpGrant

    return obiid

    where

    parseDetail (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypePatchTracker) $
            error "createPatchTrackerC: Create object isn't a PatchTracker"
        verifyNothingE muser "PatchTracker can't have a username"
        name <- fromMaybeE mname "PatchTracker doesn't specify name"
        return (name, msummary)

    parseRepo (ObjURI h lu :| us) = do
        unless (null us) $ throwE "More than one repo is specified"
        hl <- hostIsLocalOld h
        unless hl $ throwE "A remote repo is specified"
        route <- fromMaybeE (decodeRouteLocal lu) "Not a valid route"
        case route of
            RepoR repoHash -> decodeKeyHashidE repoHash "Invalid repo hash"
            _ -> throwE "Not a repo route"

    insertLoom now name msummary obiidCreate repoID = do
        actor@(Entity actorID _) <-
            insertActor now name (fromMaybe "" msummary)
        loomID <- insert Loom
            { loomNextTicket = 1
            , loomActor      = actorID
            , loomRepo       = repoID
            , loomCreate     = obiidCreate
            }
        return (loomID, actor)

    prepareCreate name msummary loomHash repoHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let ptdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypePatchTracker
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            ptlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ LoomR loomHash
                , AP.actorInbox      = encodeRouteLocal $ LoomInboxR loomHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            repo = encodeRouteHome $ RepoR repoHash
            specific = CreateActivity Create
                { createObject   = CreatePatchTracker ptdetail (repo :| []) (Just (hLocal, ptlocal))
                , createTarget   = Nothing
                }
        return action { actionSpecific = specific }

    insertCollab loomID obiidGrant = do
        cid <- insert Collab
        insert_ $ CollabTopicLoom cid loomID
        insert_ $ CollabEnable cid obiidGrant
        insert_ $ CollabRecipLocal cid pidUser
        insert_ $ CollabFulfillsLocalTopicCreation cid

    prepareGrant adminHash loomHash obiidCreate actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal
        obikhidCreate <- encodeKeyHashid obiidCreate
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return Action
            { actionCapability = Nothing
            , actionSummary  = Nothing
            , actionAudience = Audience recips [] [] [] [] []
            , actionFulfills =
                [encodeRouteHome $ PersonOutboxItemR adminHash obikhidCreate]
            , actionSpecific = GrantActivity Grant
                { grantObject   = Left RoleAdmin
                , grantContext  = encodeRouteLocal $ LoomR loomHash
                , grantTarget   = encodeRouteHome $ PersonR adminHash
                , grantResult  = Nothing
                , grantStart   = Nothing
                , grantEnd     = Nothing
                , grantAllows  = Invoke
                , grantDelegates = Nothing
                }
            }

    insertFollowToOutbox senderHash loomHash obiidFollow = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost

        obikhid <- encodeKeyHashid obiidFollow
        let luFollow = encodeRouteLocal $ PersonOutboxItemR senderHash obikhid
            recips = [encodeRouteHome $ LoomR loomHash]
            doc = Doc hLocal Activity
                { activityId       = Just luFollow
                , activityActor    = encodeRouteLocal $ PersonR senderHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = AP.Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ LoomR loomHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        update obiidFollow [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return luFollow

    insertAcceptToOutbox senderHash loomHash obiidAccept luFollow = do
        hLocal <- asksSite siteInstanceHost
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome

        obikhid <- encodeKeyHashid obiidAccept

        let recips = [encodeRouteHome $ PersonR senderHash]
            doc = Doc hLocal Activity
                { activityId       = Just $ encodeRouteLocal $ LoomOutboxItemR loomHash obikhid
                , activityActor    = encodeRouteLocal $ LoomR loomHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = AcceptActivity Accept
                    { acceptObject = ObjURI hLocal luFollow
                    , acceptResult = Nothing
                    }
                }
        update obiidAccept [OutboxItemActivity =. persistJSONObjectFromDoc doc]

createRepositoryC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.ActorDetail
    -> VersionControlSystem
    -> Maybe (Host, AP.ActorLocal URIMode)
    -> Maybe FedURI
    -> ExceptT Text Handler OutboxItemId
createRepositoryC (Entity pidUser personUser) senderActor maybeCap localRecips remoteRecips fwdHosts action detail vcs mlocal muTarget = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    verifyNothingE mlocal "'id' not allowed in new Repository to create"
    (name, msummary) <- parseDetail detail
    senderHash <- encodeKeyHashid pidUser
    now <- liftIO getCurrentTime
    verifyNothingE muTarget "'target' not supported in Create Repository"

    (obiid, newRepoHash, deliverHttpCreate, deliverHttpGrant) <- runDBExcept $ do

        -- Insert new repo to DB
        obiidCreate <-
            lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        (repoID, Entity repoActorID repoActor) <-
            lift $ insertRepo now name msummary obiidCreate

        -- Insert the Create activity to author's outbox
        repoHash <- encodeKeyHashid repoID
        actionCreate <- prepareCreate now name msummary repoHash
        _luCreate <- lift $ updateOutboxItem (LocalActorPerson pidUser) obiidCreate actionCreate

        -- Deliver the Create activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpCreate <- do
            let sieve =
                    makeRecipientSet [] [LocalStagePersonFollowers senderHash]
                localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor personUser)
                localRecipsFinal remoteRecips fwdHosts obiidCreate actionCreate

        -- Insert collaboration access for repo's creator
        let repoOutboxID = actorOutbox repoActor
        grantID <- lift $ insertEmptyOutboxItem repoOutboxID now
        lift $ insertCollab repoID grantID

        -- Insert a Grant activity to repo's outbox
        let grantRecipActors = [LocalActorPerson senderHash]
            grantRecipStages = [LocalStagePersonFollowers senderHash]
        actionGrant <- prepareGrant senderHash repoHash obiidCreate grantRecipActors grantRecipStages
        _luGrant <- lift $ updateOutboxItem (LocalActorRepo repoID) grantID actionGrant

        -- Deliver the Grant activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpGrant <- do
            let localRecipsGrant =
                    makeRecipientSet grantRecipActors grantRecipStages
            deliverActivityDB
                (LocalActorRepo repoHash) repoActorID localRecipsGrant [] []
                grantID actionGrant

        -- Insert follow record
        obiidFollow <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        obiidAccept <- lift $ insertEmptyOutboxItem repoOutboxID now
        lift $ insert_ $ Follow (personActor personUser) (actorFollowers repoActor) True obiidFollow obiidAccept

        -- Insert a Follow activity to sender's outbox, and an Accept to the
        -- repo's outbox
        luFollow <- lift $ insertFollowToOutbox senderHash repoHash obiidFollow
        lift $ insertAcceptToOutbox senderHash repoHash obiidAccept luFollow

        -- Deliver the Follow and Accept by simply manually inserting them to
        -- repo and sender inboxes respectively
        lift $ do
            ibiidF <- insert $ InboxItem False now
            insert_ $ InboxItemLocal (actorInbox repoActor) obiidFollow ibiidF
            ibiidA <- insert $ InboxItem False now
            insert_ $ InboxItemLocal (actorInbox senderActor) obiidAccept ibiidA

        -- Return instructions for HTTP delivery to remote recipients
        return (obiidCreate, repoHash, deliverHttpCreate, deliverHttpGrant)

    -- Insert new repo to filesystem
    lift $ createRepo newRepoHash

    -- Launch asynchronous HTTP delivery of Create and Grant
    lift $ do
        forkWorker "createRepositoryC: async HTTP Create delivery" deliverHttpCreate
        forkWorker "createRepositoryC: async HTTP Grant delivery" deliverHttpGrant

    return obiid

    where

    parseDetail (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypeRepo) $
            error "createRepositoryC: Create object isn't a Repository"
        verifyNothingE muser "Repository can't have a username"
        name <- fromMaybeE mname "Repository doesn't specify name"
        return (name, msummary)

    insertRepo now name msummary createID = do
        actor@(Entity actorID _) <-
            insertActor now name (fromMaybe "" msummary)
        repoID <- insert Repo
            { repoVcs        = vcs
            , repoProject    = Nothing
            , repoMainBranch = "main"
            , repoCollabUser = Nothing
            , repoCollabAnon = Nothing
            , repoActor      = actorID
            , repoCreate     = createID
            , repoLoom       = Nothing
            }
        return (repoID, actor)

    prepareCreate now name msummary repoHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksSite siteInstanceHost
        let rdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypeRepo
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            rlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ RepoR repoHash
                , AP.actorInbox      = encodeRouteLocal $ RepoInboxR repoHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            specific = CreateActivity Create
                { createObject = CreateRepository rdetail vcs (Just (hLocal, rlocal))
                , createTarget = Nothing
                }
        return action { actionSpecific = specific }

    insertCollab repoID grantID = do
        collabID <- insert Collab
        insert_ $ CollabTopicRepo collabID repoID
        insert_ $ CollabEnable collabID grantID
        insert_ $ CollabRecipLocal collabID pidUser
        insert_ $ CollabFulfillsLocalTopicCreation collabID

    prepareGrant adminHash repoHash obiidCreate actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal
        obikhidCreate <- encodeKeyHashid obiidCreate
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return Action
            { actionCapability = Nothing
            , actionSummary    = Nothing
            , actionAudience   = Audience recips [] [] [] [] []
            , actionFulfills   =
                [encodeRouteHome $ PersonOutboxItemR adminHash obikhidCreate]
            , actionSpecific   = GrantActivity Grant
                { grantObject    = Left RoleAdmin
                , grantContext   = encodeRouteLocal $ RepoR repoHash
                , grantTarget    = encodeRouteHome $ PersonR adminHash
                , grantResult    = Nothing
                , grantStart   = Nothing
                , grantEnd     = Nothing
                , grantAllows  = Invoke
                , grantDelegates = Nothing
                }
            }

    insertFollowToOutbox senderHash repoHash obiidFollow = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost

        obikhid <- encodeKeyHashid obiidFollow
        let luFollow = encodeRouteLocal $ PersonOutboxItemR senderHash obikhid
            recips = [encodeRouteHome $ RepoR repoHash]
            doc = Doc hLocal Activity
                { activityId       = Just luFollow
                , activityActor    = encodeRouteLocal $ PersonR senderHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = AP.Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ RepoR repoHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        update obiidFollow [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return luFollow

    insertAcceptToOutbox senderHash repoHash obiidAccept luFollow = do
        hLocal <- asksSite siteInstanceHost
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome

        obikhid <- encodeKeyHashid obiidAccept

        let recips = [encodeRouteHome $ PersonR senderHash]
            doc = Doc hLocal Activity
                { activityId       = Just $ encodeRouteLocal $ RepoOutboxItemR repoHash obikhid
                , activityActor    = encodeRouteLocal $ RepoR repoHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = AcceptActivity Accept
                    { acceptObject = ObjURI hLocal luFollow
                    , acceptResult = Nothing
                    }
                }
        update obiidAccept [OutboxItemActivity =. persistJSONObjectFromDoc doc]

    createRepo repoHash = do
        root <- askRepoRootDir
        liftIO $ createDirectoryIfMissing True root
        host <- asksSite siteInstanceHost
        case vcs of
            VCSDarcs -> do
                hook <- getsYesod $ appPostApplyHookFile . appSettings
                liftIO $
                    D.createRepo
                        root
                        (keyHashidText repoHash)
                        hook
                        (renderAuthority host)
            VCSGit -> do
                hook <- getsYesod $ appPostReceiveHookFile . appSettings
                liftIO $
                    G.createRepo
                        root
                        (keyHashidText repoHash)
                        hook
                        (renderAuthority host)

createTicketTrackerC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.ActorDetail
    -> Maybe (Host, AP.ActorLocal URIMode)
    -> Maybe FedURI
    -> ExceptT Text Handler OutboxItemId
createTicketTrackerC (Entity pidUser personUser) senderActor maybeCap localRecips remoteRecips fwdHosts action tracker mlocal muTarget = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    verifyNothingE mlocal "'id' not allowed in new TicketTracker to create"
    (name, msummary) <- parseTracker tracker
    senderHash <- encodeKeyHashid pidUser
    now <- liftIO getCurrentTime
    verifyNothingE muTarget "'target' not supported in Create TicketTracker"

    (obiid, deliverHttpCreate, deliverHttpGrant) <- runDBExcept $ do

        -- Insert new deck to DB
        obiidCreate <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        wid <- findWorkflow
        (jid, obidDeck, ibidDeck, aidDeck, fsidDeck) <- lift $ insertDeck now name msummary obiidCreate wid

        -- Insert the Create activity to author's outbox
        deckHash <- encodeKeyHashid jid
        actionCreate <- prepareCreate name msummary deckHash
        _luCreate <- lift $ updateOutboxItem (LocalActorPerson pidUser) obiidCreate actionCreate

        -- Deliver the Create activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpCreate <- do
            let sieve =
                    makeRecipientSet [] [LocalStagePersonFollowers senderHash]
                localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor personUser)
                localRecipsFinal remoteRecips fwdHosts obiidCreate actionCreate

        -- Insert collaboration access for deck's creator
        obiidGrant <- lift $ insertEmptyOutboxItem obidDeck now
        lift $ insertCollab jid obiidGrant

        -- Insert a Grant activity to deck's outbox
        let grantRecipActors = [LocalActorPerson senderHash]
            grantRecipStages = [LocalStagePersonFollowers senderHash]
        actionGrant <- prepareGrant senderHash deckHash obiidCreate grantRecipActors grantRecipStages
        _luGrant <- lift $ updateOutboxItem (LocalActorDeck jid) obiidGrant actionGrant

        -- Deliver the Grant activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpGrant <- do
            let localRecipsGrant =
                    makeRecipientSet grantRecipActors grantRecipStages
            deliverActivityDB
                (LocalActorDeck deckHash) aidDeck localRecipsGrant [] []
                obiidGrant actionGrant

        -- Insert follow record
        obiidFollow <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        obiidAccept <- lift $ insertEmptyOutboxItem obidDeck now
        lift $ insert_ $ Follow (personActor personUser) fsidDeck True obiidFollow obiidAccept

        -- Insert a Follow activity to sender's outbox, and an Accept to the
        -- deck's outbox
        luFollow <- lift $ insertFollowToOutbox senderHash deckHash obiidFollow
        lift $ insertAcceptToOutbox senderHash deckHash obiidAccept luFollow

        -- Deliver the Follow and Accept by simply manually inserting them to
        -- deck and sender inboxes respectively
        lift $ do
            ibiidF <- insert $ InboxItem False now
            insert_ $ InboxItemLocal ibidDeck obiidFollow ibiidF
            ibiidA <- insert $ InboxItem False now
            insert_ $ InboxItemLocal (actorInbox senderActor) obiidAccept ibiidA

        -- Return instructions for HTTP delivery to remote recipients
        return (obiidCreate, deliverHttpCreate, deliverHttpGrant)

    -- Launch asynchronous HTTP delivery of Create and Grant
    lift $ do
        forkWorker "createTicketTrackerC: async HTTP Create delivery" deliverHttpCreate
        forkWorker "createTicketTrackerC: async HTTP Grant delivery" deliverHttpGrant

    return obiid
    where
    parseTracker (AP.ActorDetail typ muser mname msummary) = do
        unless (typ == AP.ActorTypeTicketTracker) $
            error "createTicketTrackerC: Create object isn't a TicketTracker"
        verifyNothingE muser "TicketTracker can't have a username"
        name <- fromMaybeE mname "TicketTracker doesn't specify name"
        return (name, msummary)

    findWorkflow = do
        mw <- lift $ selectFirst ([] :: [Filter Workflow]) []
        entityKey <$> fromMaybeE mw "Can't find a workflow"

    insertDeck now name msummary obiidCreate wid = do
        ibid <- insert Inbox
        obid <- insert Outbox
        fsid <- insert FollowerSet
        aid <- insert Actor
            { actorName      = name
            , actorDesc      = fromMaybe "" msummary
            , actorCreatedAt = now
            , actorInbox     = ibid
            , actorOutbox    = obid
            , actorFollowers = fsid
            }
        did <- insert Deck
            { deckActor      = aid
            , deckWorkflow   = wid
            , deckNextTicket = 1
            , deckWiki       = Nothing
            , deckCollabAnon = Nothing
            , deckCollabUser = Nothing
            , deckCreate     = obiidCreate
            }
        return (did, obid, ibid, aid, fsid)

    prepareCreate name msummary deckHash = do
        encodeRouteLocal <- getEncodeRouteLocal
        hLocal <- asksSite siteInstanceHost
        let ttdetail = AP.ActorDetail
                { AP.actorType     = AP.ActorTypeTicketTracker
                , AP.actorUsername = Nothing
                , AP.actorName     = Just name
                , AP.actorSummary  = msummary
                }
            ttlocal = AP.ActorLocal
                { AP.actorId         = encodeRouteLocal $ DeckR deckHash
                , AP.actorInbox      = encodeRouteLocal $ DeckInboxR deckHash
                , AP.actorOutbox     = Nothing
                , AP.actorFollowers  = Nothing
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys = []
                , AP.actorSshKeys    = []
                }
            specific = CreateActivity Create
                { createObject = CreateTicketTracker ttdetail (Just (hLocal, ttlocal))
                , createTarget = Nothing
                }
        return action { actionSpecific = specific }

    insertCollab did obiidGrant = do
        cid <- insert Collab
        insert_ $ CollabTopicDeck cid did
        insert_ $ CollabEnable cid obiidGrant
        insert_ $ CollabRecipLocal cid pidUser
        insert_ $ CollabFulfillsLocalTopicCreation cid

    prepareGrant adminHash deckHash obiidCreate actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal
        obikhidCreate <- encodeKeyHashid obiidCreate
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return Action
            { actionCapability = Nothing
            , actionSummary    = Nothing
            , actionAudience   = Audience recips [] [] [] [] []
            , actionFulfills   =
                [encodeRouteHome $ PersonOutboxItemR adminHash obikhidCreate]
            , actionSpecific   = GrantActivity Grant
                { grantObject    = Left RoleAdmin
                , grantContext   = encodeRouteLocal $ DeckR deckHash
                , grantTarget    = encodeRouteHome $ PersonR adminHash
                , grantResult    = Nothing
                , grantStart   = Nothing
                , grantEnd     = Nothing
                , grantAllows  = Invoke
                , grantDelegates = Nothing
                }
            }

    insertFollowToOutbox senderHash deckHash obiidFollow = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost

        obikhid <- encodeKeyHashid obiidFollow
        let luFollow = encodeRouteLocal $ PersonOutboxItemR senderHash obikhid
            recips = [encodeRouteHome $ DeckR deckHash]
            doc = Doc hLocal Activity
                { activityId       = Just luFollow
                , activityActor    = encodeRouteLocal $ PersonR senderHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = AP.Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = FollowActivity AP.Follow
                    { AP.followObject  = encodeRouteHome $ DeckR deckHash
                    , AP.followContext = Nothing
                    , AP.followHide    = False
                    }
                }
        update obiidFollow [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return luFollow

    insertAcceptToOutbox senderHash deckHash obiidAccept luFollow = do
        hLocal <- asksSite siteInstanceHost
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome

        obikhid <- encodeKeyHashid obiidAccept

        let recips = [encodeRouteHome $ PersonR senderHash]
            doc = Doc hLocal Activity
                { activityId       = Just $ encodeRouteLocal $ DeckOutboxItemR deckHash obikhid
                , activityActor    = encodeRouteLocal $ DeckR deckHash
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = Audience recips [] [] [] [] []
                , activityFulfills = []
                , activityProof    = Nothing
                , activitySpecific = AcceptActivity Accept
                    { acceptObject = ObjURI hLocal luFollow
                    , acceptResult = Nothing
                    }
                }
        update obiidAccept [OutboxItemActivity =. persistJSONObjectFromDoc doc]

followC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Follow URIMode
    -> ExceptT Text Handler OutboxItemId
followC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action follow = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    (followee, hide) <- parseFollow follow
    case followee of
        Left (FolloweeActor (LocalActorPerson personID))
            | personID == senderPersonID ->
                throwE "Trying to follow yourself"
        _ -> pure ()

    -- Verify that followee's actor is addressed
    case followee of
        Left f -> do
            actorByHash <- hashLocalActor $ followeeActor f
            unless (actorIsAddressed localRecips actorByHash) $
                throwE "Followee's actor not addressed by the Follow"
        Right (h, luActor, luObject) ->
            verifyRemoteAddressed remoteRecips $ ObjURI h luActor

    now <- liftIO getCurrentTime
    senderHash <- encodeKeyHashid senderPersonID

    (followID, deliverHttpFollow, maybeDeliverHttpAccept) <- runDBExcept $ do

        -- If followee is local, find it in our DB
        followeeDB <- bitraverse getFollowee pure followee

        -- Insert Follow activity to author's outbox
        followID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        luFollow <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) followID action

        -- Deliver the Follow activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpFollow <- do
            sieve <- do
                (actors, stages) <-
                    case followeeDB of
                        Left (actorByKey, _, _) -> do
                            actorByHash <- hashLocalActor actorByKey
                            return
                                ( [actorByHash]
                                , [localActorFollowers actorByHash]
                                )
                        Right _ -> pure ([], [])
                let stages' = LocalStagePersonFollowers senderHash : stages
                return $ makeRecipientSet actors stages'
            let localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts followID action

        maybeDeliverHttpAccept <-
            case followeeDB of
                Right (h, luActor, luObject) -> lift $ do

                    -- For remote followee, just remember the request in our DB
                    let uObject = ObjURI h luObject
                        muContext =
                            if luActor == luObject
                                then Nothing
                                else Just $ ObjURI h luActor
                    insert_ $ FollowRemoteRequest senderPersonID uObject muContext (not hide) followID
                    return Nothing

                Left (actorByKey, actorID, maybeFollowerSetID) -> Just <$> do

                    -- Verify followee's actor has received the Accept
                    verifyActorHasItem actorID followID "Followee's actor didn't receive the Follow"

                    -- Insert an Accept activity to followee's outbox
                    actor <- lift $ getJust actorID
                    acceptID <- lift $ insertEmptyOutboxItem (actorOutbox actor) now
                    let acceptActors = [LocalActorPerson senderHash]
                        acceptStages = []
                    actionAccept <- prepareAccept luFollow acceptActors acceptStages
                    _luAccept <- lift $ updateOutboxItem actorByKey acceptID actionAccept

                    -- Insert author to followee's followers collection
                    let fsid =
                            fromMaybe (actorFollowers actor) maybeFollowerSetID
                    mfid <-
                        lift $ insertUnique $
                        Follow (personActor senderPerson) fsid (not hide) followID acceptID
                    _ <- fromMaybeE mfid "Already following this object"

                    -- Deliver the Accept activity to local recipients, and
                    -- schedule delivery for unavailable remote recipients
                    let localRecipsAccept = makeRecipientSet acceptActors acceptStages
                    actorByHash <- hashLocalActor actorByKey
                    deliverActivityDB
                        actorByHash actorID localRecipsAccept [] []
                        acceptID actionAccept

        -- Return instructions for HTTP delivery to remote recipients
        return (followID, deliverHttpFollow, maybeDeliverHttpAccept)

    -- Launch asynchronous HTTP delivery of Follow and Accept
    lift $ do
        forkWorker "followC: async HTTP Follow delivery" deliverHttpFollow
        for_ maybeDeliverHttpAccept $
            forkWorker "followC: async HTTP Accept delivery"

    return followID

    where

    prepareAccept luFollow actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return AP.Action
            { AP.actionCapability = Nothing
            , AP.actionSummary    = Nothing
            , AP.actionAudience   = Audience recips [] [] [] [] []
            , AP.actionFulfills   = []
            , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                { AP.acceptObject   = ObjURI hLocal luFollow
                , AP.acceptResult   = Nothing
                }
            }

inviteC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Invite URIMode
    -> ExceptT Text Handler OutboxItemId
inviteC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action invite = do
    error "Temporarily disabled due to switch to new actor system"
{-
    -- Check input
    (resource, recipient) <- parseInvite (Left senderPersonID) invite
    capID <- fromMaybeE maybeCap "No capability provided"

    -- If resource is remote, HTTP GET it and its managing actor, and insert to
    -- our DB. If resource is local, find it in our DB.
    resourceDB <-
        bitraverse
            (runDBExcept . flip getGrantResource "Grant context not found in DB")
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ runDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . show) <$>
                        fetchRemoteResource instanceID h lu
                case result of
                    Left (Entity actorID actor) ->
                        return (remoteActorIdent actor, actorID, u)
                    Right (objectID, luManager, (Entity actorID _)) ->
                        return (objectID, actorID, ObjURI h luManager)
            )
            resource

    -- If recipient is remote, HTTP GET it, make sure it's an actor, and insert
    -- it to our DB. If recipient is local, find it in our DB.
    recipientDB <-
        bitraverse
            (runDBExcept . flip getGrantRecip "Grant recipient not found in DB")
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ runDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor instanceID h lu
                case result of
                    Left Nothing -> throwE "Recipient @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Recipient isn't an actor"
                    Right (Just actor) -> return (entityKey actor, u)
            )
            recipient

    -- Verify that resource and recipient are addressed by the Invite
    bitraverse_
        (verifyResourceAddressed localRecips . bmap entityKey)
        (\ (_, _, u) -> verifyRemoteAddressed remoteRecips u)
        resourceDB
    bitraverse_
        (verifyRecipientAddressed localRecips . bmap entityKey)
        (verifyRemoteAddressed remoteRecips . snd)
        recipientDB

    now <- liftIO getCurrentTime
    senderHash <- encodeKeyHashid senderPersonID

    (obiidInvite, deliverHttpInvite) <- runDBExcept $ do

        -- If resource is local, verify the specified capability gives relevant
        -- access to it.
        case resourceDB of
            Left r -> do
                capability <-
                    case capID of
                        Left (actor, _, item) -> return (actor, item)
                        Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local topic"
                verifyCapability capability (Left senderPersonID) (bmap entityKey r)
            Right _ -> pure ()

        -- Insert new Collab to DB
        inviteID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        case resourceDB of
            Left localResource ->
                lift $ insertCollab localResource recipientDB inviteID
            Right _ -> pure ()

        -- Insert the Grant activity to author's outbox
        _luInvite <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) inviteID action

        -- Deliver the Invite activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpInvite <- do
            sieve <- do
                resourceHash <- bitraverse hashGrantResource pure resource
                recipientHash <- bitraverse hashGrantRecip pure recipient
                let sieveActors = catMaybes
                        [ case resourceHash of
                            Left (GrantResourceRepo r) -> Just $ LocalActorRepo r
                            Left (GrantResourceDeck d) -> Just $ LocalActorDeck d
                            Left (GrantResourceLoom l) -> Just $ LocalActorLoom l
                            Right _                    -> Nothing
                        , case recipientHash of
                            Left (GrantRecipPerson p) -> Just $ LocalActorPerson p
                            Right _                   -> Nothing
                        ]
                    sieveStages = catMaybes
                        [ Just $ LocalStagePersonFollowers senderHash
                        , case resourceHash of
                            Left (GrantResourceRepo r) -> Just $ LocalStageRepoFollowers r
                            Left (GrantResourceDeck d) -> Just $ LocalStageDeckFollowers d
                            Left (GrantResourceLoom l) -> Just $ LocalStageLoomFollowers l
                            Right _                    -> Nothing
                        , case recipientHash of
                            Left (GrantRecipPerson p) -> Just $ LocalStagePersonFollowers p
                            Right _                   -> Nothing
                        ]
                return $ makeRecipientSet sieveActors sieveStages
            let localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts inviteID action

        -- If resource is local, verify it has received the Grant
        case resourceDB of
            Left localResource -> do
                let resourceActorID =
                        case localResource of
                            GrantResourceRepo (Entity _ r) -> repoActor r
                            GrantResourceDeck (Entity _ d) -> deckActor d
                            GrantResourceLoom (Entity _ l) -> loomActor l
                verifyActorHasItem resourceActorID inviteID "Local topic didn't receive the Invite"
            Right _ -> pure ()

        -- If recipient is local, verify it has received the invite
        case recipientDB of
            Left (GrantRecipPerson (Entity _ p)) ->
                verifyActorHasItem (personActor p) inviteID "Local recipient didn't receive the Invite"
            Right _ -> pure ()

        -- Return instructions for HTTP delivery to remote recipients
        return (inviteID, deliverHttpInvite)

    -- Launch asynchronous HTTP delivery of the Grant activity
    lift $ do
        forkWorker "inviteC: async HTTP Grant delivery" deliverHttpInvite

    return obiidInvite

    where

    fetchRemoteResource instanceID host localURI = do
        maybeActor <- runSiteDB $ runMaybeT $ do
            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject instanceID localURI
            MaybeT $ getBy $ UniqueRemoteActor roid
        case maybeActor of
            Just actor -> return $ Right $ Left actor
            Nothing -> do
                manager <- asksSite getHttpManager
                errorOrResource <- fetchResource manager host localURI
                case errorOrResource of
                    Left maybeError ->
                        return $ Left $ maybe ResultIdMismatch ResultGetError maybeError
                    Right resource -> do
                        case resource of
                            ResourceActor (AP.Actor local detail) -> runSiteDB $ do
                                roid <- either entityKey id <$> insertBy' (RemoteObject instanceID localURI)
                                let ra = RemoteActor
                                        { remoteActorIdent      = roid
                                        , remoteActorName       =
                                            AP.actorName detail <|> AP.actorUsername detail
                                        , remoteActorInbox      = AP.actorInbox local
                                        , remoteActorFollowers  = AP.actorFollowers local
                                        , remoteActorErrorSince = Nothing
                                        }
                                Right . Left . either id id <$> insertByEntity' ra
                            ResourceChild luId luManager -> do
                                roid <- runSiteDB $ either entityKey id <$> insertBy' (RemoteObject instanceID localURI)
                                result <- fetchRemoteActor instanceID host luManager
                                return $
                                    case result of
                                        Left e -> Left $ ResultSomeException e
                                        Right (Left Nothing) -> Left ResultIdMismatch
                                        Right (Left (Just e)) -> Left $ ResultGetError e
                                        Right (Right Nothing) -> Left ResultNotActor
                                        Right (Right (Just actor)) -> Right $ Right (roid, luManager, actor)

    verifyRecipientAddressed localRecips recipient = do
        recipientHash <- hashGrantRecip recipient
        fromMaybeE (verify recipientHash) "Recipient not addressed"
        where
        verify (GrantRecipPerson p) = do
            routes <- lookup p $ recipPeople localRecips
            guard $ routePerson routes

    insertCollab resource recipient inviteID = do
        collabID <- insert Collab
        case resource of
            GrantResourceRepo (Entity repoID _) ->
                insert_ $ CollabTopicRepo collabID repoID
            GrantResourceDeck (Entity deckID _) ->
                insert_ $ CollabTopicDeck collabID deckID
            GrantResourceLoom (Entity loomID _) ->
                insert_ $ CollabTopicLoom collabID loomID
        fulfillsID <- insert $ CollabFulfillsInvite collabID
        insert_ $ CollabInviterLocal fulfillsID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right (remoteActorID, _) ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    hashGrantRecip (GrantRecipPerson k) =
        GrantRecipPerson <$> encodeKeyHashid k
-}

offerTicketC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Ticket URIMode
    -> FedURI
    -> ExceptT Text Handler OutboxItemId
offerTicketC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action ticket uTarget = do

    -- Check input
    verifyNothingE maybeCap "Capability not needed"
    (title, desc, source, tam) <- do
        hostLocal <- asksSite siteInstanceHost
        WorkItemOffer {..} <- checkOfferTicket hostLocal ticket uTarget
        unless (wioAuthor == Left senderPersonID) $
            throwE "Offering a Ticket attributed to someone else"
        return (wioTitle, wioDesc, wioSource, wioRest)

    -- Verify that the target tracker is addressed by the Offer
    case tam of
        TAM_Task deckID -> do
            deckHash <- encodeKeyHashid deckID
            unless (actorIsAddressed localRecips $ LocalActorDeck deckHash) $
                throwE "Local target deck not addressed by the Offer"
        TAM_Merge loomID _ -> do
            loomHash <- encodeKeyHashid loomID
            unless (actorIsAddressed localRecips $ LocalActorLoom loomHash) $
                throwE "Local target loom not addressed by the Offer"
        TAM_Remote uTracker _ -> verifyRemoteAddressed remoteRecips uTracker

    senderHash <- encodeKeyHashid senderPersonID
    now <- liftIO getCurrentTime

    -- If tracker is a local loom, and a remote origin repo is specified, fetch
    -- that repo's AP object via HTTP and remember in DB
    maybeLocalTracker <-
        case tam of
            TAM_Task deckID -> pure $ Just $ Left deckID
            TAM_Merge loomID (Merge maybeOriginTip maybeBundle targetTip) -> do
                maybeOrigin <- for maybeOriginTip $ \case
                    TipLocalRepo repoID -> pure $ Left (repoID, Nothing)
                    TipLocalBranch repoID branch -> pure $ Left (repoID, Just branch)
                    TipRemote uOrigin -> Right <$> do
                        (vcs, raid, uClone, mb) <- withExceptT (T.pack . show) $ httpGetRemoteTip uOrigin
                        return (vcs, raid, uClone, first Just <$> mb)
                    TipRemoteBranch uRepo branch -> Right <$> do
                        (vcs, raid, uClone) <- withExceptT (T.pack . show) $ httpGetRemoteRepo uRepo
                        return (vcs, raid, uClone, Just (Nothing, branch))
                originOrBundle <-
                    fromMaybeE
                        (align maybeOrigin maybeBundle)
                        "MR provides neither origin nor patches"
                (targetRepoID, maybeTargetBranch) <-
                    case targetTip of
                        TipLocalRepo repoID -> pure (repoID, Nothing)
                        TipLocalBranch repoID branch -> pure (repoID, Just branch)
                        _ -> throwE "Offer target is a local loom but MR target is a remote repo (Looms serve only local repos)"
                return $ Just $ Right (loomID, originOrBundle, targetRepoID, maybeTargetBranch)
            TAM_Remote _ _ -> pure Nothing

    (offerID, deliverHttpOffer, maybeAcceptMaybePull) <- runDBExcept $ do

        -- If target tracker is local, find it in our DB
        -- If that tracker is a loom, find and check the MR too
        maybeLocalTrackerDB <- for maybeLocalTracker $ bitraverse
            (\ deckID -> do
                deck <- getE deckID "Offer local target no such deck in DB"
                return (deckID, deckActor deck)
            )
            (\ (loomID, originOrBundle, targetRepoID, maybeTargetBranch) -> do
                loom <- getE loomID "Offer local target no such loom in DB"

                unless (targetRepoID == loomRepo loom) $
                    throwE "MR target repo isn't the one served by the Offer target loom"
                targetRepo <- getE targetRepoID "MR target local repo not found in DB"
                unless (repoLoom targetRepo == Just loomID) $
                    throwE "Offer target loom doesn't have repo's consent to serve it"

                for_ (justThere originOrBundle) $ \ (Material typ diffs) -> do
                    unless (repoVcs targetRepo == patchMediaTypeVCS typ) $
                        throwE "Patch type and local target repo VCS mismatch"
                    case (typ, diffs) of
                        (PatchMediaTypeDarcs, _ :| _ : _) ->
                            throwE "More than one Darcs dpatch file provided"
                        _ -> pure ()

                originOrBundle' <-
                    bitraverse
                        (\ origin -> do
                            (vcs, origin') <-
                                case origin of
                                    Left (repoID, maybeBranch) -> do
                                        repo <- getE repoID "MR origin local repo not found in DB"
                                        return (repoVcs repo, Left (repoID, maybeBranch))
                                    Right (vcs, remoteActorID, uClone, maybeBranch) ->
                                        pure (vcs, Right (remoteActorID, uClone, maybeBranch))
                            unless (vcs == repoVcs targetRepo) $
                                throwE "Origin repo VCS differs from target repo VCS"
                            return origin'
                        )
                        pure
                        originOrBundle

                -- Verify that the VCS of target repo, origin repo and patches
                -- all match, and that branches are specified for Git and
                -- aren't specified for Darcs
                tipInfo <- case repoVcs targetRepo of
                    VCSGit -> do
                        targetBranch <- fromMaybeE maybeTargetBranch "Local target repo is Git but no target branch specified"
                        maybeOrigin <- for (justHere originOrBundle') $ \case
                            Left (originRepoID, maybeOriginBranch) -> do
                                originBranch <- fromMaybeE maybeOriginBranch "Local origin repo is Git but no origin branch specified"
                                return (Left originRepoID, originBranch)
                            Right (_remoteActorID, uClone, maybeOriginBranch) -> do
                                (_maybeURI, originBranch) <- fromMaybeE maybeOriginBranch "Remote origin repo is Git but no origin branch specified"
                                return (Right uClone, originBranch)
                        return $ Left (targetBranch, maybeOrigin)
                    VCSDarcs -> do
                        verifyNothingE maybeTargetBranch "Local target repo is Darcs but target branch specified"
                        maybeOriginRepo <- for (justHere originOrBundle') $ \case
                            Left (originRepoID, maybeOriginBranch) -> do
                                verifyNothingE maybeOriginBranch "Local origin repo is Darcs but origin branch specified"
                                return $ Left originRepoID
                            Right (_remoteActorID, uClone, maybeOriginBranch) -> do
                                verifyNothingE maybeOriginBranch "Remote origin repo is Darcs but origin branch specified"
                                return $ Right uClone
                        return $ Right $ maybeOriginRepo

                return (loomID, loomActor loom, originOrBundle', targetRepoID, maybeTargetBranch, tipInfo)
            )

        -- Insert Offer to sender's outbox
        offerID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        luOffer <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) offerID action

        -- Deliver the Offer activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpOffer <- do
            hashRepo <- getEncodeKeyHashid
            let tipRepo tip =
                    case tip of
                        TipLocalRepo repoID -> Just $ hashRepo repoID
                        TipLocalBranch repoID _ -> Just $ hashRepo repoID
                        _ -> Nothing
            hashDeck <- getEncodeKeyHashid
            hashLoom <- getEncodeKeyHashid
            let (tracker, target, origin) =
                    case tam of
                        TAM_Task deckID ->
                            ( Just $ Left $ hashDeck deckID
                            , Nothing
                            , Nothing
                            )
                        TAM_Merge loomID (Merge maybeOriginTip _ targetTip) ->
                            ( Just $ Right $ hashLoom loomID
                            , tipRepo targetTip
                            , tipRepo =<< maybeOriginTip
                            )
                        TAM_Remote _ maybeMerge ->
                            ( Nothing
                            , tipRepo . mergeTarget =<< maybeMerge
                            , tipRepo =<< mergeOrigin =<< maybeMerge
                            )
                sieveActors = catMaybes
                    [ tracker <&> \case
                        Left deckHash -> LocalActorDeck deckHash
                        Right loomHash -> LocalActorLoom loomHash
                    , LocalActorRepo <$> target
                    , LocalActorRepo <$> origin
                    ]
                sieveStages = catMaybes
                    [ tracker <&> \case
                        Left deckHash -> LocalStageDeckFollowers deckHash
                        Right loomHash -> LocalStageLoomFollowers loomHash
                    , LocalStageRepoFollowers <$> target
                    , LocalStageRepoFollowers <$> origin
                    , Just $ LocalStagePersonFollowers senderHash
                    ]
                sieve = makeRecipientSet sieveActors sieveStages
                localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts offerID action

        -- If Offer target is a local deck/loom, verify that it has received
        -- the Offer, insert a new Ticket to DB, and publish Accept
        maybeAcceptMaybePull <- for maybeLocalTrackerDB $ \ tracker -> do

            -- Verify that tracker received the Offer
            let trackerActorID =
                    case tracker of
                        Left (_, actorID) -> actorID
                        Right (_, actorID, _, _, _, _) -> actorID
            verifyActorHasItem trackerActorID offerID "Local tracker didn't receive the Offer"

            -- Insert ticket/MR to DB
            acceptID <- lift $ do
                trackerActor <- getJust trackerActorID
                insertEmptyOutboxItem (actorOutbox trackerActor) now
            (ticketRoute, maybePull) <- lift $ do
                ticketID <- insertTicket now title desc source offerID acceptID
                case tracker of
                    Left (deckID, _) ->
                        (,Nothing) <$> insertTask deckID ticketID
                    Right (loomID, _, originOrBundle, targetRepoID, maybeTargetBranch, tipInfo) -> do
                        (clothID, route) <- insertMerge now loomID ticketID maybeTargetBranch originOrBundle
                        let maybeTipInfo =
                                case tipInfo of
                                    Left (b, mo) -> Left . (b,) <$> mo
                                    Right mo -> Right <$> mo
                            hasBundle = isJust $ justThere originOrBundle
                            pull = (clothID, targetRepoID, hasBundle,) <$> maybeTipInfo
                        return (route, pull)

            -- Insert an Accept activity to tracker's outbox
            hashDeck <- getEncodeKeyHashid
            hashLoom <- getEncodeKeyHashid
            let acceptRecipActors = [LocalActorPerson senderHash]
                acceptRecipStages =
                    [ case tracker of
                        Left (deckID, _) ->
                            LocalStageDeckFollowers $ hashDeck deckID
                        Right (loomID, _, _, _, _, _) ->
                            LocalStageLoomFollowers $ hashLoom loomID
                    , LocalStagePersonFollowers senderHash
                    ]
            actionAccept <- prepareAccept ticketRoute luOffer acceptRecipActors acceptRecipStages
            let trackerByKey =
                    case tracker of
                        Left (deckID, _) -> LocalActorDeck deckID
                        Right (loomID, _, _, _, _, _) -> LocalActorLoom loomID
            _luAccept <- lift $ updateOutboxItem trackerByKey acceptID actionAccept

            -- Deliver the Accept activity to local recipients, and schedule
            -- delivery for unavailable remote recipients
            deliverHttpAccept <- do
                let trackerLocalActor =
                        case tracker of
                            Left (deckID, _) ->
                                LocalActorDeck $ hashDeck deckID
                            Right (loomID, _, _, _, _, _) ->
                                LocalActorLoom $ hashLoom loomID
                    localRecipsAccept =
                        makeRecipientSet acceptRecipActors acceptRecipStages
                deliverActivityDB
                    trackerLocalActor trackerActorID localRecipsAccept [] []
                    acceptID actionAccept

            -- Return instructions for HTTP delivery to remote recipients, and
            -- info for pulling origin branch to generate patches
            return (deliverHttpAccept, maybePull)

        -- Return instructions for HTTP delivery to remote recipients, and info
        -- for pulling origin branch to generate patches
        return (offerID, deliverHttpOffer, maybeAcceptMaybePull)

    -- Launch asynchronous HTTP delivery of Offer and Accept, and generate
    -- patches if we opened a local MR that mentions just an origin
    lift $ forkWorker "offerTicketC: async HTTP Offer delivery" deliverHttpOffer
    for_ maybeAcceptMaybePull $ \ (deliverHttpAccept, maybePull) -> do
        lift $ forkWorker "offerTicketC: async HTTP Accept delivery" deliverHttpAccept
        traverse generatePatches maybePull

    return offerID

    where

    insertTicket now title desc source offerID acceptID = do
        did <- insert Discussion
        fsid <- insert FollowerSet
        tid <- insert Ticket
            { ticketNumber      = Nothing
            , ticketCreated     = now
            , ticketTitle       = title
            , ticketSource      = source
            , ticketDescription = desc
            , ticketDiscuss     = did
            , ticketFollowers   = fsid
            , ticketAccept      = acceptID
            }
        insert_ TicketAuthorLocal
            { ticketAuthorLocalTicket = tid
            , ticketAuthorLocalAuthor = senderPersonID
            , ticketAuthorLocalOpen   = offerID
            }
        return tid

    insertTask deckID ticketID = do
        ticketDeckID <- insert $ TicketDeck ticketID deckID
        TicketR <$> encodeKeyHashid deckID <*> encodeKeyHashid ticketDeckID

    insertMerge
        :: UTCTime
        -> LoomId
        -> TicketId
        -> Maybe Text
        -> These
            (Either
                (RepoId, Maybe Text)
                (RemoteActorId, FedURI, Maybe (Maybe LocalURI, Text))
            )
            Material
        -> AppDB (TicketLoomId, Route App)
    insertMerge now loomID ticketID maybeBranch originOrBundle = do
        clothID <- insert $ TicketLoom ticketID loomID maybeBranch
        for_ (justHere originOrBundle) $ \case
            Left (repoID, maybeOriginBranch) ->
                insert_ $ MergeOriginLocal clothID repoID maybeOriginBranch
            Right (remoteActorID, _uClone, maybeOriginBranch) -> do
                originID <- insert $ MergeOriginRemote clothID remoteActorID
                for_ maybeOriginBranch $ \ (mlu, b) ->
                    insert_ $ MergeOriginRemoteBranch originID mlu b
        for_ (justThere originOrBundle) $ \ (Material typ diffs) -> do
            bundleID <- insert $ Bundle clothID False
            insertMany_ $ NE.toList $ NE.reverse $
                NE.map (Patch bundleID now typ) diffs
        route <- ClothR <$> encodeKeyHashid loomID <*> encodeKeyHashid clothID
        return (clothID, route)

    prepareAccept ticketRoute luOffer actors stages = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return Action
            { actionCapability = Nothing
            , actionSummary    = Nothing
            , actionAudience   = Audience recips [] [] [] [] []
            , actionFulfills   = []
            , actionSpecific   = AcceptActivity Accept
                { acceptObject   = ObjURI hLocal luOffer
                , acceptResult   = Just $ encodeRouteLocal ticketRoute
                }
            }

{-
verifyHosterRecip _           _    (Right _) = return ()
verifyHosterRecip localRecips name (Left wi) =
    fromMaybeE (verify wi) $
        name <> " ticket hoster actor isn't listed as a recipient"
    where
    verify (WorkItemSharerTicket shr _ _) = do
        sharerSet <- lookup shr localRecips
        guard $ localRecipSharer $ localRecipSharerDirect sharerSet
    verify (WorkItemProjectTicket shr prj _) = do
        sharerSet <- lookup shr localRecips
        projectSet <- lookup prj $ localRecipProjectRelated sharerSet
        guard $ localRecipProject $ localRecipProjectDirect projectSet
    verify (WorkItemRepoProposal shr rp _) = do
        sharerSet <- lookup shr localRecips
        repoSet <- lookup rp $ localRecipRepoRelated sharerSet
        guard $ localRecipRepo $ localRecipRepoDirect repoSet
-}

{-
workItemRecipSieve wiFollowers (WorkItemDetail ident context author) =
    let authorC =
            case author of
                Left shr -> [LocalPersonCollectionSharerFollowers shr]
                Right _ -> []
        ticketC =
            case ident of
                Left (wi, _) -> [wiFollowers wi]
                Right _ -> []
        (contextA, contextC) =
            case context of
                Left local ->
                    case local of
                        Left (shr, prj) ->
                            ( [LocalActorProject shr prj]
                            , [ LocalPersonCollectionProjectTeam shr prj
                                , LocalPersonCollectionProjectFollowers shr prj
                                ]
                            )
                        Right (shr, rp) ->
                            ( [LocalActorRepo shr rp]
                            , [ LocalPersonCollectionRepoTeam shr rp
                                , LocalPersonCollectionRepoFollowers shr rp
                                ]
                            )
                Right _ -> ([], [])
    in  (contextA, authorC ++ ticketC ++ contextC)
-}

{-
workItemActor (WorkItemSharerTicket shr _ _) = LocalActorSharer shr
workItemActor (WorkItemProjectTicket shr prj _) = LocalActorProject shr prj
workItemActor (WorkItemRepoProposal shr rp _) = LocalActorRepo shr rp
-}

actorOutboxItem (LocalActorPerson p) = PersonOutboxItemR p
actorOutboxItem (LocalActorGroup _) = error "No outbox for Group yet"
actorOutboxItem (LocalActorRepo r) = RepoOutboxItemR r
actorOutboxItem (LocalActorDeck d) = DeckOutboxItemR d
actorOutboxItem (LocalActorLoom l) = LoomOutboxItemR l

offerDepC
    :: Entity Person
    -> Maybe HTML
    -> Audience URIMode
    -> TicketDependency URIMode
    -> FedURI
    -> ExceptT Text Handler OutboxItemId
offerDepC (Entity pidUser personUser) summary audience dep uTarget = do
    error "offerDepC temporarily disabled"

{-

    let shrUser = sharerIdent sharerUser
    (parent, child) <- checkDepAndTarget dep uTarget
    ParsedAudience localRecips remoteRecips blinded fwdHosts <- do
        mrecips <- parseAudience audience
        fromMaybeE mrecips "Offer Ticket with no recipients"
    federation <- asksSite $ appFederation . appSettings
    unless (federation || null remoteRecips) $
        throwE "Federation disabled, but remote recipients specified"
    verifyHosterRecip localRecips "Parent" parent
    verifyHosterRecip localRecips "Child" child
    now <- liftIO getCurrentTime
    parentDetail <- runWorkerExcept $ getWorkItemDetail "Parent" parent
    childDetail <- runWorkerExcept $ getWorkItemDetail "Child" child
    (obiidOffer, docOffer, remotesHttpOffer, maybeAccept) <- runDBExcept $ do
        (obiid, doc, luOffer) <- lift $ insertOfferToOutbox shrUser now (personOutbox personUser) blinded
        remotesHttpOffer <- do
            wiFollowers <- askWorkItemFollowers
            let sieve =
                    let (parentA, parentC) =
                            workItemRecipSieve wiFollowers parentDetail
                        (childA, childC) =
                            workItemRecipSieve wiFollowers childDetail
                    in  makeRecipientSet
                            (parentA ++ childA)
                            (LocalPersonCollectionSharerFollowers shrUser :
                             parentC ++ childC
                            )
            moreRemoteRecips <-
                lift $
                    deliverLocal'
                        True
                        (LocalActorSharer shrUser)
                        (personInbox personUser)
                        obiid
                        (localRecipSieve sieve False localRecips)
            unless (federation || null moreRemoteRecips) $
                throwE "Federation disabled, but recipient collection remote members found"
            lift $ deliverRemoteDB fwdHosts obiid remoteRecips moreRemoteRecips
        maccept <-
            case (widIdent parentDetail, widIdent childDetail) of
                (Right _, Left (wi, ltid)) -> do
                    mhoster <-
                        lift $ runMaybeT $
                        case wi of
                            WorkItemSharerTicket shr _ _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                personInbox <$>
                                    MaybeT (getValBy $ UniquePersonIdent sid)
                            WorkItemProjectTicket shr prj _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                j <- MaybeT $ getValBy $ UniqueProject prj sid
                                lift $ actorInbox <$> getJust (projectActor j)
                            WorkItemRepoProposal shr rp _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                repoInbox <$>
                                    MaybeT (getValBy $ UniqueRepo rp sid)
                    ibidHoster <- fromMaybeE mhoster "Child hoster not in DB"
                    ibiid <- do
                        mibil <- lift $ getValBy $ UniqueInboxItemLocal ibidHoster obiid
                        inboxItemLocalItem <$>
                            fromMaybeE mibil "Child hoster didn't receive the Offer to their inbox in DB"
                    lift $ insert_ TicketDependencyOffer
                        { ticketDependencyOfferOffer = ibiid
                        , ticketDependencyOfferChild = ltid
                        }
                    return Nothing
                (Right _, Right _) -> return Nothing
                (Left (wi, ltidParent), _) -> Just <$> do
                    mhoster <-
                        lift $ runMaybeT $
                        case wi of
                            WorkItemSharerTicket shr _ _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                p <- MaybeT (getValBy $ UniquePersonIdent sid)
                                return (personOutbox p, personInbox p)
                            WorkItemProjectTicket shr prj _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                j <- MaybeT $ getValBy $ UniqueProject prj sid
                                a <- lift $ getJust $ projectActor j
                                return (actorOutbox a, actorInbox a)
                            WorkItemRepoProposal shr rp _ -> do
                                sid <- MaybeT $ getKeyBy $ UniqueSharer shr
                                r <- MaybeT (getValBy $ UniqueRepo rp sid)
                                return (repoOutbox r, repoInbox r)
                    (obidHoster, ibidHoster) <- fromMaybeE mhoster "Parent hoster not in DB"
                    obiidAccept <- lift $ insertEmptyOutboxItem obidHoster now
                    tdid <- lift $ insertDep now pidUser obiid ltidParent (widIdent childDetail) obiidAccept
                    (docAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept) <-
                        lift $ insertAccept shrUser wi parentDetail childDetail obiid obiidAccept tdid
                    knownRemoteRecipsAccept <-
                        lift $
                        deliverLocal'
                            False
                            (workItemActor wi)
                            ibidHoster
                            obiidAccept
                            localRecipsAccept
                    lift $ (obiidAccept,docAccept,fwdHostsAccept,) <$> deliverRemoteDB fwdHostsAccept obiidAccept remoteRecipsAccept knownRemoteRecipsAccept
        return (obiid, doc, remotesHttpOffer, maccept)
    lift $ do
        forkWorker "offerDepC: async HTTP Offer delivery" $ deliverRemoteHttp' fwdHosts obiidOffer docOffer remotesHttpOffer
        for_ maybeAccept $ \ (obiidAccept, docAccept, fwdHostsAccept, remotesHttpAccept) ->
            forkWorker "offerDepC: async HTTP Accept delivery" $ deliverRemoteHttp' fwdHostsAccept obiidAccept docAccept remotesHttpAccept
    return obiidOffer
    where
    insertOfferToOutbox shrUser now obid blinded = do
        hLocal <- asksSite siteInstanceHost
        obiid <- insertEmptyOutboxItem obid now
        encodeRouteLocal <- getEncodeRouteLocal
        obikhid <- encodeKeyHashid obiid
        let luAct = encodeRouteLocal $ SharerOutboxItemR shrUser obikhid
            doc = Doc hLocal Activity
                { activityId       = Just luAct
                , activityActor    = encodeRouteLocal $ SharerR shrUser
                , activityCapability = Nothing
                , activitySummary  = summary
                , activityAudience = blinded
                , activitySpecific =
                    OfferActivity $ Offer (OfferDep dep) uTarget
                }
        update obiid [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return (obiid, doc, luAct)
    insertDep now pidAuthor obiidOffer ltidParent child obiidAccept = do
        tdid <- insert LocalTicketDependency
            { localTicketDependencyParent  = ltidParent
            , localTicketDependencyCreated = now
            , localTicketDependencyAccept  = obiidAccept
            }
        case child of
            Left (_wi, ltid) -> insert_ TicketDependencyChildLocal
                { ticketDependencyChildLocalDep   = tdid
                , ticketDependencyChildLocalChild = ltid
                }
            Right (ObjURI h lu, _luFollowers) -> do
                iid <- either entityKey id <$> insertBy' (Instance h)
                roid <- either entityKey id <$> insertBy' (RemoteObject iid lu)
                insert_ TicketDependencyChildRemote
                    { ticketDependencyChildRemoteDep   = tdid
                    , ticketDependencyChildRemoteChild = roid
                    }
        insert_ TicketDependencyAuthorLocal
            { ticketDependencyAuthorLocalDep    = tdid
            , ticketDependencyAuthorLocalAuthor = pidAuthor
            , ticketDependencyAuthorLocalOpen   = obiidOffer
            }
        return tdid
    workItemActor (WorkItemSharerTicket shr _ _) = LocalActorSharer shr
    workItemActor (WorkItemProjectTicket shr prj _) = LocalActorProject shr prj
    workItemActor (WorkItemRepoProposal shr rp _) = LocalActorRepo shr rp
    insertAccept shrUser wiParent (WorkItemDetail _ parentCtx parentAuthor) (WorkItemDetail childId childCtx childAuthor) obiidOffer obiidAccept tdid = do
        encodeRouteLocal <- getEncodeRouteLocal
        encodeRouteHome <- getEncodeRouteHome
        wiFollowers <- askWorkItemFollowers
        hLocal <- asksSite siteInstanceHost

        obikhidOffer <- encodeKeyHashid obiidOffer
        obikhidAccept <- encodeKeyHashid obiidAccept
        tdkhid <- encodeKeyHashid tdid

        let audAuthor =
                AudLocal
                    [LocalActorSharer shrUser]
                    [LocalPersonCollectionSharerFollowers shrUser]
            audParentContext = contextAudience parentCtx
            audChildContext = contextAudience childCtx
            audParentAuthor = authorAudience parentAuthor
            audParentFollowers = AudLocal [] [wiFollowers wiParent]
            audChildAuthor = authorAudience childAuthor
            audChildFollowers =
                case childId of
                    Left (wi, _ltid) -> AudLocal [] [wiFollowers wi]
                    Right (ObjURI h _, luFollowers) -> AudRemote h [] [luFollowers]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience $
                    audAuthor :
                    audParentAuthor :
                    audParentFollowers :
                    audChildAuthor :
                    audChildFollowers :
                    audParentContext ++ audChildContext

            actor = workItemActor wiParent
            recips = map encodeRouteHome audLocal ++ audRemote
            doc = Doc hLocal Activity
                { activityId       =
                    Just $ encodeRouteLocal $
                        actorOutboxItem actor obikhidAccept
                , activityActor    = encodeRouteLocal $ renderLocalActor actor
                , activityCapability = Nothing
                , activitySummary  = Nothing
                , activityAudience = Audience recips [] [] [] [] []
                , activitySpecific = AcceptActivity Accept
                    { acceptObject =
                        encodeRouteHome $ SharerOutboxItemR shrUser obikhidOffer
                    , acceptResult =
                        Just $ encodeRouteLocal $ TicketDepR tdkhid
                    }
                }

        update obiidAccept [OutboxItemActivity =. persistJSONObjectFromDoc doc]
        return (doc, recipientSet, remoteActors, fwdHosts)
-}

{-
insertAcceptOnTicketStatus shrUser wi (WorkItemDetail _ ctx author) obiidResolve obiidAccept = do
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    wiFollowers <- askWorkItemFollowers
    hLocal <- asksSite siteInstanceHost

    obikhidResolve <- encodeKeyHashid obiidResolve
    obikhidAccept <- encodeKeyHashid obiidAccept

    let audAuthor =
            AudLocal
                [LocalActorSharer shrUser]
                [LocalPersonCollectionSharerFollowers shrUser]
        audTicketContext = contextAudience ctx
        audTicketAuthor = authorAudience author
        audTicketFollowers = AudLocal [] [wiFollowers wi]

        (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
            collectAudience $
                audAuthor :
                audTicketAuthor :
                audTicketFollowers :
                audTicketContext

        actor = workItemActor wi
        recips = map encodeRouteHome audLocal ++ audRemote
        doc = Doc hLocal Activity
            { activityId       =
                Just $ encodeRouteLocal $
                    actorOutboxItem actor obikhidAccept
            , activityActor    = encodeRouteLocal $ renderLocalActor actor
            , activityCapability = Nothing
            , activitySummary  = Nothing
            , activityAudience = Audience recips [] [] [] [] []
            , activitySpecific = AcceptActivity Accept
                { acceptObject =
                    encodeRouteHome $ SharerOutboxItemR shrUser obikhidResolve
                , acceptResult = Nothing
                }
            }

    update obiidAccept [OutboxItemActivity =. persistJSONObjectFromDoc doc]
    return (doc, recipientSet, remoteActors, fwdHosts)
-}

resolveC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Resolve URIMode
    -> ExceptT Text Handler OutboxItemId
resolveC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action (AP.Resolve uObject) = do

    -- Check input
    maybeLocalWorkItem <-
        nameExceptT "Resolve object" $ either Just (const Nothing) <$> do
            routeOrRemote <- parseFedURIOld uObject
            bitraverse
                (\ r -> do
                    wiByHash <-
                        fromMaybeE (parseWorkItem r) "Not a work item route"
                    unhashWorkItemE wiByHash "Work item invalid keyhashid"
                )
                pure
                routeOrRemote
    capID <- fromMaybeE maybeCap "No capability provided"

    -- Verify that the work item's tracker is addressed
    for_ maybeLocalWorkItem $ \ wi -> do
        trackerByHash <- hashLocalActor $ workItemActor wi
        unless (actorIsAddressed localRecips trackerByHash) $
            throwE "Work item's tracker not addressed by the Resolve"

    senderHash <- encodeKeyHashid senderPersonID
    now <- liftIO getCurrentTime

    (resolveID, deliverHttpResolve, maybeDeliverHttpAccept) <- runDBExcept $ do

        workItemDB <- for maybeLocalWorkItem $ \ wi -> do

            -- Find the work item and its tracker in DB, and verify the work
            -- item isn't already resolved
            (resource, actor, ticketID) <-
                case wi of
                    WorkItemTicket deckID taskID -> do
                        maybeTicket <- lift $ getTicket deckID taskID
                        (Entity _ deck, _task, Entity ticketID _, _author, resolve) <-
                            fromMaybeE maybeTicket "No such ticket in DB"
                        verifyNothingE resolve "Ticket already resolved"
                        actor <- lift $ getJustEntity $ deckActor deck
                        return (GrantResourceDeck deckID, actor, ticketID)
                    WorkItemCloth loomID clothID -> do
                        maybeCloth <- lift $ getCloth loomID clothID
                        (Entity _ loom, _cloth, Entity ticketID _, _author, resolve, _merge) <-
                            fromMaybeE maybeCloth "No such MR in DB"
                        verifyNothingE resolve "MR already resolved"
                        actor <- lift $ getJustEntity $ loomActor loom
                        return (GrantResourceLoom loomID, actor, ticketID)

            -- Verify the sender is authorized by the tracker to resolve work
            -- items
            capability <-
                case capID of
                    Left (capActor, _, capItem) -> return (capActor, capItem)
                    Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local tracker"
            verifyCapability capability (Left senderPersonID) resource

            return (wi, actor, ticketID)

        -- Insert Resolve to sender's outbox
        resolveID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        luResolve <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) resolveID action

        -- Deliver the Resolve activity to local recipients, and schedule
        -- delivery for unavailable remote recipients
        deliverHttpResolve <- do
            sieve <- do
                (actors, stages) <-
                    case maybeLocalWorkItem of
                        Nothing -> pure ([], [])
                        Just (WorkItemTicket deckID taskID) -> do
                            deckHash <- encodeKeyHashid deckID
                            taskHash <- encodeKeyHashid taskID
                            return
                                ( [LocalActorDeck deckHash]
                                , [ LocalStageDeckFollowers deckHash
                                  , LocalStageTicketFollowers deckHash taskHash
                                  ]
                                )
                        Just (WorkItemCloth loomID clothID) -> do
                            loomHash <- encodeKeyHashid loomID
                            clothHash <- encodeKeyHashid clothID
                            return
                                ( [LocalActorLoom loomHash]
                                , [ LocalStageLoomFollowers loomHash
                                  , LocalStageClothFollowers loomHash clothHash
                                  ]
                                )
                let stages' = LocalStagePersonFollowers senderHash : stages
                return $ makeRecipientSet actors stages'
            let localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts resolveID action

        -- Verify that the tracker has received the Resolve, resolve the work
        -- item in DB, and publish Accept
        maybeDeliverHttpAccept <- for workItemDB $ \ (wi, Entity trackerActorID trackerActor, ticketID) -> do

            -- Verify tracker received the Resolve
            verifyActorHasItem
                trackerActorID
                resolveID
                "Local tracker didn't receive the Resolve"

            -- Mark work item in DB as resolved by the Resolve
            acceptID <-
                lift $ insertEmptyOutboxItem (actorOutbox trackerActor) now
            lift $ insertResolve ticketID resolveID acceptID

            -- Insert an Accept activity to tracker's outbox
            trackerStages <-
                case wi of
                    WorkItemTicket deckID taskID -> do
                        deckHash <- encodeKeyHashid deckID
                        taskHash <- encodeKeyHashid taskID
                        return
                            [ LocalStageDeckFollowers deckHash
                            , LocalStageTicketFollowers deckHash taskHash
                            ]
                    WorkItemCloth loomID clothID -> do
                        loomHash <- encodeKeyHashid loomID
                        clothHash <- encodeKeyHashid clothID
                        return
                            [ LocalStageLoomFollowers loomHash
                            , LocalStageClothFollowers loomHash clothHash
                            ]
            let acceptActors = [LocalActorPerson senderHash]
                acceptStages =
                    LocalStagePersonFollowers senderHash : trackerStages
            actionAccept <- prepareAccept luResolve acceptActors acceptStages
            let trackerByKey = workItemActor wi
            _ <- lift $ updateOutboxItem trackerByKey acceptID actionAccept

            -- Deliver the Accept activity to local recipients, and schedule
            -- delivery for unavailable remote recipients
            let localRecipsAccept = makeRecipientSet acceptActors acceptStages
            trackerByHash <- hashLocalActor trackerByKey
            deliverActivityDB
                trackerByHash trackerActorID localRecipsAccept [] []
                acceptID actionAccept

        -- Return instructions for HTTP delivery of Resolve and Accept to
        -- remote recipients
        return
            ( resolveID
            , deliverHttpResolve
            , maybeDeliverHttpAccept
            )

    -- Launch asynchronous HTTP delivery of Resolve and Accept
    lift $ do
        forkWorker "resolveC: async HTTP Resolve delivery" deliverHttpResolve
        for_ maybeDeliverHttpAccept $
            forkWorker "resolveC: async HTTP Accept delivery"

    return resolveID

    where

    insertResolve ticketID resolveID acceptID = do
        trid <- insert TicketResolve
            { ticketResolveTicket = ticketID
            , ticketResolveAccept = acceptID
            }
        insert_ TicketResolveLocal
            { ticketResolveLocalTicket   = trid
            , ticketResolveLocalActivity = resolveID
            }

    prepareAccept luResolve actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return AP.Action
            { AP.actionCapability = Nothing
            , AP.actionSummary    = Nothing
            , AP.actionAudience   = Audience recips [] [] [] [] []
            , AP.actionFulfills   = []
            , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                { AP.acceptObject   = ObjURI hLocal luResolve
                , AP.acceptResult   = Nothing
                }
            }

undoC
    :: Entity Person
    -> Actor
    -> Maybe
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
    -> RecipientRoutes
    -> [(Host, NonEmpty LocalURI)]
    -> [Host]
    -> AP.Action URIMode
    -> AP.Undo URIMode
    -> ExceptT Text Handler OutboxItemId
undoC (Entity senderPersonID senderPerson) senderActor maybeCap localRecips remoteRecips fwdHosts action (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI uObject

    now <- liftIO getCurrentTime
    senderHash <- encodeKeyHashid senderPersonID

    (undoID, deliverHttpUndo, maybeDeliverHttpAccept) <- runDBExcept $ do

        -- Find the undone activity in our DB
        undoneDB <- do
            a <- getActivity undone
            fromMaybeE a "Can't find undone in DB"

        -- See if the undone activity is a Follow/Resolve on a local target
        -- If it is, verify the relevant actor is addressed, verify
        -- permissions, and perform the actual undoing in the DB
        maybeUndoLocal <- do
            maybeUndo <-
                lift $ runMaybeT $
                    Left <$> MaybeT (tryUnfollow undoneDB) <|>
                    Right <$> MaybeT (tryUnresolve undoneDB)
            case maybeUndo of
                Nothing -> pure Nothing
                Just (Left (updateDB, actorID, Left followerSetID)) -> do
                    actorByKey <- lift $ getLocalActor actorID
                    unless (actorByKey == LocalActorPerson senderPersonID) $
                        throwE "Tryin to undo a Follow of someone else"
                    (fByKey, fActorID, _) <- do
                        followee <- lift $ getFollowee' followerSetID
                        getFollowee followee
                    fByHash <- hashLocalActor fByKey
                    unless (actorIsAddressed localRecips fByHash) $
                        throwE "Followee's actor not addressed by the Undo"
                    lift updateDB
                    fActor <- lift $ getJust fActorID
                    return $ Just
                        ( fByKey
                        , Entity fActorID fActor
                        , makeRecipientSet
                            [fByHash]
                            [LocalStagePersonFollowers senderHash]
                        , [LocalActorPerson senderHash]
                        , []
                        )
                Just (Left (updateDB, actorID, Right uTarget)) -> do
                    actorByKey <- lift $ getLocalActor actorID
                    unless (actorByKey == LocalActorPerson senderPersonID) $
                        throwE "Trying to undo a Follow of someone else"
                    verifyRemoteAddressed remoteRecips uTarget
                    lift updateDB
                    return Nothing
                Just (Right (updateDB, ticketID)) -> do
                    wiByKey <- lift $ getWorkItem ticketID
                    wiByHash <- hashWorkItem wiByKey
                    let resource = workItemResource wiByKey
                        actorByKey = workItemActor wiByKey
                        actorByHash = workItemActor wiByHash
                    unless (actorIsAddressed localRecips actorByHash) $
                        throwE "Work item's actor not addressed by the Undo"
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local tracker"
                    verifyCapability capability (Left senderPersonID) resource
                    lift updateDB
                    actorID <- do
                        maybeActor <- lift $ getLocalActorEntity actorByKey
                        case localActorID <$> maybeActor of
                            Nothing -> error "Actor entity not in DB"
                            Just aid -> pure aid
                    actor <- lift $ getJust actorID
                    return $ Just
                        ( actorByKey
                        , Entity actorID actor
                        , makeRecipientSet
                            [actorByHash]
                            [ localActorFollowers actorByHash
                            , workItemFollowers wiByHash
                            , LocalStagePersonFollowers senderHash
                            ]
                        , [LocalActorPerson senderHash]
                        , [ localActorFollowers actorByHash
                          , workItemFollowers wiByHash
                          , LocalStagePersonFollowers senderHash
                          ]
                        )

        -- Insert the Undo activity to author's outbox
        undoID <- lift $ insertEmptyOutboxItem (actorOutbox senderActor) now
        luUndo <- lift $ updateOutboxItem (LocalActorPerson senderPersonID) undoID action

        -- Deliver the Undo activity to local recipients, and schedule delivery
        -- for unavailable remote recipients
        deliverHttpUndo <- do
            let sieve =
                    case maybeUndoLocal of
                        Nothing ->
                            makeRecipientSet
                                [] [LocalStagePersonFollowers senderHash]
                        Just (_, _, s, _, _) -> s
                localRecipsFinal = localRecipSieve sieve False localRecips
            deliverActivityDB
                (LocalActorPerson senderHash) (personActor senderPerson)
                localRecipsFinal remoteRecips fwdHosts undoID action

        maybeDeliverHttpAccept <- for maybeUndoLocal $ \ (actorByKey, Entity actorID actor, _, acceptActors, acceptStages) -> do

            -- Verify the relevant actor has received the Undp
            verifyActorHasItem actorID undoID "Actor didn't receive the Undo"

            -- Insert an Accept activity to actor's outbox
            acceptID <- lift $ insertEmptyOutboxItem (actorOutbox actor) now
            actionAccept <- prepareAccept luUndo acceptActors acceptStages
            _luAccept <- lift $ updateOutboxItem actorByKey acceptID actionAccept

            -- Deliver the Accept activity to local recipients, and schedule
            -- delivery for unavailable remote recipients
            let localRecipsAccept = makeRecipientSet acceptActors acceptStages
            actorByHash <- hashLocalActor actorByKey
            deliverActivityDB
                actorByHash actorID localRecipsAccept [] []
                acceptID actionAccept

        -- Return instructions for HTTP delivery to remote recipients
        return (undoID, deliverHttpUndo, maybeDeliverHttpAccept)

    -- Launch asynchronous HTTP delivery of Undo and Accept
    lift $ do
        forkWorker "undoC: async HTTP Undo delivery" deliverHttpUndo
        for_ maybeDeliverHttpAccept $
            forkWorker "undoC: async HTTP Accept delivery"

    return undoID

    where

    prepareAccept luUndo actors stages = do
        encodeRouteHome <- getEncodeRouteHome
        hLocal <- asksSite siteInstanceHost
        let recips =
                map encodeRouteHome $
                        map renderLocalActor actors ++
                        map renderLocalStage stages
        return AP.Action
            { AP.actionCapability = Nothing
            , AP.actionSummary    = Nothing
            , AP.actionAudience   = Audience recips [] [] [] [] []
            , AP.actionFulfills   = []
            , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                { AP.acceptObject   = ObjURI hLocal luUndo
                , AP.acceptResult   = Nothing
                }
            }
