{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2020, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Persist.Discussion
    ( MessageTreeNodeAuthor (..)
    , MessageTreeNode (..)
    , getDiscussionTree
    , getLocalParentMessageId
    , getMessageParent
    , getMessageFromID
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Graph.Inductive.Graph (mkGraph, lab')
import Data.Graph.Inductive.PatriciaTree (Gr)
import Data.Graph.Inductive.Query.DFS (dffWith)
import Data.Maybe (isNothing, mapMaybe)
import Data.Text (Text)
import Data.Tree (Forest)
import Database.Esqueleto hiding (isNothing)
import Yesod.Core
import Yesod.Core.Content
import Yesod.Persist.Core

import qualified Data.HashMap.Lazy as M (fromList, lookup)
import qualified Database.Esqueleto as E

import Network.FedURI
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Tree.Local (sortForestOn)
import Database.Persist.Local

import Vervis.Data.Actor
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Persist.Actor
import Vervis.Recipient

data MessageTreeNodeAuthor
    = MessageTreeNodeLocal LocalMessageId (LocalActorBy Key) Text Text
    | MessageTreeNodeRemote Host LocalURI LocalURI (Maybe Text)

data MessageTreeNode = MessageTreeNode
    { mtnMessageId :: MessageId
    , mtnMessage   :: Message
    , mtnAuthor    :: MessageTreeNodeAuthor
    }

getLocalAuthor lmid aid name = do
    authorByKey <- getLocalActor aid
    code <-
        case authorByKey of
            LocalActorPerson personID -> do
                person <- getJust personID
                return $ "~" <> username2text (personUsername person)
            LocalActorGroup groupID -> do
                groupHash <- encodeKeyHashid groupID
                return $ "&" <> keyHashidText groupHash
            LocalActorRepo repoID -> do
                repoHash <- encodeKeyHashid repoID
                return $ "^" <> keyHashidText repoHash
            LocalActorDeck deckID -> do
                deckHash <- encodeKeyHashid deckID
                return $ "=" <> keyHashidText deckHash
            LocalActorLoom loomID -> do
                loomHash <- encodeKeyHashid loomID
                return $ "+" <> keyHashidText loomHash
    return $ MessageTreeNodeLocal lmid authorByKey code name

getAllMessages :: AppDB DiscussionId -> Handler [MessageTreeNode]
getAllMessages getdid = runDB $ do
    did <- getdid
    l <- select $ from $ \ (lm `InnerJoin` m `InnerJoin` a) -> do
            on $ lm ^. LocalMessageAuthor ==. a ^. ActorId
            on $ lm ^. LocalMessageRest ==. m ^. MessageId
            where_ $ m ^. MessageRoot ==. val did
            return
                ( m
                , lm ^. LocalMessageId
                , lm ^. LocalMessageAuthor
                , a ^. ActorName
                )
    r <- select $ from $ \ (rm `InnerJoin` m `InnerJoin` ra `InnerJoin` ro `InnerJoin` i `InnerJoin` ro2) -> do
        on $ rm ^. RemoteMessageIdent ==. ro2 ^. RemoteObjectId
        on $ ro ^. RemoteObjectInstance ==. i ^. InstanceId
        on $ ra ^. RemoteActorIdent ==. ro ^. RemoteObjectId
        on $ rm ^. RemoteMessageAuthor ==. ra ^. RemoteActorId
        on $ rm ^. RemoteMessageRest ==. m ^. MessageId
        where_ $ m ^. MessageRoot ==. val did
        return
            ( m
            , i ^. InstanceHost
            , ro2 ^. RemoteObjectIdent
            , ro ^. RemoteObjectIdent
            , ra ^. RemoteActorName
            )
    locals <- traverse mklocal l
    let remotes = map mkremote r
    return $ locals ++ remotes
    where
    mklocal (Entity mid m, Value lmid, Value aid, Value name) = do
        author <- getLocalAuthor lmid aid name
        return $ MessageTreeNode mid m author
    mkremote (Entity mid m, Value h, Value luMsg, Value luAuthor, Value name) =
        MessageTreeNode mid m $ MessageTreeNodeRemote h luMsg luAuthor name

discussionTree :: [MessageTreeNode] -> Forest MessageTreeNode
discussionTree mss =
    let nodes = zip [1..] mss
        mkEntry n mtn = (mtnMessageId mtn, n)
        nodeMap = M.fromList $ map (uncurry mkEntry) nodes
        mkEdge n mtn =
            case messageParent $ mtnMessage mtn of
                Nothing  -> Nothing
                Just mid ->
                    case M.lookup mid nodeMap of
                        Nothing -> error "message parent not in discussion"
                        Just p  -> Just (p, n, ())
        edges = mapMaybe (uncurry mkEdge) nodes
        graph = mkGraph nodes edges :: Gr MessageTreeNode ()
        roots =
            [n | (n, mtn) <- nodes, isNothing $ messageParent $ mtnMessage mtn]
    in  dffWith lab' roots graph

sortByTime :: Forest MessageTreeNode -> Forest MessageTreeNode
sortByTime = sortForestOn $ messageCreated . mtnMessage

-- | Get the tree of messages in a given discussion, with siblings sorted from
-- old to new.
getDiscussionTree :: AppDB DiscussionId -> Handler (Forest MessageTreeNode)
getDiscussionTree getdid =
    sortByTime . discussionTree <$> getAllMessages getdid

getMessageFromRoute
    :: MonadIO m
    => LocalActorBy Key
    -> LocalMessageId
    -> ExceptT Text (ReaderT SqlBackend m)
        ( LocalActorBy Entity
        , Entity Actor
        , Entity LocalMessage
        , Entity Message
        )
getMessageFromRoute authorByKey localMsgID = do
    authorByEntity <- do
        maybeActor <- lift $ getLocalActorEntity authorByKey
        fromMaybeE maybeActor "No such author in DB"
    let actorID = localActorID authorByEntity
    actor <- lift $ getJust actorID
    localMsg <- do
        mlm <- lift $ get localMsgID
        fromMaybeE mlm "No such lmid in DB"
    unless (localMessageAuthor localMsg == actorID) $
        throwE "No such message, lmid mismatches author"
    let msgID = localMessageRest localMsg
    msg <- lift $ getJust msgID
    return
        ( authorByEntity
        , Entity actorID actor
        , Entity localMsgID localMsg
        , Entity msgID msg
        )

getLocalParentMessageId
    :: MonadIO m
    => DiscussionId
    -> (LocalActorBy Key, LocalMessageId)
    -> ExceptT Text (ReaderT SqlBackend m) MessageId
getLocalParentMessageId discussionID (authorByKey, localMsgID) = do
    (_, _, _, Entity msgID msg) <- getMessageFromRoute authorByKey localMsgID
    unless (messageRoot msg == discussionID) $
        throwE "Local parent belongs to a different discussion"
    return msgID

-- | Given the parent specified by the Note we received, check if we already
-- know and have this parent note in the DB, and whether the child and parent
-- belong to the same discussion root.
getMessageParent
    :: MonadIO m
    => DiscussionId
    -> Either (LocalActorBy Key, LocalMessageId) FedURI
    -> ExceptT Text (ReaderT SqlBackend m) (Either MessageId FedURI)
getMessageParent did (Left msg) = Left <$> getLocalParentMessageId did msg
getMessageParent did (Right p@(ObjURI hParent luParent)) = do
    mrm <- lift $ runMaybeT $ do
        iid <- MaybeT $ getKeyBy $ UniqueInstance hParent
        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid luParent
        MaybeT $ getValBy $ UniqueRemoteMessageIdent roid
    case mrm of
        Just rm -> Left <$> do
            let mid = remoteMessageRest rm
            m <- lift $ getJust mid
            unless (messageRoot m == did) $
                throwE "Remote parent belongs to a different discussion"
            return mid
        Nothing -> return $ Right p

getMessageFromID :: AppDB DiscussionId -> MessageId -> AppDB MessageTreeNode
getMessageFromID getdid mid = do
    did <- getdid
    m <- get404 mid
    unless (messageRoot m == did) notFound
    mlocal <- getBy $ UniqueLocalMessage mid
    mremote <- getBy $ UniqueRemoteMessage mid
    author <- case (mlocal, mremote) of
        (Nothing, Nothing) -> fail "Message with no author"
        (Just _, Just _) -> fail "Message used as both local and remote"
        (Just (Entity lmid lm), Nothing) -> do
            let actorID = localMessageAuthor lm
            name <- actorName <$> getJust actorID
            getLocalAuthor lmid actorID name
        (Nothing, Just (Entity _rmid rm)) -> do
            ra <- getJust $ remoteMessageAuthor rm
            roA <- getJust $ remoteActorIdent ra
            roM <- getJust $ remoteMessageIdent rm
            i <- getJust $ remoteObjectInstance roA
            return $
                MessageTreeNodeRemote
                    (instanceHost i)
                    (remoteObjectIdent roM)
                    (remoteObjectIdent roA)
                    (remoteActorName ra)
    return $ MessageTreeNode mid m author
