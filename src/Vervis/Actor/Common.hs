{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE RankNTypes #-}

module Vervis.Actor.Common
    ( actorFollow
    , topicAccept
    , topicReject
    , topicInvite
    , topicJoin
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Ticket

actorFollow
    :: (PersistRecordBackend r SqlBackend, ToBackendKey SqlBackend r)
    => (Route App -> ActE a)
    -> (r -> ActorId)
    -> Bool
    -> (Actor -> a -> ActDBE FollowerSetId)
    -> (a -> ActDB RecipientRoutes)
    -> (forall f. f r -> LocalActorBy f)
    -> (a -> Act [Aud URIMode])
    -> UTCTime
    -> Key r
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
actorFollow parseFollowee grabActor unread getFollowee getSieve makeLocalActor makeAudience now recipID author body mfwd luFollow (AP.Follow uObject _ hide) = do

    -- Check input
    followee <- nameExceptT "Follow object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not me/mine"
        parseFollowee route
    verifyNothingE
        (AP.activityCapability $ actbActivity body)
        "Capability not needed"

    maybeFollow <- withDBExcept $ do

        -- Find recipient actor in DB
        recip <- lift $ getJust recipID
        let recipActorID = grabActor recip
        recipActor <- lift $ getJust recipActorID

        -- Insert the Follow to actor's inbox
        mractid <- lift $ insertToInbox now author body (actorInbox recipActor) luFollow unread
        for mractid $ \ followID -> do

            -- Find followee in DB
            followerSetID <- getFollowee recipActor followee

            -- Verify not already following us
            let followerID = remoteAuthorId author
            maybeFollow <-
                lift $ getBy $ UniqueRemoteFollow followerID followerSetID
            verifyNothingE maybeFollow "You're already following this object"

            -- Record the new follow in DB
            acceptID <-
                lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
            lift $ insert_ $ RemoteFollow followerID followerSetID (not hide) followID acceptID

            -- Prepare an Accept activity and insert to actor's outbox
            accept@(actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept) <-
                lift $ prepareAccept followee
            _luAccept <- lift $ updateOutboxItem' (makeLocalActor recipID) acceptID actionAccept

            sieve <- lift $ getSieve followee
            return (recipActorID, followID, acceptID, sieve, accept)

    case maybeFollow of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, followID, acceptID, sieve, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            lift $ for_ mfwd $ \ (localRecips, sig) ->
                forwardActivity
                    (actbBL body) localRecips sig actorID
                    (makeLocalActor recipID) sieve
                    (EventRemoteFollowLocalRecipFwdToFollower followID)
            lift $ sendActivity
                (makeLocalActor recipID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID
                EventAcceptRemoteFollow actionAccept
            done "Recorded Follow and published Accept"

    where

    prepareAccept followee = do
        encodeRouteHome <- getEncodeRouteHome

        ra <- getJust $ remoteAuthorId author

        let ObjURI hAuthor luAuthor = remoteAuthorURI author

            audSender =
                AudRemote hAuthor
                    [luAuthor]
                    (maybeToList $ remoteActorFollowers ra)

        audsRecip <- lift $ makeAudience followee

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience $ audSender : audsRecip

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = ObjURI hAuthor luFollow
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

topicAccept
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ActorId)
    -> (forall f. f topic -> GrantResourceBy f)
    -> UTCTime
    -> Key topic
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
topicAccept topicActor topicResource now recipKey author body mfwd luAccept accept = do

    -- Check input
    acceptee <- parseAccept accept

    -- Verify the capability URI is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    maybeNew <- withDBExcept $ do

        -- Grab recipient deck from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust recipKey
            let actorID = topicActor recip
            (actorID,) <$> getJust actorID

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        collab <- do
            maybeCollab <-
                lift $ runMaybeT $
                    Left <$> tryInvite accepteeDB <|>
                    Right <$> tryJoin accepteeDB
            fromMaybeE maybeCollab "Accepted activity isn't an Invite or Join I'm aware of"

        -- Find the local resource and verify it's me
        collabID <-
            lift $ case collab of
                Left (fulfillsID, _) ->
                    collabFulfillsInviteCollab <$> getJust fulfillsID
                Right (fulfillsID, _) ->
                    collabFulfillsJoinCollab <$> getJust fulfillsID
        topic <- lift $ getCollabTopic collabID
        unless (topicResource recipKey == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

        idsForAccept <-
            case collab of

                -- If accepting an Invite, find the Collab recipient and verify
                -- it's the sender of the Accept
                Left (fulfillsID, _) -> Left <$> do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case recip of
                        Right (Entity crrid crr)
                            | collabRecipRemoteActor crr == remoteAuthorId author -> return (fulfillsID, crrid)
                        _ -> throwE "Accepting an Invite whose recipient is someone else"

                -- If accepting a Join, verify accepter has permission
                Right (fulfillsID, _) -> Right <$> do
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                    verifyCapability
                        capability
                        (Right $ remoteAuthorId author)
                        (topicResource recipKey)
                    return fulfillsID

        -- Verify the Collab isn't already validated
        maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
        verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

        mractid <- lift $ insertToInbox now author body (actorInbox recipActor) luAccept False
        for mractid $ \ acceptID -> do

            -- Record the Accept on the Collab
            case idsForAccept of
                Left (fulfillsID, recipID) -> do
                    maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                    unless (isNothing maybeAccept) $ do
                        lift $ delete acceptID
                        throwE "This Invite already has an Accept by recip"
                Right fulfillsID -> do
                    maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                    unless (isNothing maybeAccept) $ do
                        lift $ delete acceptID
                        throwE "This Join already has an Accept"

            -- Prepare forwarding of Accept to my followers
            let recipByID = grantResourceLocalActor $ topicResource recipKey
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]
                isInvite = isLeft collab

            grantInfo <- do

                -- Enable the Collab in our DB
                grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                lift $ insert_ $ CollabEnable collabID grantID

                -- Prepare a Grant activity and insert to my outbox
                let inviterOrJoiner = either snd snd collab
                grant@(actionGrant, _, _, _) <-
                    lift $ prepareGrant isInvite inviterOrJoiner
                let recipByKey = grantResourceLocalActor $ topicResource recipKey
                _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant
                return (grantID, grant)

            return (recipActorID, isInvite, acceptID, sieve, grantInfo)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, isInvite, acceptID, sieve, (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant))) -> do
            let recipByID = grantResourceLocalActor $ topicResource recipKey
            lift $ for_ mfwd $ \ (localRecips, sig) -> do
                forwardActivity
                    (actbBL body) localRecips sig recipActorID recipByID sieve
                    (if isInvite
                        then EventRemoteAcceptInviteLocalResourceFwdToFollower acceptID
                        else EventRemoteApproveJoinLocalResourceFwdToFollower acceptID
                    )
            lift $ sendActivity
                recipByID recipActorID localRecipsGrant
                remoteRecipsGrant fwdHostsGrant grantID
                (EventGrantAfterRemoteAccept grantID) actionGrant
            done "Forwarded the Accept and published a Grant"

    where

    tryInvite (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabInviterLocalCollab <$>
            MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
    tryInvite (Right remoteActivityID) = do
        CollabInviterRemote collab actorID _ <-
            MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender)

    tryJoin (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabRecipLocalJoinFulfills <$>
            MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
    tryJoin (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner)

    prepareGrant isInvite sender = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        accepter <- getJust $ remoteAuthorId author
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = grantResourceLocalActor $ topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audAccepter =
                                let ObjURI h lu = remoteAuthorURI author
                                in  AudRemote h [lu] (maybeToList $ remoteActorFollowers accepter)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audApprover =
                                let ObjURI h lu = remoteAuthorURI author
                                in  AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = Left AP.RoleAdmin
                    , AP.grantContext   =
                        encodeRouteLocal $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then remoteAuthorURI author
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

topicReject
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ActorId)
    -> (forall f. f topic -> GrantResourceBy f)
    -> UTCTime
    -> Key topic
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
topicReject topicActor topicResource now recipKey author body mfwd luReject reject = do

    -- Check input
    rejectee <- parseReject reject

    -- Verify the capability URI is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    maybeNew <- withDBExcept $ do

        -- Grab recipient deck from DB
        (recipActorID, recipActor) <- lift $ do
            recip <- getJust recipKey
            let actorID = topicActor recip
            (actorID,) <$> getJust actorID

        -- Find the rejected activity in our DB
        rejecteeDB <- do
            a <- getActivity rejectee
            fromMaybeE a "Can't find rejectee in DB"

        -- See if the rejected activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        collab <- do
            maybeCollab <-
                lift $ runMaybeT $
                    Left <$> tryInvite rejecteeDB <|>
                    Right <$> tryJoin rejecteeDB
            fromMaybeE maybeCollab "Rejected activity isn't an Invite or Join I'm aware of"

        -- Find the local resource and verify it's me
        collabID <-
            lift $ case collab of
                Left (fulfillsID, _, _) ->
                    collabFulfillsInviteCollab <$> getJust fulfillsID
                Right (fulfillsID, _, _, _) ->
                    collabFulfillsJoinCollab <$> getJust fulfillsID
        (deleteTopic, topic) <- lift $ getCollabTopic' collabID
        unless (topicResource recipKey == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

        idsForReject <-
            case collab of

                -- If rejecting an Invite, find the Collab recipient and verify
                -- it's the sender of the Reject
                Left (fulfillsID, _, deleteInviter) -> Left <$> do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case recip of
                        Right (Entity crrid crr)
                            | collabRecipRemoteActor crr == remoteAuthorId author -> return (fulfillsID, crrid, deleteInviter)
                        _ -> throwE "Rejecting an Invite whose recipient is someone else"

                -- If rejecting a Join, verify accepter has permission
                Right (fulfillsID, _, deleteRecipJoin, deleteRecip) -> Right <$> do
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                    verifyCapability
                        capability
                        (Right $ remoteAuthorId author)
                        (topicResource recipKey)
                    return (fulfillsID, deleteRecipJoin, deleteRecip)

        -- Verify the Collab isn't already validated
        maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
        verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

        -- Verify the Collab isn't already accepted/approved
        case idsForReject of
            Left (_fulfillsID, recipID, _) -> do
                mval <-
                    lift $ getBy $ UniqueCollabRecipRemoteAcceptCollab recipID
                verifyNothingE mval "Invite is already accepted"
            Right (fulfillsID, _, _) -> do
                mval1 <- lift $ getBy $ UniqueCollabApproverLocal fulfillsID
                mval2 <- lift $ getBy $ UniqueCollabApproverRemote fulfillsID
                unless (isNothing mval1 && isNothing mval2) $
                    throwE "Join is already approved"

        mractid <- lift $ insertToInbox now author body (actorInbox recipActor) luReject False
        for mractid $ \ rejectID -> do

            -- Delete the whole Collab record
            case idsForReject of
                Left (fulfillsID, recipID, deleteInviter) -> lift $ do
                    delete recipID
                    deleteTopic
                    deleteInviter
                    delete fulfillsID
                Right (fulfillsID, deleteRecipJoin, deleteRecip) -> lift $ do
                    deleteRecipJoin
                    deleteRecip
                    deleteTopic
                    delete fulfillsID
            lift $ delete collabID

            -- Prepare forwarding of Reject to my followers
            let recipByID = grantResourceLocalActor $ topicResource recipKey
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]
                isInvite = isLeft collab

            newRejectInfo <- do

                -- Prepare a Reject activity and insert to my outbox
                newRejectID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                let inviterOrJoiner = either (view _2) (view _2) collab
                newReject@(actionReject, _, _, _) <-
                    lift $ prepareReject isInvite inviterOrJoiner
                let recipByKey = grantResourceLocalActor $ topicResource recipKey
                _luNewReject <- lift $ updateOutboxItem' recipByKey newRejectID actionReject
                return (newRejectID, newReject)

            return (recipActorID, isInvite, rejectID, sieve, newRejectInfo)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, isInvite, rejectID, sieve, (newRejectID, (action, localRecips, remoteRecips, fwdHosts))) -> do
            let recipByID = grantResourceLocalActor $ topicResource recipKey
            lift $ for_ mfwd $ \ (localRecips, sig) -> do
                forwardActivity
                    (actbBL body) localRecips sig recipActorID recipByID sieve
                    (if isInvite
                        then EventRemoteRejectInviteLocalResourceFwdToFollower rejectID
                        else EventRemoteForbidJoinLocalResourceFwdToFollower rejectID
                    )
            lift $ sendActivity
                recipByID recipActorID localRecips
                remoteRecips fwdHosts newRejectID
                (EventRejectAfterRemoteReject newRejectID) action
            done "Forwarded the Reject and published my own Reject"

    where

    tryInvite (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabInviterLocal f _) <-
            MaybeT $ getBy $ UniqueCollabInviterLocalInvite itemID
        return (f, Left actorByKey, delete k)
    tryInvite (Right remoteActivityID) = do
        Entity k (CollabInviterRemote collab actorID _) <-
            MaybeT $ getBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender, delete k)

    tryJoin (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabRecipLocalJoin recipID fulfillsID _) <-
            MaybeT $ getBy $ UniqueCollabRecipLocalJoinJoin itemID
        return (fulfillsID, Left actorByKey, delete k, delete recipID)
    tryJoin (Right remoteActivityID) = do
        Entity k (CollabRecipRemoteJoin recipID fulfillsID _) <-
            MaybeT $ getBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner, delete k, delete recipID)

    prepareReject isInvite sender = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        rejecter <- getJust $ remoteAuthorId author
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = grantResourceLocalActor $ topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audRejecter =
                                let ObjURI h lu = remoteAuthorURI author
                                in  AudRemote h [lu] (maybeToList $ remoteActorFollowers rejecter)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audRejecter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audForbidder =
                                let ObjURI h lu = remoteAuthorURI author
                                in  AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audForbidder, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   =
                    [ let ObjURI h _ = remoteAuthorURI author
                      in  ObjURI h luReject
                    ]
                , AP.actionSpecific   = AP.RejectActivity AP.Reject
                    { AP.rejectObject = AP.rejectObject reject
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

topicInvite
    :: ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> GrantResourceBy f)
    -> EntityField ct (Key topic)
    -> EntityField ct CollabId
    -> (CollabId -> Key topic -> ct)
    -> UTCTime
    -> Key topic
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
topicInvite grabActor topicResource topicField topicCollabField collabTopicCtor now topicKey author body mfwd luInvite invite = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check invite
    targetByKey <- do
        (resource, recipient) <-
            parseInvite (Right $ remoteAuthorURI author) invite
        unless (Left (topicResource topicKey) == resource) $
            throwE "Invite topic isn't me"
        return recipient

    -- If target is local, find it in our DB
    -- If target is remote, HTTP GET it, verify it's an actor, and store in
    -- our DB (if it's already there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result (approve/disapprove the Invite) would be sent later in a
    -- separate (e.g. Accept) activity. But for the PoC level, the current
    -- situation will hopefully do.
    targetDB <-
        bitraverse
            (withDBExcept . flip getGrantRecip "Invitee not found in DB")
            (\ u@(ObjURI h lu) -> do
                instanceID <-
                    lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                result <-
                    ExceptT $ first (T.pack . displayException) <$>
                        fetchRemoteActor' instanceID h lu
                case result of
                    Left Nothing -> throwE "Target @id mismatch"
                    Left (Just err) -> throwE $ T.pack $ displayException err
                    Right Nothing -> throwE "Target isn't an actor"
                    Right (Just actor) -> return $ entityKey actor
            )
            targetByKey

    maybeNew <- withDBExcept $ do

        -- Grab topic from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust topicKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        -- Verify the specified capability gives relevant access
        verifyCapability
            capability
            (Right $ remoteAuthorId author)
            (topicResource topicKey)

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <-
            lift $ case targetDB of
                Left (GrantRecipPerson (Entity personID _)) ->
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipl) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipl E.^. CollabRecipLocalCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                        return $ recipl E.^. CollabRecipLocalCollab
                Right remoteActorID ->
                    E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
                        E.on $
                            topic E.^. topicCollabField E.==.
                            recipr E.^. CollabRecipRemoteCollab
                        E.where_ $
                            topic E.^. topicField E.==. E.val topicKey E.&&.
                            recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                        return $ recipr E.^. CollabRecipRemoteCollab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        mractid <- lift $ insertToInbox now author body (actorInbox topicActor) luInvite False
        lift $ for mractid $ \ inviteID -> do

            -- Insert Collab record to DB
            insertCollab targetDB inviteID

            -- Prepare forwarding Invite to my followers
            sieve <- do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        grantResourceLocalActor $ topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]
            return (topicActorID, inviteID, sieve)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, inviteID, sieve) -> do
            let topicByID = grantResourceLocalActor $ topicResource topicKey
            lift $ for_ mfwd $ \ (localRecips, sig) -> do
                forwardActivity
                    (actbBL body) localRecips sig topicActorID topicByID sieve
                    (EventRemoteInviteLocalTopicFwdToFollower inviteID)
            done "Recorded and forwarded the Invite"

    where

    insertCollab recipient inviteID = do
        collabID <- insert Collab
        fulfillsID <- insert $ CollabFulfillsInvite collabID
        insert_ $ collabTopicCtor collabID topicKey
        let authorID = remoteAuthorId author
        insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

topicJoin
    :: ( PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic
       , PersistRecordBackend ct SqlBackend
       )
    => (topic -> ActorId)
    -> (forall f. f topic -> GrantResourceBy f)
    -> EntityField ct (Key topic)
    -> EntityField ct CollabId
    -> (CollabId -> Key topic -> ct)
    -> UTCTime
    -> Key topic
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
topicJoin grabActor topicResource topicField topicCollabField collabTopicCtor now topicKey author body mfwd luJoin join = do

    -- Check input
    resource <- parseJoin join
    unless (resource == Left (topicResource topicKey)) $
        throwE "Join's object isn't me, don't need this Join"

    maybeNew <- withDBExcept $ do

        -- Grab topic from DB
        (topicActorID, topicActor) <- lift $ do
            recip <- getJust topicKey
            let actorID = grabActor recip
            (actorID,) <$> getJust actorID

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <- lift $ do
            let targetID = remoteAuthorId author
            E.select $ E.from $ \ (topic `E.InnerJoin` recipr) -> do
                E.on $
                    topic E.^. topicCollabField E.==.
                    recipr E.^. CollabRecipRemoteCollab
                E.where_ $
                    topic E.^. topicField E.==. E.val topicKey E.&&.
                    recipr E.^. CollabRecipRemoteActor E.==. E.val targetID
                return $ recipr E.^. CollabRecipRemoteCollab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        mractid <- lift $ insertToInbox now author body (actorInbox topicActor) luJoin False
        lift $ for mractid $ \ joinID -> do

            -- Insert Collab record to DB
            insertCollab joinID

            -- Prepare forwarding Join to my followers
            sieve <- do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        grantResourceLocalActor $ topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]
            return (topicActorID, joinID, sieve)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, joinID, sieve) -> do
            let topicByID = grantResourceLocalActor $ topicResource topicKey
            lift $ for_ mfwd $ \ (localRecips, sig) -> do
                forwardActivity
                    (actbBL body) localRecips sig topicActorID topicByID sieve
                    (EventRemoteJoinLocalTopicFwdToFollower joinID)
            done "Recorded and forwarded the Join"

    where

    insertCollab joinID = do
        collabID <- insert Collab
        fulfillsID <- insert $ CollabFulfillsJoin collabID
        insert_ $ collabTopicCtor collabID topicKey
        let authorID = remoteAuthorId author
        recipID <- insert $ CollabRecipRemote collabID authorID
        insert_ $ CollabRecipRemoteJoin recipID fulfillsID joinID
