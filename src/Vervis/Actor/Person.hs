{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2022, 2023
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Person
    (
    )
where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Yesod.Persist.Core

import qualified Data.Text as T

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Ticket

------------------------------------------------------------------------------
-- Following
------------------------------------------------------------------------------

-- Meaning: Someone is following someone
-- Behavior:
--      * Verify I'm the target
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
personFollow
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
personFollow now recipPersonID author body mfwd luFollow follow = do
    recipPersonHash <- encodeKeyHashid recipPersonID
    actorFollow
        (\case
            PersonR p | p == recipPersonHash -> pure ()
            _ -> throwE "Asking to follow someone else"
        )
        personActor
        True
        (\ recipPersonActor () ->
            pure $ actorFollowers recipPersonActor
        )
        (\ () -> pure $ makeRecipientSet [] [])
        LocalActorPerson
        (\ () -> pure [])
        now recipPersonID author body mfwd luFollow follow

-- Meaning: A remote actor is undoing some previous action
-- Behavior:
--      * Insert to my inbox
--      * If they're undoing their Following of me:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
personUndo
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
personUndo now recipPersonID author body _mfwd luUndo (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    maybeUndo <- withDBExcept $ do

        -- Grab recipient person from DB
        (personRecip, actorRecip) <- lift $ do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        -- Insert the Undo to person's inbox
        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luUndo False
        for mractid $ \ undoID -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                tryUnfollow followers undoneDB

            for maybeUndo $ \ () -> do

                -- Prepare an Accept activity and insert to person's outbox
                acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
                accept@(actionAccept, _, _, _) <- lift $ lift prepareAccept
                _luAccept <- lift $ updateOutboxItem' (LocalActorPerson recipPersonID) acceptID actionAccept

                return (personActor personRecip, acceptID, accept)

    case maybeUndo of
        Nothing -> done "I already have this activity in my inbox"
        Just Nothing -> done "Unrelated to me, just inserted to inbox"
        Just (Just (actorID, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept))) -> do
            lift $ sendActivity
                (LocalActorPerson recipPersonID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID
                EventAcceptRemoteFollow actionAccept
            done "Undid the Follow and published Accept"

    where

    tryUnfollow _                 (Left _)                 = mzero
    tryUnfollow personFollowersID (Right remoteActivityID) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        guard $ followerSetID == personFollowersID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID

    prepareAccept = do
        encodeRouteHome <- getEncodeRouteHome

        let ObjURI hAuthor luAuthor = remoteAuthorURI author
            audSender = AudRemote hAuthor [luAuthor] []
            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = ObjURI hAuthor luUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: A remote actor accepted something
-- Behavior:
--      * Insert to my inbox
--      * If it's a Follow I sent to them, add to my following list in DB
personAccept
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
personAccept now recipPersonID author body _mfwd luAccept accept = do

    -- Check input
    acceptee <- parseAccept accept

    maybeAccept <- withDBExcept $ do

        -- Grab recipient person from DB
        (personRecip, actorRecip) <- lift $ do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luAccept True
        for mractid $ \ acceptID -> runMaybeT $ do

            -- Find the accepted activity in our DB
            accepteeDB <- MaybeT $ getActivity acceptee

            tryFollow (personActor personRecip) accepteeDB acceptID

    case maybeAccept of
        Nothing -> done "I already have this activity in my inbox"
        Just Nothing -> done "Not my Follow; Just inserted to my inbox"
        Just (Just ()) ->
            done "Recorded this Accept on the Follow request I sent"

    where

    tryFollow actorID (Left (_, _, outboxItemID)) acceptID = do
        Entity key val <-
            MaybeT $ lift $
                getBy $ UniqueFollowRemoteRequestActivity outboxItemID
        guard $ followRemoteRequestPerson val == recipPersonID
        let uRecip =
                fromMaybe
                    (followRemoteRequestTarget val)
                    (followRemoteRequestRecip val)
        unless (remoteAuthorURI author == uRecip) $
            lift $ throwE "You're Accepting a Follow I sent to someone else"
        lift $ lift $ delete key
        lift $ lift $ insert_ FollowRemote
            { followRemoteActor  = actorID
            , followRemoteRecip  = remoteAuthorId author
            , followRemoteTarget = followRemoteRequestTarget val
            , followRemotePublic = followRemoteRequestPublic val
            , followRemoteFollow = outboxItemID
            , followRemoteAccept = acceptID
            }
    tryFollow _ (Right _) _ = mzero

-- Meaning: A remote actor rejected something
-- Behavior:
--      * Insert to my inbox
--      * If it's a Follow I sent to them, remove record from my DB
personReject
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
personReject now recipPersonID author body _mfwd luReject reject = do

    -- Check input
    rejectee <- parseReject reject

    maybeReject <- withDBExcept $ do

        -- Grab recipient person from DB
        (personRecip, actorRecip) <- lift $ do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luReject True
        for mractid $ \ rejectID -> runMaybeT $ do

            -- Find the rejected activity in our DB
            rejecteeDB <- MaybeT $ getActivity rejectee

            tryFollow rejecteeDB

    case maybeReject of
        Nothing -> done "I already have this activity in my inbox"
        Just Nothing -> done "Not my Follow; Just inserted to my inbox"
        Just (Just ()) ->
            done "Recorded this Reject on the Follow request I sent"

    where

    tryFollow (Left (_, _, outboxItemID)) = do
        Entity key val <-
            MaybeT $ lift $
                getBy $ UniqueFollowRemoteRequestActivity outboxItemID
        guard $ followRemoteRequestPerson val == recipPersonID
        let uRecip =
                fromMaybe
                    (followRemoteRequestTarget val)
                    (followRemoteRequestRecip val)
        unless (remoteAuthorURI author == uRecip) $
            lift $ throwE "You're Rejecting a Follow I sent to someone else"
        lift $ lift $ delete key
    tryFollow (Right _) = mzero

------------------------------------------------------------------------------
-- Commenting
------------------------------------------------------------------------------

-- Meaning: Someone commented on an issue/PR
-- Behavior: Insert to inbox
personCreateNote
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Note URIMode
    -> ActE (Text, Act (), Next)
personCreateNote now recipPersonID author body mfwd luCreate note = do

    -- Check input
    (luNote, published, Comment maybeParent topic source content) <- do
        (luId, luAuthor, published, comment) <- parseRemoteComment note
        unless (luAuthor == objUriLocal (remoteAuthorURI author)) $
            throwE "Create author != note author"
        return (luId, published, comment)

    mractid <- withDBExcept $ do
        Entity recipActorID recipActor <- lift $ do
            person <- getJust recipPersonID
            let actorID = personActor person
            Entity actorID <$> getJust actorID

        case topic of

            Right uContext -> do
                checkContextParent uContext maybeParent
                lift $ insertToInbox now author body (actorInbox recipActor) luCreate True

            Left (CommentTopicTicket deckID taskID) -> do
                (_, _, Entity _ ticket, _, _) <- do
                    mticket <- lift $ getTicket deckID taskID
                    fromMaybeE mticket "Context: No such deck-ticket"
                let did = ticketDiscuss ticket
                _ <- traverse (getMessageParent did) maybeParent
                lift $ insertToInbox now author body (actorInbox recipActor) luCreate True

            Left (CommentTopicCloth loomID clothID) -> do
                (_, _, Entity _ ticket, _, _, _) <- do
                    mticket <- lift $ getCloth loomID clothID
                    fromMaybeE mticket "Context: No such loom-cloth"
                let did = ticketDiscuss ticket
                _ <- traverse (getMessageParent did) maybeParent
                lift $ insertToInbox now author body (actorInbox recipActor) luCreate True

    done $
        case mractid of
            Nothing -> "I already have this activity in my inbox, doing nothing"
            Just _ -> "Inserted Create{Note} to my inbox"
    where
    checkContextParent (ObjURI hContext luContext) mparent = do
        mdid <- lift $ runMaybeT $ do
            iid <- MaybeT $ getKeyBy $ UniqueInstance hContext
            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid luContext
            rd <- MaybeT $ getValBy $ UniqueRemoteDiscussionIdent roid
            return $ remoteDiscussionDiscuss rd
        for_ mparent $ \ parent ->
            case parent of
                Left msg -> do
                    did <- fromMaybeE mdid "Local parent inexistent, no RemoteDiscussion"
                    void $ getLocalParentMessageId did msg
                Right (ObjURI hParent luParent) -> do
                    mrm <- lift $ runMaybeT $ do
                        iid <- MaybeT $ getKeyBy $ UniqueInstance hParent
                        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid luParent
                        MaybeT $ getValBy $ UniqueRemoteMessageIdent roid
                    for_ mrm $ \ rm -> do
                        let mid = remoteMessageRest rm
                        m <- lift $ getJust mid
                        did <- fromMaybeE mdid "Remote parent known, but no context RemoteDiscussion"
                        unless (messageRoot m == did) $
                            throwE "Remote parent belongs to a different discussion"

------------------------------------------------------------------------------
-- Access
------------------------------------------------------------------------------

-- Meaning: Someone invited someone to a resource
-- Behavior:
--      * Insert to my inbox
--      * If I'm the target, forward the Invite to my followers
personInvite
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
personInvite now recipPersonID author body mfwd luInvite invite = do

    -- Check input
    recipient <- do
        (_resource, target) <-
            parseInvite (Right $ remoteAuthorURI author) invite
        return target

    maybeInvite <- withDBExcept $ do

        -- Grab recipient person from DB
        (personRecip, actorRecip) <- lift $ do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luInvite True
        for mractid $ \ inviteID ->
            return (personActor personRecip, inviteID)

    case maybeInvite of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, inviteID) -> do
            let targetIsRecip =
                    case recipient of
                        Left (GrantRecipPerson p) -> p == recipPersonID
                        _ -> False
            if not targetIsRecip
                then done "I'm not the target; Inserted to inbox"
                else case mfwd of
                    Nothing ->
                        done
                            "I'm the target; Inserted to inbox; \
                            \Forwarding not approved"
                    Just (localRecips, sig) -> do
                        recipHash <- encodeKeyHashid recipPersonID
                        let sieve =
                                makeRecipientSet
                                    []
                                    [LocalStagePersonFollowers recipHash]
                        lift $ forwardActivity
                            (actbBL body) localRecips sig
                            actorID
                            (LocalActorPerson recipPersonID) sieve
                            (EventRemoteInviteLocalRecipFwdToFollower inviteID)
                        done
                            "I'm the target; Inserted to inbox; \
                            \Forwarded to followers if addressed"

-- Meaning: Someone asked to join a resource
-- Behavior: Insert to my inbox
personJoin
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
personJoin now recipPersonID author body mfwd luJoin join = do

    -- Check input
    _resource <- parseJoin join

    maybeJoinID <- lift $ withDB $ do

        -- Grab recipient person from DB
        (_personRecip, actorRecip) <- do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        insertToInbox now author body (actorInbox actorRecip) luJoin True

    case maybeJoinID of
        Nothing -> done "I already have this activity in my inbox"
        Just _joinID -> done "Inserted to my inbox"

-- Meaning: A remote actor published a Grant
-- Behavior:
--      * Insert to my inbox
personGrant
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
personGrant now recipPersonID author body mfwd luGrant grant = do

    -- Check input
    (_remoteResource, recipient) <- do
        let u@(ObjURI h _) = remoteAuthorURI author
        (resource, recip, _mresult, _mstart, _mend) <- parseGrant h grant
        resourceURI <-
            case resource of
                Right r -> return (u, r)
                _ -> error "Remote Grant but parseGrant identified local resource"
        when (recip == Right u) $
            throwE "Grant sender and target are the same remote actor"
        return (resourceURI, recip)

    maybeGrant <- withDBExcept $ do

        -- Grab recipient person from DB
        (personRecip, actorRecip) <- lift $ do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luGrant True
        for mractid $ \ grantID ->
            return (personActor personRecip, grantID)

    case maybeGrant of
        Nothing -> done "I already have this activity in my inbox"
        Just (_actorID, _grantID) -> do
            let targetIsRecip =
                    case recipient of
                        Left (GrantRecipPerson p) -> p == recipPersonID
                        _ -> False
            if not targetIsRecip
                then done "I'm not the target; Inserted to inbox"
                else done "I'm the target; Inserted to inbox"

-- Meaning: A remote actor has revoked some previously published Grants
-- Behavior: Insert to my inbox
personRevoke
    :: UTCTime
    -> PersonId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Revoke URIMode
    -> ActE (Text, Act (), Next)
personRevoke now recipPersonID author body _mfwd luRevoke (AP.Revoke _lus) = do

    maybeRevoke <- lift $ withDB $ do

        -- Grab recipient person from DB
        (_personRecip, actorRecip) <- do
            p <- getJust recipPersonID
            (p,) <$> getJust (personActor p)

        insertToInbox now author body (actorInbox actorRecip) luRevoke True

    case maybeRevoke of
        Nothing -> done "I already have this activity in my inbox"
        Just _revokeID -> done "Inserted to my inbox"

------------------------------------------------------------------------------
-- Main behavior function
------------------------------------------------------------------------------

insertActivityToInbox
    :: MonadIO m
    => UTCTime -> ActorId -> OutboxItemId -> ReaderT SqlBackend m Bool
insertActivityToInbox now recipActorID outboxItemID = do
    inboxID <- actorInbox <$> getJust recipActorID
    inboxItemID <- insert $ InboxItem True now
    maybeItem <- insertUnique $ InboxItemLocal inboxID outboxItemID inboxItemID
    case maybeItem of
        Nothing -> do
            delete inboxItemID
            return False
        Just _ -> return True

personBehavior :: UTCTime -> PersonId -> Verse -> ActE (Text, Act (), Next)
personBehavior now personID (Left event) =
    case event of
        -- Meaning: Someone X received an Invite and forwarded it to me because
        --          I'm a follower of X
        -- Behavior: Insert to my inbox
        EventRemoteInviteLocalRecipFwdToFollower inviteID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID inviteID itemID
            done "Inserted Invite to inbox"
        -- Meaning: A remote actor has forwarded to me a local activity
        -- Behavior: Insert it to my inbox
        EventRemoteFwdLocalActivity authorByKey outboxItemID -> withDBExcept $ do
                recipPerson <- lift $ getJust personID
                verifyLocalActivityExistsInDB authorByKey outboxItemID
                if LocalActorPerson personID == authorByKey
                    then done "Received activity authored by self, ignoring"
                    else do
                        inserted <- lift $ insertActivityToInbox now (personActor recipPerson) outboxItemID
                        done $
                            if inserted
                                then "Activity inserted to my inbox"
                                else "Activity already exists in my inbox, ignoring"
        -- Meaning: A deck/loom received an Undo{Resolve} and forwarded it to
        --          me because I'm a follower of the deck/loom or the ticket
        -- Behavior: Insert to my inbox
        EventRemoteUnresolveLocalResourceFwdToFollower undoID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID undoID itemID
            done "Inserted Undo{Resolve} to inbox"
        -- Meaning: A remote actor accepted an Invite on a local resource, I'm
        -- being forwarded as a follower of the resource
        --
        -- Behavior: Insert the Accept to my inbox
        EventRemoteAcceptInviteLocalResourceFwdToFollower acceptID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID acceptID itemID
            done "Inserted Accept{Invite} to inbox"
        -- Meaning: A remote actor approved a Join on a local resource, I'm
        -- being forwarded as a follower of the resource
        --
        -- Behavior: Insert the Accept to my inbox
        EventRemoteApproveJoinLocalResourceFwdToFollower acceptID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID acceptID itemID
            done "Inserted Accept{Join} to inbox"
        -- Meaning: Local resource sent a Grant, I'm the
        -- inviter/approver/target/follower
        --
        -- Behavior: Insert the Grant to my inbox
        EventGrantAfterRemoteAccept grantID -> do
            _ <- lift $ withDB $ do
                (personRecip, _actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                insertActivityToInbox now (personActor personRecip) grantID
            done "Inserted Grant to my inbox"
        -- Meaning: A remote actor rejected an Invite on a local resource, I'm
        -- being forwarded as a follower of the resource
        --
        -- Behavior: Insert the Accept to my inbox
        EventRemoteRejectInviteLocalResourceFwdToFollower rejectID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID rejectID itemID
            done "Inserted Reject{Invite} to inbox"
        -- Meaning: A remote actor disapproved a Join on a local resource, I'm
        -- being forwarded as a follower of the resource
        --
        -- Behavior: Insert the Reject to my inbox
        EventRemoteForbidJoinLocalResourceFwdToFollower rejectID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID rejectID itemID
            done "Inserted Reject{Join} to inbox"
        -- Meaning: Local resource sent a Reject on Invite/Join, I'm the
        -- inviter/disapprover/target/follower
        --
        -- Behavior: Insert the Reject to my inbox
        EventRejectAfterRemoteReject rejectID -> do
            _ <- lift $ withDB $ do
                (personRecip, _actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                insertActivityToInbox now (personActor personRecip) rejectID
            done "Inserted Reject to my inbox"
        -- Meaning: An authorized remote actor sent an Invite on a local
        -- resource, I'm being forwarded as a follower of the resource
        --
        -- Behavior: Insert the Invite to my inbox
        EventRemoteInviteLocalTopicFwdToFollower inviteID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID inviteID itemID
            done "Inserted Invite to inbox"
        -- Meaning: A remote actor sent a Join on a local resource, I'm being
        -- forwarded as a follower of the resource
        --
        -- Behavior: Insert the Join to my inbox
        EventRemoteJoinLocalTopicFwdToFollower joinID -> do
            lift $ withDB $ do
                (_personRecip, actorRecip) <- do
                    p <- getJust personID
                    (p,) <$> getJust (personActor p)
                let inboxID = actorInbox actorRecip
                itemID <- insert $ InboxItem True now
                insert_ $ InboxItemRemote inboxID joinID itemID
            done "Inserted Invite to inbox"
        _ -> throwE $ "Unsupported event for Person: " <> T.pack (show event)
personBehavior now personID (Right (VerseRemote author body mfwd luActivity)) =
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept ->
            personAccept now personID author body mfwd luActivity accept
        AP.CreateActivity (AP.Create obj mtarget) ->
            case obj of
                AP.CreateNote _ note ->
                    personCreateNote now personID author body mfwd luActivity note
                _ -> throwE "Unsupported create object type for people"
        AP.FollowActivity follow ->
            personFollow now personID author body mfwd luActivity follow
        AP.GrantActivity grant ->
            personGrant now personID author body mfwd luActivity grant
        AP.InviteActivity invite ->
            personInvite now personID author body mfwd luActivity invite
        AP.JoinActivity join ->
            personJoin now personID author body mfwd luActivity join
        AP.RejectActivity reject ->
            personReject now personID author body mfwd luActivity reject
        AP.RevokeActivity revoke ->
            personRevoke now personID author body mfwd luActivity revoke
        AP.UndoActivity undo ->
            personUndo now personID author body mfwd luActivity undo
        _ -> throwE "Unsupported activity type for Person"

instance VervisActor Person where
    actorBehavior = personBehavior
