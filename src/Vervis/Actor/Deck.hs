{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Deck
    (
    )
where

import Control.Applicative
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Yesod.Persist.Core

import qualified Data.Text as T

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI
import Vervis.Federation.Util
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Ticket

------------------------------------------------------------------------------
-- Following
------------------------------------------------------------------------------

-- Meaning: A remote actor is following someone/something
-- Behavior:
--      * Verify the target is me or a ticket of mine
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
deckFollow
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
deckFollow now recipDeckID author body mfwd luFollow follow = do
    recipDeckHash <- encodeKeyHashid recipDeckID
    actorFollow
        (\case
            DeckR d | d == recipDeckHash -> pure Nothing
            TicketR d t | d == recipDeckHash ->
                Just <$> decodeKeyHashidE t "Invalid task keyhashid"
            _ -> throwE "Asking to follow someone else"
        )
        deckActor
        False
        (\ recipDeckActor maybeTaskID ->
            case maybeTaskID of
                Nothing -> pure $ actorFollowers recipDeckActor
                Just taskID -> do
                    maybeTicket <- lift $ getTicket recipDeckID taskID
                    (_deck, _task, Entity _ ticket, _author, _resolve) <-
                        fromMaybeE maybeTicket "I don't have this ticket in DB"
                    return $ ticketFollowers ticket
        )
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorDeck
        (\ _ -> pure [])
        now recipDeckID author body mfwd luFollow follow

------------------------------------------------------------------------------
-- Access
------------------------------------------------------------------------------

-- Meaning: A remote actor accepted something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Accept is by the Invite target
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Accept is authorized
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--     * Otherwise respond with error
deckAccept
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
deckAccept = topicAccept deckActor GrantResourceDeck

-- Meaning: A remote actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
deckReject
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
deckReject = topicReject deckActor GrantResourceDeck

-- Meaning: A remote actor A invited someone B to a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A isn't inviting themselves
--      * Verify A is authorized by me to invite actors to me
--      * Verify B doesn't already have an invite/join/grant for me
--      * Remember the invite in DB
--      * Forward the Invite to my followers
deckInvite
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
deckInvite =
    topicInvite
        deckActor GrantResourceDeck
        CollabTopicDeckDeck CollabTopicDeckCollab CollabTopicDeck

-- Meaning: A remote actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
deckJoin
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
deckJoin =
    topicJoin
        deckActor GrantResourceDeck
        CollabTopicDeckDeck CollabTopicDeckCollab CollabTopicDeck

------------------------------------------------------------------------------
-- Ambiguous: Following/Resolving
------------------------------------------------------------------------------

-- Meaning: A remote actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me, or a ticket of mine:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * If they're unresolving a resolved ticket of mine:
--          * Verify they're authorized via a Grant
--          * Record it in my DB
--          * Forward the Undo to my+ticket followers
--          * Send an Accept to sender+followers and to my+ticket followers
--      * Otherwise respond with an error
deckUndo
    :: UTCTime
    -> DeckId
    -> RemoteAuthor
    -> ActivityBody
    -> Maybe (RecipientRoutes, ByteString)
    -> LocalURI
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
deckUndo now recipDeckID author body mfwd luUndo (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab recipient deck from DB
        (deckRecip, actorRecip) <- lift $ do
            p <- getJust recipDeckID
            (p,) <$> getJust (deckActor p)

        -- Insert the Undo to deck's inbox
        mractid <- lift $ insertToInbox now author body (actorInbox actorRecip) luUndo False
        for mractid $ \ undoID -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB
                    , tryUnresolve maybeCapability undoneDB
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow or Resolve related to me"

            -- Prepare an Accept activity and insert to deck's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorDeck recipDeckID) acceptID actionAccept

            return (deckActor deckRecip, undoID, sieve, acceptID, accept)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, undoID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept)) -> do
            lift $ for_ mfwd $ \ (localRecips, sig) -> do
                forwardActivity
                    (actbBL body) localRecips sig actorID
                    (LocalActorDeck recipDeckID) sieve
                    (EventRemoteUnresolveLocalResourceFwdToFollower undoID)
            lift $ sendActivity
                (LocalActorDeck recipDeckID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID
                EventAcceptRemoteFollow actionAccept
            done
                "Undid the Follow/Resolve, forwarded the Undo and published \
                \Accept"

    where

    tryUnfollow _               (Left _)                 = mzero
    tryUnfollow deckFollowersID (Right remoteActivityID) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID <|> verifyTargetTicket followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        let ObjURI hAuthor luAuthor = remoteAuthorURI author
            audSenderOnly = AudRemote hAuthor [luAuthor] []
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == deckFollowersID
        verifyTargetTicket followerSetID = do
            ticketID <-
                MaybeT $ lift $ getKeyBy $ UniqueTicketFollowers followerSetID
            TicketDeck _ d <-
                MaybeT $ lift $ getValBy $ UniqueTicketDeck ticketID
            guard $ d == recipDeckID

    tryUnresolve maybeCapability undone = do
        (deleteFromDB, ticketID) <- findTicket undone
        Entity taskID (TicketDeck _ d) <-
            MaybeT $ lift $ getBy $ UniqueTicketDeck ticketID
        guard $ d == recipDeckID

        -- Verify the sender is authorized by the deck to unresolve a ticket
        capability <- lift $ do
            cap <-
                fromMaybeE
                    maybeCapability
                    "Asking to unresolve ticket but no capability provided"
            case cap of
                Left c -> pure c
                Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
        lift $
            verifyCapability
                capability
                (Right $ remoteAuthorId author)
                (GrantResourceDeck recipDeckID)

        lift $ lift deleteFromDB

        recipDeckHash <- encodeKeyHashid recipDeckID
        taskHash <- encodeKeyHashid taskID
        audSender <- lift $ do
            ra <- lift $ getJust $ remoteAuthorId author
            let ObjURI hAuthor luAuthor = remoteAuthorURI author
            return $
                AudRemote hAuthor
                    [luAuthor]
                    (maybeToList $ remoteActorFollowers ra)
        return
            ( makeRecipientSet
                []
                [ LocalStageDeckFollowers recipDeckHash
                , LocalStageTicketFollowers recipDeckHash taskHash
                ]
            , [ AudLocal
                    []
                    [ LocalStageDeckFollowers recipDeckHash
                    , LocalStageTicketFollowers recipDeckHash taskHash
                    ]
              , audSender
              ]
            )
        where
        findTicket (Left (_actorByKey, _actorEntity, itemID)) = do
            Entity resolveLocalID resolveLocal <-
                MaybeT $ lift $ getBy $ UniqueTicketResolveLocalActivity itemID
            let resolveID = ticketResolveLocalTicket resolveLocal
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveLocalID >> delete resolveID
                , ticketID
                )
        findTicket (Right remoteActivityID) = do
            Entity resolveRemoteID resolveRemote <-
                MaybeT $ lift $ getBy $
                    UniqueTicketResolveRemoteActivity remoteActivityID
            let resolveID = ticketResolveRemoteTicket resolveRemote
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveRemoteID >> delete resolveID
                , ticketID
                )

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        let ObjURI hAuthor _ = remoteAuthorURI author
            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = ObjURI hAuthor luUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

------------------------------------------------------------------------------
-- Main behavior function
------------------------------------------------------------------------------

deckBehavior
    :: UTCTime -> DeckId -> Verse -> ExceptT Text Act (Text, Act (), Next)
deckBehavior _now _deckID (Left event) =
    case event of
        EventRemoteFwdLocalActivity _ _ ->
            throwE "Got a forwarded local activity, I don't need those"
        _ -> throwE $ "Unsupported event for Deck: " <> T.pack (show event)
deckBehavior now deckID (Right (VerseRemote author body mfwd luActivity)) =
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept ->
            deckAccept now deckID author body mfwd luActivity accept
        AP.FollowActivity follow ->
            deckFollow now deckID author body mfwd luActivity follow
        AP.InviteActivity invite ->
            deckInvite now deckID author body mfwd luActivity invite
        AP.JoinActivity join ->
            deckJoin now deckID author body mfwd luActivity join
        AP.RejectActivity reject ->
            deckReject now deckID author body mfwd luActivity reject
        AP.UndoActivity undo ->
            deckUndo now deckID author body mfwd luActivity undo
        _ -> throwE "Unsupported activity type for Deck"

instance VervisActor Deck where
    actorBehavior = deckBehavior
