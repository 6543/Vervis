{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2021, 2022, 2023
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.ActivityPub
    ( RemoteRecipient (..)
    --, getFollowers
    , unionRemotes
    , insertMany'
    , isInstanceErrorP
    , isInstanceErrorG

    --, checkDep
    --, getProjectAndDeps

    , provideEmptyCollection
    , insertEmptyOutboxItem
    , insertEmptyOutboxItem'
    , verifyContentTypeAP
    , verifyContentTypeAP_E
    , getActivity
    --, ActorEntity (..)
    --, getOutboxActorEntity
    --, actorEntityPath

    , verifyActorHasItem
    )
where

import Control.Applicative
import Control.Exception hiding (Handler, try)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.IO.Unlift
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.Function
import Data.List.NonEmpty (NonEmpty (..), nonEmpty)
import Data.Maybe
import Data.Semigroup
import Data.Text (Text)
import Data.Text.Encoding
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Network.HTTP.Client
import Network.TLS -- hiding (SHA256)
import Text.Blaze.Html (preEscapedToHtml)
import Text.Blaze.Html.Renderer.Text
import UnliftIO.Exception (try)
import Yesod.Core hiding (logError, logWarn, logInfo, logDebug)
import Yesod.Core.Handler
import Yesod.Persist.Core

import qualified Data.ByteString.Lazy as BL
import qualified Data.CaseInsensitive as CI
import qualified Data.List.NonEmpty as NE
import qualified Data.List as L
import qualified Data.List.Ordered as LO
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Database.Esqueleto as E

import Yesod.HttpSignature

import Control.Concurrent.Actor
import Database.Persist.JSON
import Network.FedURI
import Network.HTTP.Digest
import Web.Actor (stageInstanceHost)
import Yesod.ActivityPub
import Yesod.MonadSite
import Yesod.FedURI
import Yesod.Hashids

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.List.NonEmpty.Local
import Data.Patch.Local hiding (Patch)
import Data.Tuple.Local
import Database.Persist.Local

import qualified Data.Patch.Local as P

import Vervis.Actor
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient
import Vervis.RemoteActorStore
import Vervis.Settings
import Vervis.Time

{-
getFollowers :: FollowerSetId -> AppDB ([PersonId], [((InstanceId, Host), NonEmpty RemoteRecipient)])
getFollowers fsid = do
    local <- selectList [FollowTarget ==. fsid] [Asc FollowPerson]
    remote <- E.select $ E.from $ \ (rf `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
        E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
        E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
        E.on $ rf E.^. RemoteFollowActor E.==. ra E.^. RemoteActorId
        E.where_ $ rf E.^. RemoteFollowTarget E.==. E.val fsid
        E.orderBy [E.asc $ i E.^. InstanceId, E.asc $ ra E.^. RemoteActorId]
        return
            ( i E.^. InstanceId
            , i E.^. InstanceHost
            , ra E.^. RemoteActorId
            , ro E.^. RemoteObjectIdent
            , ra E.^. RemoteActorInbox
            , ra E.^. RemoteActorErrorSince
            )
    return
        ( map (followPerson . entityVal) local
        , groupRemotes $
            map (\ (E.Value iid, E.Value h, E.Value raid, E.Value luActor, E.Value luInbox, E.Value msince) ->
                    (iid, h, raid, luActor, luInbox, msince)
                )
                remote
        )
    where
    groupRemotes :: [(InstanceId, Host, RemoteActorId, LocalURI, LocalURI, Maybe UTCTime)] -> [((InstanceId, Host), NonEmpty RemoteRecipient)]
    groupRemotes = groupWithExtractBy ((==) `on` fst) fst snd . map toTuples
        where
        toTuples (iid, h, raid, luA, luI, ms) = ((iid, h), RemoteRecipient raid luA luI ms)
-}

unionRemotes
    :: [((InstanceId, Host), NonEmpty RemoteRecipient)]
    -> [((InstanceId, Host), NonEmpty RemoteRecipient)]
    -> [((InstanceId, Host), NonEmpty RemoteRecipient)]
unionRemotes = unionGroupsOrdWith fst remoteRecipientActor

insertMany' mk xs = zip' xs <$> insertMany (NE.toList $ mk <$> xs)
    where
    zip' x y =
        case nonEmpty y of
            Just y' | length x == length y' -> NE.zip x y'
            _ -> error "insertMany' returned different length!"

isInstanceErrorHttp (InvalidUrlException _ _)    = False
isInstanceErrorHttp (HttpExceptionRequest _ hec) =
    case hec of
        ResponseTimeout -> True
        ConnectionTimeout -> True
        InternalException se ->
            case fromException se of
                Just (HandshakeFailed _) -> True
                _ -> False
        _ -> False

isInstanceErrorP (AP.APPostErrorSig _)   = False
isInstanceErrorP (AP.APPostErrorHTTP he) = isInstanceErrorHttp he

isInstanceErrorG Nothing  = False
isInstanceErrorG (Just e) =
    case e of
        AP.APGetErrorHTTP he -> isInstanceErrorHttp he
        AP.APGetErrorJSON _ -> False
        AP.APGetErrorContentType _ -> False

{-
checkDep hProject shrProject prjProject u = do
    let (h, lu) = f2l u
    unless (h == hProject) $
        throwE "Dep belongs to different host"
    (shrTicket, prjTicket, num) <- parseTicket lu
    unless (shrTicket == shrProject) $
        throwE "Dep belongs to different sharer under same host"
    unless (prjTicket == prjProject) $
        throwE "Dep belongs to different project under same sharer"
    return num
    where
    parseTicket lu = do
        route <- case decodeRouteLocal lu of
            Nothing -> throwE "Expected ticket route, got invalid route"
            Just r -> return r
        case route of
            TicketR shr prj num -> return (shr, prj, num)
            _ -> throwE "Expected ticket route, got non-ticket route"
-}

{-
getProjectAndDeps shr prj {-deps-} = do
    msid <- lift $ getKeyBy $ UniqueSharer shr
    sid <- fromMaybeE msid "Offer target: no such local sharer"
    mej <- lift $ getBy $ UniqueProject prj sid
    Entity jid j <- fromMaybeE mej "Offer target: no such local project"
    {-
    tids <- for deps $ \ dep -> do
        mtid <- lift $ getKeyBy $ UniqueTicket jid dep
        fromMaybeE mtid "Local dep: No such ticket number in DB"
    -}
    return (sid, jid, projectInbox j, projectFollowers j{-, tids-})
-}

provideEmptyCollection :: AP.CollectionType -> Route App -> Handler TypedContent
provideEmptyCollection typ here = do
    encodeRouteLocal <- getEncodeRouteLocal
    let coll = AP.Collection
            { AP.collectionId         = encodeRouteLocal here
            , AP.collectionType       = typ
            , AP.collectionTotalItems = Just 0
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = [] :: [Text]
            }
    provideHtmlAndAP coll $ redirectToPrettyJSON here

insertEmptyOutboxItem obid now = do
    h <- asksSite siteInstanceHost
    insert OutboxItem
        { outboxItemOutbox    = obid
        , outboxItemActivity  =
            persistJSONObjectFromDoc $ AP.Doc h AP.emptyActivity
        , outboxItemPublished = now
        }

insertEmptyOutboxItem' obid now = do
    h <- asksEnv stageInstanceHost
    insert OutboxItem
        { outboxItemOutbox    = obid
        , outboxItemActivity  =
            persistJSONObjectFromDoc $ AP.Doc h AP.emptyActivity
        , outboxItemPublished = now
        }

verifyContentTypeAP :: MonadHandler m => m ()
verifyContentTypeAP = do
    result <- runExceptT verifyContentTypeAP_E
    case result of
        Left e -> invalidArgs ["Content type error: " <> e]
        Right () -> return ()

verifyContentTypeAP_E :: MonadHandler m => ExceptT Text m ()
verifyContentTypeAP_E = do
    ctypes <- lookupHeaders "Content-Type"
    case ctypes of
        [] -> throwE "Content-Type not specified"
        [x] | x == typeAS -> return ()
            | x == typeAS2 -> return ()
            | otherwise ->
                throwE $ "Not a recognized AP Content-Type: " <>
                    case decodeUtf8' x of
                        Left _ -> T.pack (show x)
                        Right t -> t
        _ -> throwE "More than one Content-Type specified"
    where
    typeAS = "application/activity+json"
    typeAS2 =
        "application/ld+json; \
        \profile=\"https://www.w3.org/ns/activitystreams\""

getActivity (Left (actor, obiid)) = Just . Left <$> do
    actorID <- do
        maybeActorID <- lift $ getLocalActorID actor
        fromMaybeE maybeActorID "No such actor entity in DB"
    actorDB <- lift $ getJust actorID
    let obid = actorOutbox actorDB
    obi <- do
        mobi <- lift $ get obiid
        fromMaybeE mobi "No such obiid"
    unless (outboxItemOutbox obi == obid) $
        throwE "Actor/obiid mismatch"
    return (actor, Entity actorID actorDB, obiid)

getActivity (Right u@(ObjURI h lu)) = lift $ runMaybeT $ Right <$> do
    iid <- MaybeT $ getKeyBy $ UniqueInstance h
    roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
    MaybeT $ getKeyBy $ UniqueRemoteActivity roid

{-
data ActorEntity
    = ActorPerson (Entity Person)
    | ActorProject (Entity Project)
    | ActorRepo (Entity Repo)
-}

{-
getOutboxActorEntity obid = do
    mp <- getBy $ UniquePersonOutbox obid
    ma <- getBy $ UniqueActorOutbox obid
    mr <- getBy $ UniqueRepoOutbox obid
    case (mp, ma, mr) of
        (Nothing, Nothing, Nothing) -> error "obid not in use"
        (Just p, Nothing, Nothing) -> return $ ActorPerson p
        (Nothing, Just (Entity aid _), Nothing) -> do
            mj <- getBy $ UniqueProjectActor aid
            case mj of
                Nothing -> error "found Actor not in use by any Project"
                Just j -> return $ ActorProject j
        (Nothing, Nothing, Just r) -> return $ ActorRepo r
        _ -> error "obid used by multiple actors"

actorEntityPath (ActorPerson (Entity _ p)) =
    LocalActorSharer . sharerIdent <$> getJust (personIdent p)
actorEntityPath (ActorProject (Entity _ j)) =
    flip LocalActorProject (projectIdent j) . sharerIdent <$>
        getJust (projectSharer j)
actorEntityPath (ActorRepo (Entity _ r)) =
    flip LocalActorRepo (repoIdent r) . sharerIdent <$>
        getJust (repoSharer r)
-}

verifyActorHasItem actorID itemID errorMessage = do
    inboxID <- lift $ actorInbox <$> getJust actorID
    maybeItem <- lift $ getBy $ UniqueInboxItemLocal inboxID itemID
    void $ fromMaybeE maybeItem errorMessage
