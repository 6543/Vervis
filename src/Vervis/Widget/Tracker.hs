{- This file is part of Vervis.
 -
 - Written in 2019, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Widget.Tracker
    ( deckNavW
    , loomNavW
    )
where

import Database.Persist.Types

import Yesod.Hashids

import Vervis.Foundation
import Vervis.Model
import Vervis.Settings

deckNavW :: Entity Deck -> Actor -> Widget
deckNavW (Entity deckID deck) actor = do
    deckHash <- encodeKeyHashid deckID
    hashRepo <- getEncodeKeyHashid
    $(widgetFile "deck/widget/nav")

loomNavW :: Entity Loom -> Actor -> Widget
loomNavW (Entity loomID loom) actor = do
    loomHash <- encodeKeyHashid loomID
    hashRepo <- getEncodeKeyHashid
    $(widgetFile "loom/widget/nav")
