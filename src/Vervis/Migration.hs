{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2021, 2022, 2023
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Migration
    ( migrateDB
    )
where

import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader (ReaderT, runReaderT)
import Data.Aeson
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Default.Instances.ByteString ()
import Data.Foldable (traverse_, for_)
import Data.List (nub)
import Data.Maybe
import Data.Proxy
import Data.String
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Calendar (Day (..))
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.BackendDataType (backendDataType, PersistDefault (..))
import Database.Persist.Migration
import Database.Persist.Schema (SchemaT, Migration)
import Database.Persist.Schema.SQL
import Database.Persist.Schema.Types hiding (Entity)
import Database.Persist.Schema.PostgreSQL (schemaBackend)
import Database.Persist.Sql (SqlBackend, toSqlKey, fromSqlKey)
import System.Directory
import System.FilePath
import Text.Blaze.Html (toHtml, preEscapedToHtml)
import Text.Blaze.Html.Renderer.Text
--import Text.Email.QuasiQuotation (email
import Text.Email.Validate (unsafeEmailAddress)
import Text.Hamlet
import Web.Hashids
import Web.PathPieces (toPathPiece)

import qualified Data.CaseInsensitive as CI
import qualified Data.HashMap.Strict as M
import qualified Data.List.Ordered as LO
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Builder as TLB
import qualified Database.Esqueleto as E
import qualified HTMLEntities.Decoder as HED

import qualified Database.Persist.Schema as S
import qualified Database.Persist.Schema.Types as ST

import Network.FedURI
import Database.Persist.JSON
import Web.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import Yesod.RenderSource

import Data.Either.Local
import Database.Persist.Local

import Vervis.FedURI
import Vervis.Model.Ident
import Vervis.Model.Ticket
import Vervis.Model.Workflow
import Vervis.Foundation (App (..), Route (..))
import Vervis.Migration.Entities
import Vervis.Migration.Model
import Vervis.Settings

instance PersistDefault ByteString where
    pdef = def

type Run m = SchemaT SqlBackend m ()
type Mig m = Migration SqlBackend m

defaultTime :: UTCTime
defaultTime = UTCTime (ModifiedJulianDay 0) 0

withPrepare :: Monad m => Mig m -> Run m -> Mig m
withPrepare (validate, apply) prepare = (validate, prepare >> apply)

--withPrePost :: Monad m => Run m -> Mig m -> Run m -> Mig m
--withPrePost pre (validate, apply) post = (validate, pre >> apply >> post)

addUnique' entity@(EntityName e) unique fields =
    addUnique entity $
        Unique (fromString $ "Unique" ++ T.unpack e ++ T.unpack unique) fields

removeUnique' entity@(EntityName e) unique =
    removeUnique entity $
        fromString $ "Unique" ++ T.unpack e ++ T.unpack unique

renameUnique' entity@(EntityName e) old new =
    renameUnique
        entity
        (fromString $ "Unique" ++ T.unpack e ++ T.unpack old)
        (fromString $ "Unique" ++ T.unpack e ++ T.unpack new)

changes :: (MonadSite m, SiteEnv m ~ App) => Host -> HashidsContext -> [Mig m]
changes hLocal ctx =
    [ -- 1
      addEntities model_2016_08_04
      -- 2
    , unchecked $ S.unsetFieldDefault "Sharer" "created"
      -- 3
    , unchecked $ S.unsetFieldDefault "Project" "nextTicket"
      -- 4
    , unchecked $ S.unsetFieldDefault "Repo" "vcs"
      -- 5
    , unchecked $ S.unsetFieldDefault "Repo" "mainBranch"
      -- 6
    , removeField "Ticket" "done"
      -- 7
    , addFieldPrimRequired "Ticket" ("TSNew" :: Text) "status"
      -- 8
    , addEntities model_2016_09_01_just_workflow
      -- 9
    , addEntities model_2016_09_01_rest
      -- 10
    , let key = toSqlKey 1 :: Key Workflow2016
      in  withPrepare
            (addFieldRefRequired "Project"
                (toBackendKey key)
                "workflow"
                "Workflow"
            ) $ do
                noProjects <- lift $
                    null <$> selectKeysList [] [LimitTo 1 :: SelectOpt Project2016]
                unless noProjects $ lift $ do
                    msid <-
                        listToMaybe <$>
                        selectKeysList [] [Asc Sharer2016Id, LimitTo 1]
                    for_ msid $ \ sid ->
                        insertKey key $
                            Workflow2016 sid "dummy" Nothing Nothing
      -- 11
    , addFieldPrimRequired "Workflow" ("WSSharer" :: Text) "scope"
      -- 12
    , unsetFieldPrimMaybe "Person" "hash" ("" :: Text)
      -- 13
    , changeFieldTypePrimRequiredFreeHs "Person" "hash" encodeUtf8
      -- 14
    --, unsetFieldPrimMaybe "Person" "email" [email|noreply@no.such.email|]
    , unsetFieldPrimMaybe "Person" "email" $
        unsafeEmailAddress "noreply" "no.such.email"
      -- 15
    , addFieldPrimRequired "Person" True "verified"
      -- 16
    , addFieldPrimRequired "Person" ("" :: Text) "verifiedKey"
      -- 17
    , addFieldPrimRequired "Person" ("" :: Text) "resetPassphraseKey"
      -- 18
    , renameField "Person" "hash" "passphraseHash"
      -- 19
    , renameField "Person" "resetPassphraseKey" "resetPassKey"
      -- 20
    , addFieldPrimRequired "Person" defaultTime "verifiedKeyCreated"
      -- 21
    , addFieldPrimRequired "Person" defaultTime "resetPassKeyCreated"
      -- 22
    , addUnique "Person" $ Unique "UniquePersonEmail" ["email"]
      -- 23
    , renameField "ProjectCollabAnon" "repo" "project"
      -- 24
    , renameField "ProjectCollabUser" "repo" "project"
      -- 25
    , addFieldPrimRequired "Person" ("" :: Text) "about"
      -- 26
    , setFieldMaybe "ProjectCollab" "role"
      -- 27
    , removeField "RepoCollab" "role"
      -- 28
    , addFieldRefOptional "RepoCollab" Nothing "role" "ProjectRole"
      -- 29
    , removeEntity "RepoCollabAnon"
      -- 30
    , removeEntity "RepoCollabUser"
      -- 31
    , addFieldRefOptional "Repo" Nothing "collabUser" "ProjectRole"
      -- 32
    , addFieldRefOptional "Repo" Nothing "collabAnon" "ProjectRole"
      -- 33
    , addFieldRefOptional "Project" Nothing "collabUser" "ProjectRole"
      -- 34
    , addFieldRefOptional "Project" Nothing "collabAnon" "ProjectRole"
      -- 35
    , unchecked $ lift $ do
        l <- E.select $ E.from $ \ (j `E.LeftOuterJoin`
                                   jcu `E.LeftOuterJoin`
                                   jca) -> do
            E.on $
                E.just (j E.^. Project2018Id) E.==.
                jca E.?. ProjectCollabAnon2018Project
            E.on $
                E.just (j E.^. Project2018Id) E.==.
                jcu E.?. ProjectCollabUser2018Project
            E.where_ $ E.not_ $
                E.isNothing (jcu E.?. ProjectCollabUser2018Project) E.&&.
                E.isNothing (jca E.?. ProjectCollabAnon2018Project)
            return
                ( j E.^. Project2018Id
                , jca E.?. ProjectCollabAnon2018Role
                , jcu E.?. ProjectCollabUser2018Role
                )
        for_ l $ \ (E.Value jid, E.Value malid, E.Value mulid) ->
            update jid
                [ Project2018CollabAnon =. malid
                , Project2018CollabUser =. mulid
                ]
      -- 36
    , removeEntity "ProjectCollabAnon"
      -- 37
    , removeEntity "ProjectCollabUser"
      -- 38
    , removeEntity "RepoAccess"
      -- 39
    , removeEntity "RepoRoleInherit"
      -- 40
    , removeEntity "RepoRole"
      -- 41
    , addEntities model_2019_02_03_verifkey
      -- 42
    , unchecked $ lift $ do
        deleteWhere ([] :: [Filter VerifKeySharedUsage2019])
        deleteWhere ([] :: [Filter VerifKey2019])
      -- 43
    , removeUnique "Message" "UniqueMessage"
      -- 44
    , removeField "Message" "number"
      -- 45
    , removeField "Discussion" "nextMessage"
      -- 46
    , addEntities model_2019_03_19
      -- 47
    , unchecked $ lift $ do
        msgs <- selectList ([] :: [Filter Message2019]) []
        let mklocal (Entity mid m) = LocalMessage2019 (message2019Author m) mid
        insertMany_ $ map mklocal msgs
      -- 48
    , removeField "Message" "author"
      -- 49
    , addUnique "Ticket" $ Unique "UniqueTicketDiscussion" ["discuss"]
      -- 50
    , addEntities model_2019_03_30
      -- 51
    , addFieldRefRequired'
        "Ticket"
        FollowerSet2019
        (Just $ do
            tids <- selectKeysList ([] :: [Filter Ticket2019]) []
            for_ tids $ \ tid -> do
                fsid <- insert FollowerSet2019
                update tid [Ticket2019Followers =. fsid]
        )
        "followers"
        "FollowerSet"
      -- 52
    , addUnique "Ticket" $ Unique "UniqueTicketFollowers" ["followers"]
      -- 53
    , removeField "RemoteDiscussion" "sharer"
      -- 54
    , addFieldPrimOptional
        "LocalMessage"
        (Nothing :: Maybe Text)
        "unlinkedParent"
      -- 55
    , addEntities model_2019_04_11
      -- 56
    , renameEntity "RemoteSharer" "RemoteActor"
      -- 57
    , renameUnique "RemoteActor" "UniqueRemoteSharer" "UniqueRemoteActor"
      -- 58
    , addFieldPrimOptional
        "RemoteActor"
        (Nothing :: Maybe UTCTime)
        "errorSince"
      -- 59
    , addEntities model_2019_04_12
      -- 60
    , addEntities model_2019_04_22
      -- 61
    , addFieldRefRequiredEmpty "RemoteMessage" "create" "RemoteActivity"
      -- 62
    , removeField "RemoteMessage" "raw"
      -- 63
    , removeEntity "RemoteRawObject"
      -- 64
    , addFieldPrimRequired "UnlinkedDelivery" True "forwarding"
      -- 65
    , addFieldPrimRequired "Delivery" True "forwarding"
      -- 66
    , addEntities model_2019_05_03
      -- 67
    , addFieldPrimRequired "Follow" False "manual"
      -- 68
    , addFieldPrimRequired "RemoteFollow" False "manual"
      -- 69
    , addEntity $ ST.Entity "InboxItem" [] []
      -- 70
    , addFieldRefRequiredEmpty "InboxItemLocal" "item" "InboxItem"
      -- 71
    , addFieldRefRequiredEmpty "InboxItemRemote" "item" "InboxItem"
      -- 72
    , addUnique "InboxItemLocal" $ Unique "UniqueInboxItemLocalItem" ["item"]
      -- 73
    , addUnique "InboxItemRemote" $ Unique "UniqueInboxItemRemoteItem" ["item"]
      -- 74
    , addEntities model_2019_05_17
      -- 75
    , addFieldPrimOptional "RemoteActor" (Nothing :: Maybe Text) "name"
      -- 76
    , addFieldPrimRequired "InboxItem" False "unread"
      -- 77
    , addFieldRefRequired''
        "LocalMessage"
        (do let user = "$$temp$$"
            sid <-
                insert $ Sharer201905 (text2shr user) Nothing defaultTime
            pid <-
                insert $
                    Person201905
                        sid user "" "e@ma.il" False "" defaultTime ""
                        defaultTime ""
            let h = Authority "x.y" Nothing :: Host
                doc = persistJSONObjectFromDoc $ Doc h emptyActivity
            insertEntity $ OutboxItem201905 pid doc defaultTime
        )
        (Just $ \ (Entity obid ob) -> do
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            lms <- selectList ([] :: [Filter LocalMessage201905]) []
            for_ lms $ \ (Entity lmid lm) -> do
                let pid = localMessage201905Author lm
                obidNew <- insert $ OutboxItem201905 pid doc defaultTime

                update lmid [LocalMessage201905Create =. obidNew]

            delete obid
            let pid = outboxItem201905Person ob
            p <- getJust pid
            delete pid
            delete $ person201905Ident p
        )
        "create"
        "OutboxItem"
      -- 78
    , addUnique "LocalMessage" $ Unique "UniqueLocalMessageCreate" ["create"]
      -- 79
    , renameEntity "ProjectRole" "Role"
      -- 80
    , renameUnique "Role" "UniqueProjectRole" "UniqueRole"
      -- 81
    , renameEntity "ProjectRoleInherit" "RoleInherit"
      -- 82
    , renameUnique "RoleInherit" "UniqueProjectRoleInherit" "UniqueRoleInherit"
      -- 83
    , renameEntity "ProjectAccess" "RoleAccess"
      -- 84
    , renameUnique "RoleAccess" "UniqueProjectAccess" "UniqueRoleAccess"
      -- 85
    , renameField "Message" "content" "source"
      -- 86
    , addFieldPrimRequired "Message" ("" :: Text) "content"
      -- 87
    , unchecked $ lift $ do
        msgs <- selectList ([] :: [Filter Message201906]) []
        for_ msgs $ \ (Entity mid m) ->
            let source = T.filter (/= '\r') $ message201906Source m
            in  case renderPandocMarkdown source of
                    Left err -> liftIO $ throwIO $ userError $ T.unpack err
                    Right content ->
                        update mid
                            [ Message201906Source  =. source
                            , Message201906Content =. content
                            ]
      -- 88
    , renameField "Ticket" "desc" "source"
      -- 89
    , addFieldPrimRequired "Ticket" ("" :: Text) "description"
      -- 90
    , unchecked $ lift $ do
        tickets <- selectList ([] :: [Filter Ticket201906]) []
        for_ tickets $ \ (Entity tid t) ->
            let source = T.filter (/= '\r') $ ticket201906Source t
            in  case renderPandocMarkdown source of
                    Left err -> liftIO $ throwIO $ userError $ T.unpack err
                    Right content ->
                        update tid
                            [ Ticket201906Source      =. source
                            , Ticket201906Description =. content
                            ]
      -- 91
    , addEntities model_2019_06_06
      -- 92
    , unchecked $ lift $ do
        tickets <- selectList ([] :: [Filter Ticket20190606]) []
        let mklocal (Entity tid t) =
                TicketAuthorLocal20190606 tid $ ticket20190606Creator t
        insertMany_ $ map mklocal tickets
      -- 93
    , setFieldMaybe "Ticket" "closer"
      -- 94
    , removeField "Ticket" "creator"
      -- 95
    , addEntity $ ST.Entity "Inbox" [] []
      -- 96
    , addFieldRefRequired'
        "Person"
        Inbox20190607
        (Just $ do
            pids <- selectKeysList ([] :: [Filter Person20190607]) []
            for_ pids $ \ pid -> do
                ibid <- insert Inbox20190607
                update pid [Person20190607Inbox =. ibid]
        )
        "inbox"
        "Inbox"
      -- 97
    , addFieldRefRequired'
        "InboxItemLocal"
        Inbox20190607
        (Just $ do
            ibils <- selectList ([] :: [Filter InboxItemLocal20190607]) []
            for_ ibils $ \ (Entity ibilid ibil) -> do
                person <- getJust $ inboxItemLocal20190607Person ibil
                let ibid = person20190607Inbox person
                update ibilid [InboxItemLocal20190607Inbox =. ibid]
        )
        "inbox"
        "Inbox"
      -- 98
    , addFieldRefRequired'
        "InboxItemRemote"
        Inbox20190607
        (Just $ do
            ibirs <- selectList ([] :: [Filter InboxItemRemote20190607]) []
            for_ ibirs $ \ (Entity ibirid ibir) -> do
                person <- getJust $ inboxItemRemote20190607Person ibir
                let ibid = person20190607Inbox person
                update ibirid [InboxItemRemote20190607Inbox =. ibid]
        )
        "inbox"
        "Inbox"
      -- 99
    , removeUnique "InboxItemLocal" "UniqueInboxItemLocal"
      -- 100
    , removeField "InboxItemLocal" "person"
      -- 101
    , addUnique "InboxItemLocal" $
        Unique "UniqueInboxItemLocal" ["inbox", "activity"]
      -- 102
    , removeUnique "InboxItemRemote" "UniqueInboxItemRemote"
      -- 103
    , removeField "InboxItemRemote" "person"
      -- 104
    , addUnique "InboxItemRemote" $
        Unique "UniqueInboxItemRemote" ["inbox", "activity"]
      -- 105
    , addUnique "Person" $ Unique "UniquePersonInbox" ["inbox"]
      -- 106
    , addFieldRefRequired'
        "Project"
        Inbox20190609
        (Just $ do
            jids <- selectKeysList ([] :: [Filter Project20190609]) []
            for_ jids $ \ jid -> do
                ibid <- insert Inbox20190609
                update jid [Project20190609Inbox =. ibid]
        )
        "inbox"
        "Inbox"
      -- 107
    , addUnique "Project" $ Unique "UniqueProjectInbox" ["inbox"]
      -- 108
    , addUnique "RemoteMessage" $ Unique "UniqueRemoteMessageCreate" ["create"]
      -- 109
    , unchecked $ lift $ do
        ibiids <- selectKeysList ([] :: [Filter InboxItem2019Fill]) []
        activities <- for ibiids $ \ ibiid ->
            requireEitherAlt
                (fmap inboxItemLocal2019FillActivity <$>
                    getValBy (UniqueInboxItemLocalItem2019Fill ibiid)
                )
                (fmap inboxItemRemote2019FillActivity <$>
                    getValBy (UniqueInboxItemRemoteItem2019Fill ibiid)
                )
                "InboxItem neither remote nor local"
                "InboxItem both remote and local"
        let getValByJust mkuniq id_ desc = do
                mval <- getValBy $ mkuniq id_
                case mval of
                    Nothing ->
                        error $
                            desc ++ show (fromSqlKey id_) ++
                            " isn't the Create of any Message"
                    Just val -> return val
        for_ (nub activities) $ \ activity -> do
            mid <- case activity of
                    Left obid ->
                        localMessage2019FillRest <$>
                            getValByJust
                                UniqueLocalMessageCreate2019Fill obid "obiid"
                    Right ractid ->
                        remoteMessage2019FillRest <$>
                            getValByJust
                                UniqueRemoteMessageCreate2019Fill ractid "ractid"
            did <- message2019FillRoot <$> getJust mid
            mt <- getValBy $ UniqueTicketDiscussion2019Fill did
            for_ mt $ \ t -> do
                ibid <-
                    project2019FillInbox <$> getJust (ticket2019FillProject t)
                ibiid <- insert $ InboxItem2019Fill False
                case activity of
                    Left obid ->
                        insert_ $ InboxItemLocal2019Fill ibid obid ibiid
                    Right ractid ->
                        insert_ $ InboxItemRemote2019Fill ibid ractid ibiid
      -- 110
    , addFieldRefRequired'
        "Project"
        FollowerSet20190610
        (Just $ do
            jids <- selectKeysList ([] :: [Filter Project20190610]) []
            for_ jids $ \ jid -> do
                fsid <- insert FollowerSet20190610
                update jid [Project20190610Followers =. fsid]
        )
        "followers"
        "FollowerSet"
      -- 111
    , addUnique "Project" $ Unique "UniqueProjectFollowers" ["followers"]
      -- 112
    , addFieldRefRequiredEmpty "TicketAuthorRemote" "offer" "RemoteActivity"
      -- 113
    , addUnique "TicketAuthorRemote" $
        Unique "UniqueTicketAuthorRemoteOffer" ["offer"]
      -- 114
    , addFieldRefRequired''
        "TicketAuthorLocal"
        (do let user = "$$temp$$"
            sid <-
                insert $ Sharer20190612 (text2shr user) Nothing defaultTime
            ibid <- insert Inbox20190612
            pid <-
                insert $
                    Person20190612
                        sid user "" "e@ma.il" False "" defaultTime ""
                        defaultTime "" ibid
            let localUri = LocalURI "/x/y"
                h = Authority "x.y" Nothing :: Host
                fedUri = ObjURI h localUri
                doc = Doc h Activity
                    { activityId       = Nothing
                    , activityActor    = localUri
                    , activityCapability = Nothing
                    , activitySummary  = Nothing
                    , activityAudience = Audience [] [] [] [] [] []
                    , activityFulfills = []
                    , activityProof    = Nothing
                    , activitySpecific = RejectActivity $ Reject fedUri
                    }
            insertEntity $ OutboxItem20190612 pid (persistJSONObjectFromDoc doc) defaultTime
        )
        (Just $ \ (Entity obidTemp obTemp) -> do
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            ts <- selectList ([] :: [Filter Ticket20190612]) []
            for_ ts $ \ (Entity tid ticket) -> do

                Entity talid tal <-
                    fromJust <$> getBy (UniqueTicketAuthorLocal20190612 tid)
                let pidAuthor = ticketAuthorLocal20190612Author tal

                j <- getJust $ ticket20190612Project ticket
                let ibidProject = project20190612Inbox j

                obidNew <- insert OutboxItem20190612
                    { outboxItem20190612Person    = pidAuthor
                    , outboxItem20190612Activity  = doc
                    , outboxItem20190612Published =
                        ticket20190612Created ticket
                    }

                update talid [TicketAuthorLocal20190612Offer =. obidNew]
                ibiid <- insert $ InboxItem20190612 False
                insert_ $ InboxItemLocal20190612 ibidProject obidNew ibiid

            delete obidTemp
            let pidTemp = outboxItem20190612Person obTemp
            pTemp <- getJust pidTemp
            delete pidTemp
            delete $ person20190612Ident pTemp
            delete $ person20190612Inbox pTemp
        )
        "offer"
        "OutboxItem"
      -- 115
    , addUnique "TicketAuthorLocal" $
        Unique "UniqueTicketAuthorLocaleOffer" ["offer"]
      -- 116
    , addEntity $ ST.Entity "Outbox" [] []
      -- 117
    , addFieldRefRequired'
        "Person"
        Outbox20190615
        (Just $ do
            pids <- selectKeysList ([] :: [Filter Person20190615]) []
            for_ pids $ \ pid -> do
                obid <- insert Outbox20190615
                update pid [Person20190615Outbox =. obid]
        )
        "outbox"
        "Outbox"
      -- 118
    , addUnique "Person" $ Unique "UniquePersonOutbox" ["outbox"]
      -- 119
    , addFieldRefRequired'
        "OutboxItem"
        Outbox20190615
        (Just $ do
            obiids <- selectList ([] :: [Filter OutboxItem20190615]) []
            for_ obiids $ \ (Entity obiid obi) -> do
                person <- getJust $ outboxItem20190615Person obi
                let obid = person20190615Outbox person
                update obiid [OutboxItem20190615Outbox =. obid]
        )
        "outbox"
        "Outbox"
      -- 120
    , removeField "OutboxItem" "person"
      -- 121
    , addFieldRefRequired'
        "Project"
        Outbox20190616
        (Just $ do
            jids <- selectKeysList ([] :: [Filter Project20190616]) []
            for_ jids $ \ jid -> do
                obid <- insert Outbox20190616
                update jid [Project20190616Outbox =. obid]
        )
        "outbox"
        "Outbox"
      -- 122
    , addUnique "Project" $ Unique "UniqueProjectOutbox" ["outbox"]
      -- 123
    , unchecked $ lift $ do
        ts <- selectList ([] :: [Filter Ticket20190612]) []
        for_ ts $ \ (Entity tid t) ->
            let title =
                    TL.toStrict $ renderHtml $ toHtml $ ticket20190612Title t
            in  update tid [Ticket20190612Title =. title]
      -- 124
    , addFieldRefRequired''
        "Ticket"
        (do obid <- insert Outbox20190624
            let h = Authority "x.y" Nothing :: Host
                doc = persistJSONObjectFromDoc $ Doc h emptyActivity
            insertEntity $ OutboxItem20190624 obid doc defaultTime
        )
        (Just $ \ (Entity obiidTemp obiTemp) -> do
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            ts <- selectList ([] :: [Filter Ticket20190624]) []
            for_ ts $ \ (Entity tid ticket) -> do

                Entity talid tal <-
                    fromJust <$> getBy (UniqueTicketAuthorLocal20190624 tid)
                let pidAuthor = ticketAuthorLocal20190624Author tal
                pAuthor <- getJust pidAuthor
                let ibidAuthor = person20190624Inbox pAuthor

                j <- getJust $ ticket20190624Project ticket
                let obidProject = project20190624Outbox j

                obiidNew <- insert OutboxItem20190624
                    { outboxItem20190624Outbox    = obidProject
                    , outboxItem20190624Activity  = doc
                    , outboxItem20190624Published =
                        ticket20190624Created ticket
                    }

                update tid [Ticket20190624Accept =. obiidNew]
                ibiid <- insert $ InboxItem20190624 True
                insert_ $ InboxItemLocal20190624 ibidAuthor obiidNew ibiid

            delete obiidTemp
            delete $ outboxItem20190624Outbox obiTemp
        )
        "accept"
        "OutboxItem"
      -- 125
    , addUnique "Ticket" $ Unique "UniqueTicketAccept" ["accept"]
      -- 126
    , unchecked $ lift $ do
        tids <- selectKeysList [Ticket20190624Status !=. "TSClosed"] []
        updateWhere
            [Ticket20190624Id <-. tids]
            [Ticket20190624Closer =. Nothing]
      -- 127
    , addFieldRefRequired''
        "TicketDependency"
        (do let user = "$$temp$$"
            sid <-
                insert $ Sharer127 (text2shr user) Nothing defaultTime
            ibid <- insert Inbox127
            obid <- insert Outbox127
            insertEntity $
                Person127
                    sid user "" "e@ma.il" False "" defaultTime ""
                    defaultTime "" ibid obid
        )
        (Just $ \ (Entity pidTemp pTemp) -> do
            tds <- selectList ([] :: [Filter TicketDependency127]) []
            for_ tds $ \ (Entity tdid td) -> do
                t <- getJust $ ticketDependency127Parent td
                j <- getJust $ ticket127Project t
                mpid <- getKeyBy $ UniquePersonIdent127 $ project127Sharer j
                let pid = fromMaybe (error "No Person found for Sharer") mpid
                update tdid [TicketDependency127Author =. pid]

            delete pidTemp
            delete $ person127Ident pTemp
        )
        "author"
        "Person"
      -- 128
    , addFieldPrimRequired
        "TicketDependency"
        ("(A ticket dependency)" :: Text)
        "summary"
      -- 129
    , addFieldPrimRequired "TicketDependency" defaultTime "created"
      -- 130
    , addFieldRefRequired'
        "Repo"
        FollowerSet130
        (Just $ do
            rids <- selectKeysList ([] :: [Filter Repo130]) []
            for_ rids $ \ rid -> do
                fsid <- insert FollowerSet130
                update rid [Repo130Followers =. fsid]
        )
        "followers"
        "FollowerSet"
      -- 131
    , addUnique "Repo" $ Unique "UniqueRepoFollowers" ["followers"]
      -- 132
    , addFieldRefRequired'
        "Repo"
        Inbox130
        (Just $ do
            rids <- selectKeysList ([] :: [Filter Repo130]) []
            for_ rids $ \ rid -> do
                ibid <- insert Inbox130
                update rid [Repo130Inbox =. ibid]
        )
        "inbox"
        "Inbox"
      -- 133
    , addUnique "Repo" $ Unique "UniqueRepoInbox" ["inbox"]
      -- 134
    , addFieldRefRequired'
        "Person"
        FollowerSet130
        (Just $ do
            pids <- selectKeysList ([] :: [Filter Person130]) []
            for_ pids $ \ pid -> do
                fsid <- insert FollowerSet130
                update pid [Person130Followers =. fsid]
        )
        "followers"
        "FollowerSet"
      -- 135
    , addUnique "Person" $ Unique "UniquePersonFollowers" ["followers"]
      -- 136
    , addFieldPrimRequired "Follow" True "public"
      -- 137
    , addFieldPrimRequired "RemoteFollow" True "public"
      -- 138
    , addFieldRefRequired'
        "Repo"
        Outbox138
        (Just $ do
            rids <- selectKeysList ([] :: [Filter Repo138]) []
            for_ rids $ \ rid -> do
                obid <- insert Outbox138
                update rid [Repo138Outbox =. obid]
        )
        "outbox"
        "Outbox"
      -- 139
    , addUnique "Repo" $ Unique "UniqueRepoOutbox" ["outbox"]
      -- 140
    , addFieldRefRequiredEmpty "Follow" "follow" "OutboxItem"
      -- 141
    , addUnique "Follow" $ Unique "UniqueFollowFollow" ["follow"]
      -- 142
    , addFieldRefRequiredEmpty "RemoteFollow" "follow" "RemoteActivity"
      -- 143
    , addUnique "RemoteFollow" $ Unique "UniqueRemoteFollowFollow" ["follow"]
      -- 144
    , addEntities model_2019_09_25
      -- 145
    , addFieldRefRequiredEmpty "Follow" "accept" "OutboxItem"
      -- 146
    , addUnique "Follow" $ Unique "UniqueFollowAccept" ["accept"]
      -- 147
    , addFieldRefRequiredEmpty "RemoteFollow" "accept" "OutboxItem"
      -- 148
    , addUnique "RemoteFollow" $ Unique "UniqueRemoteFollowAccept" ["accept"]
      -- 149
    , removeField "Follow" "manual"
      -- 150
    , removeField "RemoteFollow" "manual"
      -- 151
    , addEntities model_2019_11_04
      -- 152
    , addFieldRefRequired''
        "RemoteActivity"
        (do iid <- insert $ Instance152 $ Authority "152.fake.fake" Nothing
            insertEntity $ RemoteObject152 iid $ LocalURI "/fake/152"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            racts <- selectList ([] :: [Filter RemoteActivity152]) []
            for_ racts $ \ (Entity ractid ract) -> do
                let iid = remoteActivity152Instance ract
                    lu = remoteActivity152Ident ract
                roid <- insert $ RemoteObject152 iid lu
                update ractid [RemoteActivity152IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject152Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 153
    , addUnique "RemoteActivity" $
        Unique "UniqueRemoteActivityNew" ["identNew"]
      -- 154
    , removeUnique "RemoteActivity" "UniqueRemoteActivity"
      -- 155
    , renameUnique "RemoteActivity" "UniqueRemoteActivityNew" "UniqueRemoteActivity"
      -- 156
    , removeField "RemoteActivity" "instance"
      -- 157
    , removeField "RemoteActivity" "ident"
      -- 158
    , renameField "RemoteActivity" "identNew" "ident"
      -- 159
    , addFieldRefRequired''
        "UnfetchedRemoteActor"
        (do iid <- insert $ Instance159 $ Authority "159.fake.fake" Nothing
            insertEntity $ RemoteObject159 iid $ LocalURI "/fake/159"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            uras <- selectList ([] :: [Filter UnfetchedRemoteActor159]) []
            for_ uras $ \ (Entity uraid ura) -> do
                let iid = unfetchedRemoteActor159Instance ura
                    lu = unfetchedRemoteActor159Ident ura
                roid <- insert $ RemoteObject159 iid lu
                update uraid [UnfetchedRemoteActor159IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject159Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 160
    , addUnique "UnfetchedRemoteActor" $
        Unique "UniqueUnfetchedRemoteActorNew" ["identNew"]
      -- 161
    , addFieldRefRequired''
        "RemoteActor"
        (do iid <- insert $ Instance159 $ Authority "159.fake.fake" Nothing
            insertEntity $ RemoteObject159 iid $ LocalURI "/fake/159"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            ras <- selectList ([] :: [Filter RemoteActor159]) []
            for_ ras $ \ (Entity raid ra) -> do
                let iid = remoteActor159Instance ra
                    lu = remoteActor159Ident ra
                roid <- insert $ RemoteObject159 iid lu
                update raid [RemoteActor159IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject159Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 162
    , addUnique "RemoteActor" $ Unique "UniqueRemoteActorNew" ["identNew"]
      -- 163
    , removeUnique "UnfetchedRemoteActor" "UniqueUnfetchedRemoteActor"
      -- 164
    , renameUnique "UnfetchedRemoteActor" "UniqueUnfetchedRemoteActorNew" "UniqueUnfetchedRemoteActor"
      -- 165
    , removeUnique "RemoteActor" "UniqueRemoteActor"
      -- 166
    , renameUnique "RemoteActor" "UniqueRemoteActorNew" "UniqueRemoteActor"
      -- 167
    , removeField "UnfetchedRemoteActor" "instance"
      -- 168
    , removeField "UnfetchedRemoteActor" "ident"
      -- 169
    , renameField "UnfetchedRemoteActor" "identNew" "ident"
      -- 170
    , removeField "RemoteActor" "instance"
      -- 171
    , removeField "RemoteActor" "ident"
      -- 172
    , renameField "RemoteActor" "identNew" "ident"
      -- 173
    , addFieldRefRequired''
        "RemoteCollection"
        (do iid <- insert $ Instance159 $ Authority "173.fake.fake" Nothing
            insertEntity $ RemoteObject159 iid $ LocalURI "/fake/173"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            rcs <- selectList ([] :: [Filter RemoteCollection159]) []
            for_ rcs $ \ (Entity rcid rc) -> do
                let iid = remoteCollection159Instance rc
                    lu = remoteCollection159Ident rc
                roid <- insert $ RemoteObject159 iid lu
                update rcid [RemoteCollection159IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject159Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 174
    , addUnique "RemoteCollection"
        $ Unique "UniqueRemoteCollectionNew" ["identNew"]
      -- 175
    , removeUnique "RemoteCollection" "UniqueRemoteCollection"
      -- 176
    , renameUnique "RemoteCollection" "UniqueRemoteCollectionNew" "UniqueRemoteCollection"
      -- 177
    , removeField "RemoteCollection" "instance"
      -- 178
    , removeField "RemoteCollection" "ident"
      -- 179
    , renameField "RemoteCollection" "identNew" "ident"
      -- 180
    , renameEntity "WorkflowFieldEnum" "WorkflowEnum"
      -- 181
    , renameEntity "WorkflowFieldEnumCtor" "WorkflowEnumCtor"
      -- 182
    , renameUnique "WorkflowEnum" "UniqueWorkflowFieldEnum" "UniqueWorkflowEnum"
      -- 183
    , renameUnique "WorkflowEnumCtor" "UniqueWorkflowFieldEnumCtor" "UniqueWorkflowEnumCtor"
      -- 184
    , addEntities model_2020_01_05
      -- 185
    , addFieldPrimOptional "WorkflowField" (Nothing :: Maybe Int) "color"
      -- 186
    , removeUnique "Ticket" "UniqueTicket"
      -- 187
    , setFieldMaybe "Ticket" "number"
      -- 188
    , addEntities model_2020_02_05
      -- 189
    , unchecked $ lift $ do
        ts <- selectList ([] :: [Filter Ticket189]) []
        let makeLT (Entity tid t) = LocalTicket189
                { localTicket189Ticket    = tid
                , localTicket189Discuss   = ticket189Discuss t
                , localTicket189Followers = ticket189Followers t
                }
        insertMany_ $ map makeLT ts
      -- 190
    , removeUnique "Ticket" "UniqueTicketDiscussion"
      -- 191
    , removeUnique "Ticket" "UniqueTicketFollowers"
      -- 192
    , removeField "Ticket" "discuss"
      -- 193
    , removeField "Ticket" "followers"
      -- 194
    , addFieldRefRequired''
        "TicketAuthorLocal"
        (do tid <- do
                jid <- do
                    let temp = "$$temp$$"
                    sid <- insert $ Sharer194 (text2shr temp) Nothing defaultTime
                    wid <- insert $ Workflow194 sid (text2wfl temp) Nothing Nothing WSSharer
                    ibid <- insert Inbox194
                    obid <- insert Outbox194
                    fsid <- insert FollowerSet194
                    insert $ Project194 (text2prj temp) sid Nothing Nothing wid 1 Nothing Nothing Nothing ibid obid fsid
                obiid <- do
                    obid <- insert Outbox194
                    let h = Authority "x.y" Nothing :: Host
                        doc = Doc h emptyActivity
                    insert $ OutboxItem194 obid (persistJSONObjectFromDoc doc) defaultTime
                insert $ Ticket194 jid Nothing defaultTime "" "" "" Nothing "TSNew" defaultTime Nothing obiid
            did <- insert Discussion194
            fsid <- insert FollowerSet194
            insertEntity $ LocalTicket194 tid did fsid
        )
        (Just $ \ (Entity ltidTemp ltTemp) -> do
            tals <- selectList ([] :: [Filter TicketAuthorLocal194]) []
            for_ tals $ \ (Entity talid tal) -> do
                tlid <- do
                    mtlid <- getKeyBy $ UniqueLocalTicket194 $ ticketAuthorLocal194Ticket tal
                    case mtlid of
                        Nothing -> error $ "No LocalTicket for talid#" ++ show talid
                        Just tlid -> return tlid
                update talid [TicketAuthorLocal194TicketNew =. tlid]

            delete ltidTemp

            let tid = localTicket194Ticket ltTemp
            t <- getJust tid
            delete tid

            let jid = ticket194Project t
            j <- getJust jid
            delete jid
            delete $ project194Workflow j
            delete $ project194Sharer j
            delete $ project194Inbox j
            delete $ project194Outbox j
            delete $ project194Followers j

            let obiid = ticket194Accept t
            obi <- getJust obiid
            delete obiid
            delete $ outboxItem194Outbox obi

            delete $ localTicket194Discuss ltTemp

            delete $ localTicket194Followers ltTemp
        )
        "ticketNew"
        "LocalTicket"
      -- 195
    , addUnique "TicketAuthorLocal" $
        Unique "UniqueTicketAuthorLocalNew" ["ticketNew"]
      -- 196
    , removeUnique "TicketAuthorLocal" "UniqueTicketAuthorLocal"
      -- 197
    , removeField "TicketAuthorLocal" "ticket"
      -- 198
    , renameUnique
        "TicketAuthorLocal"
        "UniqueTicketAuthorLocalNew"
        "UniqueTicketAuthorLocal"
      -- 199
    , renameField "TicketAuthorLocal" "ticketNew" "ticket"
      -- 200
    , addEntities model_2020_02_07
      -- 201
    , unchecked $ lift $ do
        ts <- selectList ([] :: [Filter Ticket201]) []
        let makeTPL (Entity tid t) = TicketProjectLocal201
                { ticketProjectLocal201Ticket  = tid
                , ticketProjectLocal201Project = ticket201Project t
                , ticketProjectLocal201Accept  = ticket201Accept t
                }
        insertMany_ $ map makeTPL ts
      -- 202
    , removeField "Ticket" "project"
      -- 203
    , removeUnique "Ticket" "UniqueTicketAccept"
      -- 204
    , removeField "Ticket" "accept"
      -- 205
    , addFieldRefRequired''
        "TicketAuthorRemote"
        (do tid <- insert $ Ticket205 Nothing defaultTime "" "" "" Nothing "TSNew" defaultTime Nothing
            jid <- do
                let temp = "$$temp$$"
                sid <- insert $ Sharer205 (text2shr temp) Nothing defaultTime
                wid <- insert $ Workflow205 sid (text2wfl temp) Nothing Nothing WSSharer
                ibid <- insert Inbox205
                obid <- insert Outbox205
                fsid <- insert FollowerSet205
                insert $ Project205 (text2prj temp) sid Nothing Nothing wid 1 Nothing Nothing Nothing ibid obid fsid
            obiid <- do
                obid <- insert Outbox205
                let h = Authority "x.y" Nothing :: Host
                    doc = Doc h emptyActivity
                insert $ OutboxItem205 obid (persistJSONObjectFromDoc doc) defaultTime
            insertEntity $ TicketProjectLocal205 tid jid obiid
        )
        (Just $ \ (Entity tplidTemp tplTemp) -> do
            tars <- selectList ([] :: [Filter TicketAuthorRemote205]) []
            for_ tars $ \ (Entity tarid tar) -> do
                tplid <- do
                    mtplid <- getKeyBy $ UniqueTicketProjectLocal205 $ ticketAuthorRemote205Ticket tar
                    case mtplid of
                        Nothing -> error $ "No TicketProjectLocal for tarid#" ++ show tarid
                        Just k -> return k
                update tarid [TicketAuthorRemote205TicketNew =. tplid]

            delete tplidTemp

            let tid = ticketProjectLocal205Ticket tplTemp
            _t <- getJust tid
            delete tid

            let jid = ticketProjectLocal205Project tplTemp
            j <- getJust jid
            delete jid
            delete $ project205Workflow j
            delete $ project205Sharer j
            delete $ project205Inbox j
            delete $ project205Outbox j
            delete $ project205Followers j

            let obiid = ticketProjectLocal205Accept tplTemp
            obi <- getJust obiid
            delete obiid
            delete $ outboxItem205Outbox obi
        )
        "ticketNew"
        "TicketProjectLocal"
      -- 206
    , addUnique "TicketAuthorRemote" $
        Unique "UniqueTicketAuthorRemoteNew" ["ticketNew"]
      -- 207
    , removeUnique "TicketAuthorRemote" "UniqueTicketAuthorRemote"
      -- 208
    , removeField "TicketAuthorRemote" "ticket"
      -- 209
    , renameUnique
        "TicketAuthorRemote"
        "UniqueTicketAuthorRemoteNew"
        "UniqueTicketAuthorRemote"
      -- 210
    , renameField "TicketAuthorRemote" "ticketNew" "ticket"
      -- 211
    , renameField "TicketAuthorLocal" "offer" "open"
      -- 212
    , renameUnique
        "TicketAuthorLocal"
        "UniqueTicketAuthorLocaleOffer"
        "UniqueTicketAuthorLocalOpen"
      -- 213
    , renameField "TicketAuthorRemote" "offer" "open"
      -- 214
    , renameUnique
        "TicketAuthorRemote"
        "UniqueTicketAuthorRemoteOffer"
        "UniqueTicketAuthorRemoteOpen"
      -- 215
    , addFieldRefRequired''
        "RemoteDiscussion"
        (do iid <- insert $ Instance215 $ Authority "215.fake.fake" Nothing
            insertEntity $ RemoteObject215 iid $ LocalURI "/fake/215"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            rdids <- selectList ([] :: [Filter RemoteDiscussion215]) []
            for_ rdids $ \ (Entity rdid rd) -> do
                let iid = remoteDiscussion215Instance rd
                    lu = remoteDiscussion215Ident rd
                roid <- insert $ RemoteObject215 iid lu
                update rdid [RemoteDiscussion215IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject215Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 216
    , addUnique "RemoteDiscussion" $
        Unique "UniqueRemoteDiscussionIdentNew" ["identNew"]
      -- 217
    , removeUnique "RemoteDiscussion" "UniqueRemoteDiscussionIdent"
      -- 218
    , renameUnique "RemoteDiscussion" "UniqueRemoteDiscussionIdentNew" "UniqueRemoteDiscussionIdent"
      -- 219
    , removeField "RemoteDiscussion" "instance"
      -- 220
    , removeField "RemoteDiscussion" "ident"
      -- 221
    , renameField "RemoteDiscussion" "identNew" "ident"
      -- 222
    , addEntities model_2020_02_09
      -- 223
    , unchecked $ lift $ do
        ids <- E.select $ E.from $
            \ (tal `E.InnerJoin` lt `E.InnerJoin` t `E.InnerJoin` tpl) -> do
                E.on $ t E.^. Ticket223Id E.==. tpl E.^. TicketProjectLocal223Ticket
                E.on $ lt E.^. LocalTicket223Ticket E.==. t E.^. Ticket223Id
                E.on $ tal E.^. TicketAuthorLocal223Ticket E.==. lt E.^. LocalTicket223Id
                return (tpl E.^. TicketProjectLocal223Id, tal E.^. TicketAuthorLocal223Id)
        insertMany_ $ map (uncurry TicketUnderProject223 . bimap E.unValue E.unValue) ids
      -- 224
    , addUnique "TicketUnderProject" $
        Unique "UniqueTicketUnderProjectProject" ["project"]
      -- 225
    , addUnique "TicketUnderProject" $
        Unique "UniqueTicketUnderProjectAuthor" ["author"]
      -- 226
    , removeUnique "TicketUnderProject" "UniqueTicketUnderProject"
      -- 227
    , addFieldRefRequired''
        "RemoteMessage"
        (do iid <- insert $ Instance227 $ Authority "227.fake.fake" Nothing
            insertEntity $ RemoteObject227 iid $ LocalURI "/fake/227"
        )
        (Just $ \ (Entity roidTemp roTemp) -> do
            rmids <- selectList ([] :: [Filter RemoteMessage227]) []
            for_ rmids $ \ (Entity rmid rm) -> do
                let iid = remoteMessage227Instance rm
                    lu = remoteMessage227Ident rm
                roid <- insert $ RemoteObject227 iid lu
                update rmid [RemoteMessage227IdentNew =. roid]
            delete roidTemp
            delete $ remoteObject227Instance roTemp
        )
        "identNew"
        "RemoteObject"
      -- 228
    , addUnique "RemoteMessage" $
        Unique "UniqueRemoteMessageIdentNew" ["identNew"]
      -- 229
    , removeUnique "RemoteMessage" "UniqueRemoteMessageIdent"
      -- 230
    , renameUnique
        "RemoteMessage"
        "UniqueRemoteMessageIdentNew"
        "UniqueRemoteMessageIdent"
      -- 231
    , removeField "RemoteMessage" "instance"
      -- 232
    , removeField "RemoteMessage" "ident"
      -- 233
    , renameField "RemoteMessage" "identNew" "ident"
      -- 234
    , addEntities model_2020_02_22
      -- 235
    , addEntities model_2020_04_07
      -- 236
    , addEntities model_2020_04_09
      -- 237
    , addFieldPrimOptional
        "RemoteActor"
        (Nothing :: Maybe Text)
        "followers"
      -- 238
    , addFieldRefRequired''
        "RemoteTicket"
        (do iid <- insert $ Instance238 $ Authority "238.fake.fake" Nothing
            roid <- insert $ RemoteObject238 iid $ LocalURI "/fake/238"
            did <- insert Discussion238
            insertEntity $ RemoteDiscussion238 roid did
        )
        (Just $ \ (Entity rdidTemp rdTemp) -> do
            rtids <- selectList ([] :: [Filter RemoteTicket238]) []
            for_ rtids $ \ (Entity rtid rt) -> do
                let roid = remoteTicket238Ident rt
                mrdid <- getKeyBy $ UniqueRemoteDiscussionIdent238 roid
                rdid <-
                    case mrdid of
                        Nothing -> do
                            did <- insert Discussion238
                            insert $ RemoteDiscussion238 roid did
                        Just rdid -> return rdid
                update rtid [RemoteTicket238Discuss =. rdid]

            delete rdidTemp

            let roidTemp = remoteDiscussion238Ident rdTemp
            roTemp <- getJust roidTemp
            delete roidTemp
            delete $ remoteObject238Instance roTemp

            delete $ remoteDiscussion238Discuss rdTemp
        )
        "discuss"
        "RemoteDiscussion"
      -- 239
    , addUnique "RemoteTicket" $ Unique "UniqueRemoteTicketDiscuss" ["discuss"]
      -- 240
    , addEntities model_2020_05_12
      -- 241
    , unchecked $ lift $ do
        fwds <- selectList ([] :: [Filter Forwarding241]) []
        let makeSender (Entity fwdid fwd) =
                ForwarderProject241 fwdid (forwarding241Sender fwd)
        insertMany_ $ map makeSender fwds
      -- 242
    , removeField "Forwarding" "sender"
      -- 243
    , renameEntity "TicketProjectLocal" "TicketContextLocal"
      -- 244
    , renameUnique "TicketContextLocal" "UniqueTicketProjectLocal" "UniqueTicketContextLocal"
      -- 245
    , renameUnique "TicketContextLocal" "UniqueTicketProjectLocalAccept" "UniqueTicketContextLocalAccept"
      -- 246
    , addEntities model_2020_05_16
      -- 247
    , unchecked $ lift $ do
        tcls <- selectList ([] :: [Filter TicketContextLocal247]) []
        let makeTPL (Entity tclid tcl) =
                TicketProjectLocal247 tclid (ticketContextLocal247Project tcl)
        insertMany_ $ map makeTPL tcls
      -- 248
    , removeField "TicketContextLocal" "project"
      -- 249
    , addEntities model_2020_05_17
      -- 250
    , addFieldPrimRequired "Patch" defaultTime "created"
      -- 251
    , addFieldPrimOptional "TicketRepoLocal" (Nothing :: Maybe Text) "branch"
      -- 252
    , addEntities model_2020_05_25
      -- 253
    , removeField "TicketDependency" "summary"
      -- 254
    , addEntities model_2020_05_28
      -- 255
    , unchecked $ lift $ do
        tds <- selectList ([] :: [Filter TicketDependency255]) []
        for_ tds $ \ (Entity tdid td) -> do
            let pid = ticketDependency255Author td
            p <- getJust pid
            obiid <-
                insert $
                    OutboxItem255
                        (person255Outbox p)
                        (persistJSONObjectFromDoc $ Doc hLocal emptyActivity)
                        (ticketDependency255Created td)
            insert_ $ TicketDependencyAuthorLocal255 tdid pid obiid
      -- 256
    , removeField "TicketDependency" "author"
      -- 257
    , addEntities model_2020_06_01
      -- 258
    , renameEntity "TicketDependency" "LocalTicketDependency"
      -- 259
    , renameUnique
        "LocalTicketDependency"
        "UniqueTicketDependency"
        "UniqueLocalTicketDependency"
      -- 260
    , unchecked $ lift $ do
        tds <- selectList ([] :: [Filter LocalTicketDependency260]) []
        for_ tds $ \ (Entity tdid td) -> do
            let tid = localTicketDependency260Child td
            location <-
                requireEitherAlt
                    (getKeyBy $ UniqueLocalTicket260 tid)
                    (runMaybeT $ do
                        tclid <- MaybeT $ getKeyBy $ UniqueTicketContextLocal260 tid
                        tarid <- MaybeT $ getKeyBy $ UniqueTicketAuthorRemote260 tclid
                        rt <- MaybeT $ getValBy $ UniqueRemoteTicket260 tarid
                        return $ remoteTicket260Ident rt
                    )
                    "Neither LT nor RT"
                    "Both LT and RT"
            case location of
                Left ltid -> insert_ $ TicketDependencyChildLocal260 tdid ltid
                Right roid -> insert_ $ TicketDependencyChildRemote260 tdid roid
      -- 261
    , removeUnique "LocalTicketDependency" "UniqueLocalTicketDependency"
      -- 262
    , removeField "LocalTicketDependency" "child"
      -- 263
    , addFieldRefRequired''
        "LocalTicketDependency"
        (do did <- insert Discussion263
            fsid <- insert FollowerSet263
            tid <- insert $ Ticket263 Nothing defaultTime "" "" "" Nothing "TSNew" defaultTime Nothing
            insertEntity $ LocalTicket263 tid did fsid
        )
        (Just $ \ (Entity ltidTemp ltTemp) -> do
            tdids <- selectList ([] :: [Filter LocalTicketDependency263]) []
            for_ tdids $ \ (Entity tdid td) -> do
                ltid <- do
                    mltid <-
                        getKeyBy $ UniqueLocalTicket263 $
                            localTicketDependency263Parent td
                    case mltid of
                        Nothing -> error "TD with non-local parent"
                        Just v -> return v
                update tdid [LocalTicketDependency263ParentNew =. ltid]

            delete ltidTemp

            delete $ localTicket263Ticket ltTemp
            delete $ localTicket263Discuss ltTemp
            delete $ localTicket263Followers ltTemp
        )
        "parentNew"
        "LocalTicket"
      -- 264
    , removeField "LocalTicketDependency" "parent"
      -- 265
    , renameField "LocalTicketDependency" "parentNew" "parent"
      -- 266
    , addFieldRefRequired''
        "LocalTicketDependency"
        (do obid <- insert Outbox266
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            insertEntity $ OutboxItem266 obid doc defaultTime
        )
        (Just $ \ (Entity obiidTemp obiTemp) -> do
            tdids <- selectList ([] :: [Filter LocalTicketDependency266]) []
            for_ tdids $ \ (Entity tdid td) -> do
                lt <- getJust $ localTicketDependency266Parent td
                mtpl <- runMaybeT $ do
                    tclid <- MaybeT $ getKeyBy $ UniqueTicketContextLocal266 $ localTicket266Ticket lt
                    _ <- MaybeT $ getBy $ UniqueTicketUnderProjectProject266 tclid
                    MaybeT $ getValBy $ UniqueTicketProjectLocal266 tclid
                tpl <-
                    case mtpl of
                        Nothing -> error "No TPL"
                        Just v -> return v
                j <- getJust $ ticketProjectLocal266Project tpl
                let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                obiid <-
                    insert $
                        OutboxItem266
                            (project266Outbox j)
                            doc
                            (localTicketDependency266Created td)
                update tdid [LocalTicketDependency266Accept =. obiid]

            delete obiidTemp
            delete $ outboxItem266Outbox obiTemp
        )
        "accept"
        "OutboxItem"
      -- 267
    , addEntities model_2020_06_18
      -- 268
    , addFieldRefRequiredEmpty
        "RemoteTicketDependency" "accept" "RemoteActivity"
      -- 269
    , addUnique "RemoteTicketDependency" $
        Unique "UniqueRemoteTicketDependencyAccept" ["accept"]
      -- 270
    , unchecked $ lift $ deleteWhere ([] :: [Filter RemoteCollection159])
      -- 271
    , removeUnique "RemoteCollection" "UniqueRemoteCollection"
      -- 272
    , removeField "RemoteCollection" "ident"
      -- 273
    , removeEntity "RemoteCollection"
      -- 274
    , addEntities model_2020_07_23
      -- 275
    , addEntities model_2020_07_27
      -- 276
    , unchecked $ lift $ do
        elts <- selectList ([] :: [Filter LocalTicket276]) []
        for_ elts $ \ (Entity ltid lt) -> do
            let tid = localTicket276Ticket lt
            t <- getJust tid
            for_ (ticket276Closer t) $ \ pid -> do
                let unjust s = fromMaybe $ error s
                obidCloser <- person276Outbox <$> getJust pid
                obidHoster <- do
                    tclid <-
                        unjust "No TCL" <$> getKeyBy (UniqueTicketContextLocal276 tid)
                    tpl <-
                        unjust "No TPL" <$> getValBy (UniqueTicketProjectLocal276 tclid)
                    _ <-
                        unjust "No TUP" <$> getBy (UniqueTicketUnderProjectProject276 tclid)
                    project276Outbox <$>
                        getJust (ticketProjectLocal276Project tpl)
                let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                    closed = ticket276Closed t
                obiidResolve <- insert $ OutboxItem276 obidCloser doc closed
                obiidAccept <- insert $ OutboxItem276 obidHoster doc closed
                trid <- insert $ TicketResolve276 ltid obiidAccept
                insert_ $ TicketResolveLocal276 trid obiidResolve
      -- 277
    , removeField "Ticket" "closed"
      -- 278
    , removeField "Ticket" "closer"
      -- 279
    , addEntities model_2020_08_10
      -- 280
    , addFieldRefRequired''
        "Patch"
        (do tid <- insert $ Ticket280 Nothing defaultTime "" "" "" Nothing "TSNew"
            insertEntity $ Bundle280 tid
        )
        (Just $ \ (Entity bnidTemp bnTemp) -> do
            pts <- selectList ([] :: [Filter Patch280]) []
            for_ pts $ \ (Entity ptid pt) -> do
                bnid <- insert $ Bundle280 $ patch280Ticket pt
                update ptid [Patch280Bundle =. bnid]

            delete bnidTemp
            delete $ bundle280Ticket bnTemp
        )
        "bundle"
        "Bundle"
      -- 281
    , removeField "Patch" "ticket"
      -- 282
    , unchecked $ lift $ do
        ers <- selectList ([] :: [Filter Repo282]) []
        for_ ers $ \ (Entity rid r) -> do
            vcs <-
                case repo282Vcs r of
                    "VCSDarcs" -> return "Darcs"
                    "VCSGit" -> return "Git"
                    _ -> error "Weird repoVcs"
            update rid [Repo282Vcs =. vcs]
      -- 283
    , addFieldPrimRequired "Patch" ("???" :: Text) "type"
      -- 284
    , addEntities model_2022_06_14
      -- 285
    , unchecked $ lift $ do
        rcs <- selectList ([] :: [Filter RepoCollab285]) []
        for_ rcs $ \ (Entity _ (RepoCollab285 rid pid mrlid)) -> do
            cid <- insert Collab285
            for_ mrlid $ \ rlid -> insert_ $ CollabRoleLocal285 cid rlid
            insert_ $ CollabTopicLocalRepo285 cid rid
            obiid <- do
                r <- getJust rid
                insert $
                    OutboxItem285
                        (repo285Outbox r)
                        (persistJSONObjectFromDoc $ Doc hLocal emptyActivity)
                        defaultTime
            insert_ $ CollabSenderLocal285 cid obiid
            insert_ $ CollabRecipLocal285 cid pid

        jcs <- selectList ([] :: [Filter ProjectCollab285]) []
        for_ jcs $ \ (Entity _ (ProjectCollab285 jid pid mrlid)) -> do
            cid <- insert Collab285
            for_ mrlid $ \ rlid -> insert_ $ CollabRoleLocal285 cid rlid
            insert_ $ CollabTopicLocalProject285 cid jid
            obiid <- do
                j <- getJust jid
                insert $
                    OutboxItem285
                        (project285Outbox j)
                        (persistJSONObjectFromDoc $ Doc hLocal emptyActivity)
                        defaultTime
            insert_ $ CollabSenderLocal285 cid obiid
            insert_ $ CollabRecipLocal285 cid pid
      -- 286
    , removeEntity "RepoCollab"
      -- 287
    , removeEntity "ProjectCollab"
      -- 288
    , addEntities model_2022_07_17
      -- 289
    , addFieldRefRequired''
        "Project"
        (do ibid <- insert Inbox289
            obid <- insert Outbox289
            fsid <- insert FollowerSet289
            insertEntity $ Actor289 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            js <- selectList ([] :: [Filter Project289]) []
            for js $ \ (Entity jid j) -> do
                aid <- insert Actor289
                    { actor289Name      = fromMaybe "" $ project289Name j
                    , actor289Desc      = fromMaybe "" $ project289Desc j
                    , actor289CreatedAt = defaultTime
                    , actor289Inbox     = project289Inbox j
                    , actor289Outbox    = project289Outbox j
                    , actor289Followers = project289Followers j
                    }
                update jid [Project289Actor =. aid]

            delete aidTemp
            delete $ actor289Inbox aTemp
            delete $ actor289Outbox aTemp
            delete $ actor289Followers aTemp
        )
        "actor"
        "Actor"
      -- 290
    , addUnique "Project" $ Unique "UniqueProjectActor" ["actor"]
      -- 291
    , removeUnique "Project" "UniqueProjectInbox"
      -- 292
    , removeUnique "Project" "UniqueProjectOutbox"
      -- 293
    , removeUnique "Project" "UniqueProjectFollowers"
      -- 294
    , removeField "Project" "inbox"
      -- 295
    , removeField "Project" "outbox"
      -- 296
    , removeField "Project" "followers"
      -- 297
    , addFieldRefRequired''
        "Project"
        (do obid <- insert Outbox297
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            insertEntity $ OutboxItem297 obid doc defaultTime
        )
        (Just $ \ (Entity obiidTemp obiTemp) -> do
            js <- selectList ([] :: [Filter Project297]) []
            for_ js $ \ (Entity jid j) -> do
                mp <- getValBy $ UniquePersonIdent297 $ project297Sharer j
                p <-
                    case mp of
                        Nothing -> error "Project sharer isn't a Person"
                        Just person -> return person
                let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                obiid <-
                    insert $ OutboxItem297 (person297Outbox p) doc defaultTime
                update jid [Project297Create =. obiid]

            delete obiidTemp
            delete $ outboxItem297Outbox obiTemp
        )
        "create"
        "OutboxItem"
      -- 298
    , addUnique "Project" $ Unique "UniqueProjectCreate" ["create"]
      -- 299
    , addEntities model_2022_07_24
      -- 300
    , unchecked $ lift $ do
        ctsJ <- selectList ([] :: [Filter CollabTopicLocalProject300]) []
        for_ ctsJ $ \ (Entity _ (CollabTopicLocalProject300 cid jid)) -> do
            j <- getJust jid
            mcr <- getValBy $ UniqueCollabRecipLocal300 cid
            for_ mcr $ \ (CollabRecipLocal300 _ pid) -> do
                p <- getJust pid
                when (project300Sharer j == person300Ident p) $
                    insert_ $ CollabFulfillsLocalTopicCreation300 cid
        ctsR <- selectList ([] :: [Filter CollabTopicLocalRepo300]) []
        for_ ctsR $ \ (Entity _ (CollabTopicLocalRepo300 cid rid)) -> do
            r <- getJust rid
            mcr <- getValBy $ UniqueCollabRecipLocal300 cid
            for_ mcr $ \ (CollabRecipLocal300 _ pid) -> do
                p <- getJust pid
                when (repo300Sharer r == person300Ident p) $
                    insert_ $ CollabFulfillsLocalTopicCreation300 cid
      -- 301
    , removeEntity "TicketUnderProject"
      -- 302
    , addFieldPrimRequired "Person" ("???" :: Text) "username"
      -- 303
    , unchecked $ lift $ do
        ps <- selectList ([] :: [Filter Person303]) []
        for_ ps $ \ (Entity pid p) -> do
            s <- getJust $ person303Ident p
            update pid [Person303Username =. sharer303Ident s]
      -- 304
    , addUnique "Person" $ Unique "UniquePersonUsername" ["username"]
      -- 305
    , removeUnique "Person" "UniquePersonIdent"
      -- 306
    , removeField "Person" "ident"
      -- 307
    , removeEntity "TicketProjectRemoteAccept"
      -- 308
    , unchecked $ lift $ do
        tcrs <- selectList ([] :: [Filter TicketProjectRemote308]) []
        for_ tcrs $ \ (Entity tcrid tcr) -> do
            let talid = ticketProjectRemote308Ticket tcr
            tal <- getJust talid
            let ltid = ticketAuthorLocal308Ticket tal
            LocalTicket308 tid dsid fsid <- getJust ltid
            mids <- selectKeysList [Message308Root ==. dsid] []

            delete tcrid
            delete talid
            delete ltid
            delete tid
            deleteWhere [LocalMessage308Rest <-. mids]
            deleteWhere [RemoteMessage308Rest <-. mids]
            deleteWhere [Message308Id <-. mids]
            delete dsid
            deleteWhere [Follow308Target ==. fsid]
            deleteWhere [RemoteFollow308Target ==. fsid]
            delete fsid
      -- 309
    , unchecked $ lift $ do
        rts <- selectList ([] :: [Filter RemoteTicket308]) []
        for_ rts $ \ (Entity rtid rt) -> do
            let tarid = remoteTicket308Ticket rt
                rdsid = remoteTicket308Discuss rt
            tar <- getJust tarid
            let tclid = ticketAuthorRemote308Ticket tar
            tcl <- getJust tclid
            let tid = ticketContextLocal308Ticket tcl
            rds <- getJust rdsid
            let dsid = remoteDiscussion308Discuss rds
            mids <- selectKeysList [Message308Root ==. dsid] []

            delete rtid
            delete tarid
            delete rdsid
            deleteBy $ UniqueTicketProjectLocal308 tclid
            deleteBy $ UniqueTicketRepoLocal308 tclid
            delete tclid
            delete tid
            deleteWhere [LocalMessage308Rest <-. mids]
            deleteWhere [RemoteMessage308Rest <-. mids]
            deleteWhere [Message308Id <-. mids]
            delete dsid
      -- 310
    , addFieldRefRequired''
        "Ticket"
        (insertEntity Discussion310)
        (Just $ \ (Entity dsidTemp _) -> do
            lts <- selectList ([] :: [Filter LocalTicket310]) []
            for_ lts $ \ (Entity _ lt) -> do
                let tid = localTicket310Ticket lt
                update tid [Ticket310Discuss =. localTicket310Discuss lt]

            delete dsidTemp
        )
        "discuss"
        "Discussion"
      -- 311
    , addUnique "Ticket" $ Unique "UniqueTicketDiscuss" ["discuss"]
      -- 312
    , addFieldRefRequired''
        "Ticket"
        (insertEntity FollowerSet312)
        (Just $ \ (Entity fsidTemp _) -> do
            lts <- selectList ([] :: [Filter LocalTicket312]) []
            for_ lts $ \ (Entity _ lt) -> do
                let tid = localTicket312Ticket lt
                update tid [Ticket312Followers =. localTicket312Followers lt]

            delete fsidTemp
        )
        "followers"
        "FollowerSet"
      -- 313
    , addUnique "Ticket" $ Unique "UniqueTicketFollowers" ["followers"]
      -- 314
    , removeEntity "TicketProjectRemote"
      -- 315
    , removeEntity "RemoteTicket"
      -- 316
    , addFieldRefRequired''
        "Ticket"
        (do obid <- insert Outbox316
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            insertEntity $ OutboxItem316 obid doc defaultTime
        )
        (Just $ \ (Entity obiidTemp obiTemp) -> do
            tcls <- selectList ([] :: [Filter TicketContextLocal316]) []
            for_ tcls $ \ (Entity _ (TicketContextLocal316 tid obiid)) ->
                update tid [Ticket316Accept =. obiid]

            delete obiidTemp
            delete $ outboxItem316Outbox obiTemp
        )
        "accept"
        "OutboxItem"
      -- 317
    , addUnique "Ticket" $ Unique "UniqueTicketAccept" ["accept"]
      -- 318
    , addFieldRefRequired''
        "TicketAuthorLocal"
        (do dsid <- insert Discussion318
            fsid <- insert FollowerSet318
            obid <- insert Outbox318
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem318 obid doc defaultTime
            insertEntity $ Ticket318 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            tals <- selectList ([] :: [Filter TicketAuthorLocal318]) []
            for_ tals $ \ (Entity talid tal) -> do
                lt <- getJust $ ticketAuthorLocal318Ticket tal
                update talid [TicketAuthorLocal318TicketNew =. localTicket318Ticket lt]

            delete tidTemp
            delete $ ticket318Discuss tTemp
            delete $ ticket318Followers tTemp
            let obiid = ticket318Accept tTemp
            obid <- outboxItem318Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "ticketNew"
        "Ticket"
      -- 319
    , removeUnique "TicketAuthorLocal" "UniqueTicketAuthorLocal"
      -- 320
    , removeField "TicketAuthorLocal" "ticket"
      -- 321
    , renameField "TicketAuthorLocal" "ticketNew" "ticket"
      -- 322
    , addUnique "TicketAuthorLocal" $ Unique "UniqueTicketAuthorLocal" ["ticket"]
      -- 323
    , addFieldRefRequired''
        "TicketAuthorRemote"
        (do dsid <- insert Discussion323
            fsid <- insert FollowerSet323
            obid <- insert Outbox323
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem323 obid doc defaultTime
            insertEntity $ Ticket323 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            tars <- selectList ([] :: [Filter TicketAuthorRemote323]) []
            for_ tars $ \ (Entity tarid tar) -> do
                tcl <- getJust $ ticketAuthorRemote323Ticket tar
                update tarid [TicketAuthorRemote323TicketNew =. ticketContextLocal323Ticket tcl]

            delete tidTemp
            delete $ ticket323Discuss tTemp
            delete $ ticket323Followers tTemp
            let obiid = ticket323Accept tTemp
            obid <- outboxItem323Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "ticketNew"
        "Ticket"
      -- 324
    , removeUnique "TicketAuthorRemote" "UniqueTicketAuthorRemote"
      -- 325
    , removeField "TicketAuthorRemote" "ticket"
      -- 326
    , renameField "TicketAuthorRemote" "ticketNew" "ticket"
      -- 327
    , addUnique "TicketAuthorRemote" $ Unique "UniqueTicketAuthorRemote" ["ticket"]
      -- 328
    , addFieldRefRequired''
        "TicketProjectLocal"
        (do dsid <- insert Discussion328
            fsid <- insert FollowerSet328
            obid <- insert Outbox328
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem328 obid doc defaultTime
            insertEntity $ Ticket328 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            tjls <- selectList ([] :: [Filter TicketProjectLocal328]) []
            for_ tjls $ \ (Entity tjlid tjl) -> do
                tcl <- getJust $ ticketProjectLocal328Context tjl
                update tjlid [TicketProjectLocal328Ticket =. ticketContextLocal328Ticket tcl]

            delete tidTemp
            delete $ ticket328Discuss tTemp
            delete $ ticket328Followers tTemp
            let obiid = ticket328Accept tTemp
            obid <- outboxItem328Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "ticket"
        "Ticket"
      -- 329
    , removeUnique "TicketProjectLocal" "UniqueTicketProjectLocal"
      -- 330
    , removeField "TicketProjectLocal" "context"
      -- 331
    , addUnique "TicketProjectLocal" $ Unique "UniqueTicketProjectLocal" ["ticket"]
      -- 332
    , addFieldRefRequired''
        "TicketRepoLocal"
        (do dsid <- insert Discussion332
            fsid <- insert FollowerSet332
            obid <- insert Outbox332
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem332 obid doc defaultTime
            insertEntity $ Ticket332 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            trls <- selectList ([] :: [Filter TicketRepoLocal332]) []
            for_ trls $ \ (Entity trlid trl) -> do
                tcl <- getJust $ ticketRepoLocal332Context trl
                update trlid [TicketRepoLocal332Ticket =. ticketContextLocal332Ticket tcl]

            delete tidTemp
            delete $ ticket332Discuss tTemp
            delete $ ticket332Followers tTemp
            let obiid = ticket332Accept tTemp
            obid <- outboxItem332Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "ticket"
        "Ticket"
      -- 333
    , removeUnique "TicketRepoLocal" "UniqueTicketRepoLocal"
      -- 334
    , removeField "TicketRepoLocal" "context"
      -- 335
    , addUnique "TicketRepoLocal" $ Unique "UniqueTicketRepoLocal" ["ticket"]
      -- 336
    , removeEntity "TicketContextLocal"
      -- 337
    , removeEntity "TicketDependencyOffer"
      -- 338
    , addFieldRefRequired''
        "RemoteTicketDependency"
        (do dsid <- insert Discussion338
            fsid <- insert FollowerSet338
            obid <- insert Outbox338
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem338 obid doc defaultTime
            insertEntity $ Ticket338 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            rtds <- selectList ([] :: [Filter RemoteTicketDependency338]) []
            for_ rtds $ \ (Entity rtdid rtd) -> do
                lt <- getJust $ remoteTicketDependency338Child rtd
                update rtdid [RemoteTicketDependency338ChildNew =. localTicket338Ticket lt]

            delete tidTemp
            delete $ ticket338Discuss tTemp
            delete $ ticket338Followers tTemp
            let obiid = ticket338Accept tTemp
            obid <- outboxItem338Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "childNew"
        "Ticket"
      -- 339
    , removeField "RemoteTicketDependency" "child"
      -- 340
    , renameField "RemoteTicketDependency" "childNew" "child"
      -- 341
    , addUnique "LocalTicketDependency" $ Unique "UniqueLocalTicketDependencyAccept" ["accept"]
      -- 342
    , addFieldRefRequired''
        "LocalTicketDependency"
        (do dsid <- insert Discussion342
            fsid <- insert FollowerSet342
            obid <- insert Outbox342
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem342 obid doc defaultTime
            insertEntity $ Ticket342 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            rtds <- selectList ([] :: [Filter LocalTicketDependency342]) []
            for_ rtds $ \ (Entity rtdid rtd) -> do
                lt <- getJust $ localTicketDependency342Parent rtd
                update rtdid [LocalTicketDependency342ParentNew =. localTicket342Ticket lt]

            delete tidTemp
            delete $ ticket342Discuss tTemp
            delete $ ticket342Followers tTemp
            let obiid = ticket342Accept tTemp
            obid <- outboxItem342Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "parentNew"
        "Ticket"
      -- 343
    , removeField "LocalTicketDependency" "parent"
      -- 344
    , renameField "LocalTicketDependency" "parentNew" "parent"
      -- 345
    , addFieldRefRequired''
        "TicketDependencyChildLocal"
        (do dsid <- insert Discussion345
            fsid <- insert FollowerSet345
            obid <- insert Outbox345
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem345 obid doc defaultTime
            insertEntity $ Ticket345 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            tdcls <- selectList ([] :: [Filter TicketDependencyChildLocal345]) []
            for_ tdcls $ \ (Entity tdclid tdcl) -> do
                lt <- getJust $ ticketDependencyChildLocal345Child tdcl
                update tdclid [TicketDependencyChildLocal345ChildNew =. localTicket345Ticket lt]

            delete tidTemp
            delete $ ticket345Discuss tTemp
            delete $ ticket345Followers tTemp
            let obiid = ticket345Accept tTemp
            obid <- outboxItem345Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "childNew"
        "Ticket"
      -- 346
    , removeField "TicketDependencyChildLocal" "child"
      -- 347
    , renameField "TicketDependencyChildLocal" "childNew" "child"
      -- 348
    , addFieldRefRequired''
        "TicketResolve"
        (do dsid <- insert Discussion348
            fsid <- insert FollowerSet348
            obid <- insert Outbox348
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            obiid <- insert $ OutboxItem348 obid doc defaultTime
            insertEntity $ Ticket348 Nothing defaultTime "" "" "" Nothing TSNew dsid fsid obiid
        )
        (Just $ \ (Entity tidTemp tTemp) -> do
            trs <- selectList ([] :: [Filter TicketResolve348]) []
            for_ trs $ \ (Entity trid tr) -> do
                lt <- getJust $ ticketResolve348Ticket tr
                update trid [TicketResolve348TicketNew =. localTicket348Ticket lt]

            delete tidTemp
            delete $ ticket348Discuss tTemp
            delete $ ticket348Followers tTemp
            let obiid = ticket348Accept tTemp
            obid <- outboxItem348Outbox <$> getJust obiid
            delete obiid
            delete obid
        )
        "ticketNew"
        "Ticket"
      -- 349
    , removeUnique "TicketResolve" "UniqueTicketResolve"
      -- 350
    , removeField "TicketResolve" "ticket"
      -- 351
    , renameField "TicketResolve" "ticketNew" "ticket"
      -- 352
    , addUnique "TicketResolve" $ Unique "UniqueTicketResolve" ["ticket"]
      -- 353
    , removeEntity "LocalTicket"
      -- 354
    , renameUnique "TicketProjectLocal" "UniqueTicketProjectLocal" "UniqueTicketDeck"
      -- 355
    , renameEntity "TicketProjectLocal" "TicketDeck"
      -- 356
    , addFieldRefRequired''
        "Person"
        (do ibid <- insert Inbox356
            obid <- insert Outbox356
            fsid <- insert FollowerSet356
            insertEntity $ Actor356 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            ps <- selectList ([] :: [Filter Person356]) []
            for ps $ \ (Entity pid p) -> do
                aid <- insert Actor356
                    { actor356Name      = ""
                    , actor356Desc      = person356About p
                    , actor356CreatedAt = defaultTime
                    , actor356Inbox     = person356Inbox p
                    , actor356Outbox    = person356Outbox p
                    , actor356Followers = person356Followers p
                    }
                update pid [Person356Actor =. aid]

            delete aidTemp
            delete $ actor356Inbox aTemp
            delete $ actor356Outbox aTemp
            delete $ actor356Followers aTemp
        )
        "actor"
        "Actor"
      -- 357
    , removeField "Person" "about"
      -- 358
    , removeUnique "Person" "UniquePersonInbox"
      -- 359
    , removeUnique "Person" "UniquePersonOutbox"
      -- 360
    , removeUnique "Person" "UniquePersonFollowers"
      -- 361
    , addUnique "Person" $ Unique "UniquePersonActor" ["actor"]
      -- 362
    , removeField "Person" "inbox"
      -- 363
    , removeField "Person" "outbox"
      -- 364
    , removeField "Person" "followers"
      -- 365
    , addFieldRefRequired''
        "Group"
        (do ibid <- insert Inbox365
            obid <- insert Outbox365
            fsid <- insert FollowerSet365
            insertEntity $ Actor365 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            gs <- selectList ([] :: [Filter Group365]) []
            for gs $ \ (Entity gid g) -> do
                s <- getJust $ group365Ident g
                ibid <- insert Inbox365
                obid <- insert Outbox365
                fsid <- insert FollowerSet365
                aid <- insert Actor365
                    { actor365Name      = shr2text $ sharer365Ident s
                    , actor365Desc      = ""
                    , actor365CreatedAt = defaultTime
                    , actor365Inbox     = ibid
                    , actor365Outbox    = obid
                    , actor365Followers = fsid
                    }
                update gid [Group365Actor =. aid]

            delete aidTemp
            delete $ actor365Inbox aTemp
            delete $ actor365Outbox aTemp
            delete $ actor365Followers aTemp
        )
        "actor"
        "Actor"
      -- 366
    , addUnique "Group" $ Unique "UniqueGroupActor" ["actor"]
      -- 367
    , addFieldRefRequired''
        "Repo"
        (do ibid <- insert Inbox367
            obid <- insert Outbox367
            fsid <- insert FollowerSet367
            insertEntity $ Actor367 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            rs <- selectList ([] :: [Filter Repo367]) []
            for rs $ \ (Entity rid r) -> do
                aid <- insert Actor367
                    { actor367Name      = rp2text $ repo367Ident r
                    , actor367Desc      = fromMaybe "" $ repo367Desc r
                    , actor367CreatedAt = defaultTime
                    , actor367Inbox     = repo367Inbox r
                    , actor367Outbox    = repo367Outbox r
                    , actor367Followers = repo367Followers r
                    }
                update rid [Repo367Actor =. aid]

            delete aidTemp
            delete $ actor367Inbox aTemp
            delete $ actor367Outbox aTemp
            delete $ actor367Followers aTemp
        )
        "actor"
        "Actor"
      -- 368
    , removeUnique "Repo" "UniqueRepoInbox"
      -- 369
    , removeUnique "Repo" "UniqueRepoOutbox"
      -- 370
    , removeUnique "Repo" "UniqueRepoFollowers"
      -- 371
    , addUnique "Repo" $ Unique "UniqueRepoActor" ["actor"]
      -- 372
    , removeField "Repo" "inbox"
      -- 373
    , removeField "Repo" "outbox"
      -- 374
    , removeField "Repo" "followers"
      -- 375
    , removeUnique "Repo" "UniqueRepo"
      -- 376
    , removeField "Repo" "ident"
      -- 377
    , renameUnique "Project" "UniqueProjectActor" "UniqueDeckActor"
      -- 378
    , renameUnique "Project" "UniqueProjectCreate" "UniqueDeckCreate"
      -- 379
    , removeUnique "Project" "UniqueProject"
      -- 380
    , removeField "Project" "ident"
      -- 381
    , renameEntity "Project" "Deck"
      -- 382
    , removeField "Deck" "name"
      -- 383
    , removeField "Deck" "desc"
      -- 384
    , addEntities model_384_loom
      -- 385
    , removeField "Repo" "desc"
      -- 386
    , addEntities model_386_assignee
      -- 387
    , removeField "Ticket" "assignee"
      -- 388
    , unchecked $ lift $ do
        trls <- selectList ([] :: [Filter TicketRepoLocal388]) []
        for_ trls $ \ (Entity trlid (TicketRepoLocal388 tid _ _)) -> do
            Ticket388 _ _ _ _ _ _ dsid fsid _ <- getJust tid
            mids <- selectKeysList [Message388Root ==. dsid] []
            bnids <- selectKeysList [Bundle388Ticket ==. tid] []
            mtr <- getBy $ UniqueTicketResolve388 tid

            delete trlid
            deleteWhere [LocalMessage388Rest <-. mids]
            deleteWhere [RemoteMessage388Rest <-. mids]
            deleteWhere [Message388Id <-. mids]
            delete dsid
            deleteWhere [Follow388Target ==. fsid]
            deleteWhere [RemoteFollow388Target ==. fsid]
            delete fsid
            deleteBy $ UniqueTicketAuthorLocal388 tid
            deleteBy $ UniqueTicketAuthorRemote388 tid
            for_ mtr $ \ (Entity trid _) -> do
                deleteBy $ UniqueTicketResolveLocal388 trid
                deleteBy $ UniqueTicketResolveRemote388 trid
                delete trid
            deleteWhere [Patch388Bundle <-. bnids]
            deleteWhere [Bundle388Id <-. bnids]
            delete tid
      -- 389
    , addFieldRefRequiredEmpty "TicketRepoLocal" "loom" "Loom"
      -- 390
    , renameUnique "TicketRepoLocal" "UniqueTicketRepoLocal" "UniqueTicketLoom"
      -- 391
    , renameEntity "TicketRepoLocal" "TicketLoom"
      -- 392
    , removeField "TicketLoom" "repo"
      -- 393
    , removeField "Bundle" "ticket"
      -- 394
    , addFieldRefRequiredEmpty "Bundle" "ticket" "TicketLoom"
      -- 395
    , removeField "Deck" "sharer"
      -- 396
    , unchecked $ lift $ do
        rs <- selectList [] [Asc Repo396Id]
        oldSharerDirs <- fmap (LO.nubSort . catMaybes) $ for rs $ \ (Entity rid r) -> do
            root <- asksSite $ appRepoDir . appSettings
            parent <- sharer396Ident <$> getJust (repo396Sharer r)
            dir <- actor396Name <$> getJust (repo396Actor r)
            let oldSharer =
                    root
                        </> (T.unpack $ CI.foldedCase $ unShrIdent parent)
                old =
                    oldSharer
                        </> (T.unpack $ CI.foldCase dir)
                new =
                    root
                        </> (T.unpack $ keyHashidText $
                            encodeKeyHashidPure ctx rid)
                            --E.toSqlKey $ E.fromSqlKey rid)
            liftIO $ do
                oldExists <- doesDirectoryExist old
                newExists <- doesDirectoryExist new
                case (oldExists, newExists) of
                    (True, False) -> do
                        renameDirectory old new
                        return $ Just oldSharer
                    (True, True) -> error "Both old and new dirs exist"
                    (False, False) -> error "Repo dir missing"
                    (False, True) -> do
                        oldSharerExists <- doesDirectoryExist oldSharer
                        when oldSharerExists $ removeDirectory oldSharer
                        pure Nothing
        liftIO $ for_ oldSharerDirs removeDirectory
      -- 397
    , removeField "Repo" "sharer"
      -- 398
    , renameField "TicketDeck" "project" "deck"
      -- 399
    , addEntities model_399_fwder
      -- 400
    , removeEntity "ForwarderSharer"
      -- 401
    , renameEntity "ForwarderProject" "ForwarderDeck"
      -- 402
    , renameUnique "ForwarderDeck" "UniqueForwarderProject" "UniqueForwarderDeck"
      -- 403
    , removeUnique "Role" "UniqueRole"
      -- 404
    , removeField "Role" "sharer"
      -- 405
    , renameEntity "CollabTopicLocalProject" "CollabTopicLocalDeck"
      -- 406
    , renameField "CollabTopicLocalDeck" "project" "deck"
      -- 407
    , renameUnique "CollabTopicLocalDeck" "UniqueCollabTopicLocalProject" "UniqueCollabTopicLocalDeck"
      -- 408
    , addEntities model_408_collab_loom
      -- 409
    , addFieldRefRequired''
        "Repo"
        (do obid <- insert Outbox409
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            insertEntity $ OutboxItem409 obid doc defaultTime
        )
        (Just $ \ (Entity obiidTemp obiTemp) -> do
            let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
            rs <- selectKeysList ([] :: [Filter Repo409]) []
            for_ rs $ \ rid -> do
                obid <- do
                    mp <- selectFirst [] [Asc Person409Id]
                    p <- entityVal <$> maybe (error "No people") return mp
                    a <- getJust $ person409Actor p
                    return $ actor409Outbox a
                obiid <- insert $ OutboxItem409 obid doc defaultTime
                update rid [Repo409Create =. obiid]

            delete obiidTemp
            delete $ outboxItem409Outbox obiTemp
        )
        "create"
        "OutboxItem"
      -- 410
    , addUnique' "Repo" "Create" ["create"]
      -- 411
    , removeUnique' "Group" ""
      -- 412
    , removeField "Group" "ident"
      -- 413
    , removeUnique' "Workflow" ""
      -- 414
    , addFieldRefRequired''
        "FollowRemote"
        (do ibid <- insert Inbox414
            obid <- insert Outbox414
            fsid <- insert FollowerSet414
            insertEntity $ Actor414 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            frs <- selectList ([] :: [Filter FollowRemote414]) []
            for frs $ \ (Entity frid fr) -> do
                aid <- person414Actor <$> getJust (followRemote414Person fr)
                update frid [FollowRemote414Actor =. aid]

            delete aidTemp
            delete $ actor414Inbox aTemp
            delete $ actor414Outbox aTemp
            delete $ actor414Followers aTemp
        )
        "actor"
        "Actor"
      -- 415
    , removeUnique' "FollowRemote" ""
      -- 416
    , addUnique' "FollowRemote" "" ["actor", "target"]
      -- 417
    , removeField "FollowRemote" "person"
      -- 418
    , addFieldRefRequired''
        "Follow"
        (do ibid <- insert Inbox418
            obid <- insert Outbox418
            fsid <- insert FollowerSet418
            insertEntity $ Actor418 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            fs <- selectList ([] :: [Filter Follow418]) []
            for fs $ \ (Entity fid f) -> do
                aid <- person418Actor <$> getJust (follow418Person f)
                update fid [Follow418Actor =. aid]

            delete aidTemp
            delete $ actor418Inbox aTemp
            delete $ actor418Outbox aTemp
            delete $ actor418Followers aTemp
        )
        "actor"
        "Actor"
      -- 419
    , removeUnique' "Follow" ""
      -- 420
    , addUnique' "Follow" "" ["actor", "target"]
      -- 421
    , removeField "Follow" "person"
      -- 422
    , removeField "Workflow" "sharer"
      -- 423
    , removeEntity "Sharer"
      -- 424
    , removeEntity "CollabRoleLocal"
      -- 425
    , addEntities model_425_collab_accept
      -- 426
    , unchecked $ lift $ do
        repos <- selectList [] [Asc CollabTopicLocalRepo426Id]
        for_ repos $ \ (Entity _ (CollabTopicLocalRepo426 collabID repoID)) -> do
            repo <- getJust repoID
            itemID <- do
                mgrant <- runMaybeT $ do
                    CollabSenderLocal426 _ grantID <-
                        MaybeT $ maybeRight <$>
                        requireEitherAlt
                            (getBy $ UniqueCollabSenderRemote426 collabID)
                            (getValBy $ UniqueCollabSenderLocal426 collabID)
                            "No CollabSender*"
                            "Multiple CollabSender*"
                    OutboxItem426 outboxID _ _ <- lift $ getJust $ grantID
                    actorID <- do
                        mactor <- lift $ getKeyBy $ UniqueActorOutbox426 outboxID
                        case mactor of
                            Just a -> return a
                            Nothing -> error "Outbox with no actor"
                    guard $ repo426Actor repo == actorID
                    return grantID
                case mgrant of
                    Just grantID -> return grantID
                    Nothing -> do
                        actor <- getJust $ repo426Actor repo
                        let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                        insert $ OutboxItem426 (actor426Outbox actor) doc defaultTime
            insert_ $ CollabTopicAccept426 collabID itemID

        decks <- selectList [] [Asc CollabTopicLocalDeck426Id]
        for_ decks $ \ (Entity _ (CollabTopicLocalDeck426 collabID deckID)) -> do
            deck <- getJust deckID
            itemID <- do
                mgrant <- runMaybeT $ do
                    CollabSenderLocal426 _ grantID <-
                        MaybeT $ maybeRight <$>
                        requireEitherAlt
                            (getBy $ UniqueCollabSenderRemote426 collabID)
                            (getValBy $ UniqueCollabSenderLocal426 collabID)
                            "No CollabSender*"
                            "Multiple CollabSender*"
                    OutboxItem426 outboxID _ _ <- lift $ getJust $ grantID
                    actorID <- do
                        mactor <- lift $ getKeyBy $ UniqueActorOutbox426 outboxID
                        case mactor of
                            Just a -> return a
                            Nothing -> error "Outbox with no actor"
                    guard $ deck426Actor deck == actorID
                    return grantID
                case mgrant of
                    Just grantID -> return grantID
                    Nothing -> do
                        actor <- getJust $ deck426Actor deck
                        let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                        insert $ OutboxItem426 (actor426Outbox actor) doc defaultTime
            insert_ $ CollabTopicAccept426 collabID itemID

        looms <- selectList [] [Asc CollabTopicLocalLoom426Id]
        for_ looms $ \ (Entity _ (CollabTopicLocalLoom426 collabID loomID)) -> do
            loom <- getJust loomID
            itemID <- do
                mgrant <- runMaybeT $ do
                    CollabSenderLocal426 _ grantID <-
                        MaybeT $ maybeRight <$>
                        requireEitherAlt
                            (getBy $ UniqueCollabSenderRemote426 collabID)
                            (getValBy $ UniqueCollabSenderLocal426 collabID)
                            "No CollabSender*"
                            "Multiple CollabSender*"
                    OutboxItem426 outboxID _ _ <- lift $ getJust $ grantID
                    actorID <- do
                        mactor <- lift $ getKeyBy $ UniqueActorOutbox426 outboxID
                        case mactor of
                            Just a -> return a
                            Nothing -> error "Outbox with no actor"
                    guard $ loom426Actor loom == actorID
                    return grantID
                case mgrant of
                    Just grantID -> return grantID
                    Nothing -> do
                        actor <- getJust $ loom426Actor loom
                        let doc = persistJSONObjectFromDoc $ Doc hLocal emptyActivity
                        insert $ OutboxItem426 (actor426Outbox actor) doc defaultTime
            insert_ $ CollabTopicAccept426 collabID itemID
      -- 427
    , addFieldRefRequiredEmpty "CollabTopicRemote" "actor" "RemoteActor"
      -- 428
    , addEntities model_428_collab_topic_local
      -- 429
    , addFieldRefRequired''
        "CollabTopicLocalRepo"
        (do collabID <- insert Collab429
            insertEntity $ CollabTopicLocal429 collabID
        )
        (Just $ \ (Entity topicTemp (CollabTopicLocal429 collabTemp)) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                localID <-
                    insert $ CollabTopicLocal429 $ collabTopicLocalRepo429Collab topic
                update topicID [CollabTopicLocalRepo429CollabNew =. localID]

            delete topicTemp
            delete collabTemp
        )
        "collabNew"
        "CollabTopicLocal"
      -- 430
    , addFieldRefRequired''
        "CollabTopicLocalDeck"
        (do collabID <- insert Collab430
            insertEntity $ CollabTopicLocal430 collabID
        )
        (Just $ \ (Entity topicTemp (CollabTopicLocal430 collabTemp)) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                localID <-
                    insert $ CollabTopicLocal430 $ collabTopicLocalDeck430Collab topic
                update topicID [CollabTopicLocalDeck430CollabNew =. localID]

            delete topicTemp
            delete collabTemp
        )
        "collabNew"
        "CollabTopicLocal"
      -- 431
    , addFieldRefRequired''
        "CollabTopicLocalLoom"
        (do collabID <- insert Collab431
            insertEntity $ CollabTopicLocal431 collabID
        )
        (Just $ \ (Entity topicTemp (CollabTopicLocal431 collabTemp)) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                localID <-
                    insert $ CollabTopicLocal431 $ collabTopicLocalLoom431Collab topic
                update topicID [CollabTopicLocalLoom431CollabNew =. localID]

            delete topicTemp
            delete collabTemp
        )
        "collabNew"
        "CollabTopicLocal"
      -- 432
    , removeUnique' "CollabTopicLocalRepo" ""
      -- 433
    , addUnique' "CollabTopicLocalRepo" "" ["collabNew"]
      -- 434
    , removeUnique' "CollabTopicLocalDeck" ""
      -- 435
    , addUnique' "CollabTopicLocalDeck" "" ["collabNew"]
      -- 436
    , removeUnique' "CollabTopicLocalLoom" ""
      -- 437
    , addUnique' "CollabTopicLocalLoom" "" ["collabNew"]
      -- 438
    , removeField "CollabTopicLocalRepo" "collab"
      -- 439
    , renameField "CollabTopicLocalRepo" "collabNew" "collab"
      -- 440
    , removeField "CollabTopicLocalDeck" "collab"
      -- 441
    , renameField "CollabTopicLocalDeck" "collabNew" "collab"
      -- 442
    , removeField "CollabTopicLocalLoom" "collab"
      -- 443
    , renameField "CollabTopicLocalLoom" "collabNew" "collab"
      -- 444
    , removeUnique' "CollabTopicAccept" "Collab"
      -- 445
    , renameUnique "CollabTopicAccept" "UniqueCollabTopicAcceptAccept" "UniqueCollabTopicLocalAcceptAccept"
      -- 446
    , renameEntity "CollabTopicAccept" "CollabTopicLocalAccept"
      -- 447
    , addFieldRefRequired''
        "CollabTopicLocalAccept"
        (do collabID <- insert Collab447
            insertEntity $ CollabTopicLocal447 collabID
        )
        (Just $ \ (Entity topicTemp (CollabTopicLocal447 collabTemp)) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                maybeLocal <- getKeyBy $ UniqueCollabTopicLocal447 $ collabTopicLocalAccept447Collab topic
                localID <-
                    case maybeLocal of
                        Nothing -> error "No CollabTopicLocal for this Collab"
                        Just l -> return l
                update topicID [CollabTopicLocalAccept447CollabNew =. localID]

            delete topicTemp
            delete collabTemp
        )
        "collabNew"
        "CollabTopicLocal"
      -- 448
    , addUnique' "CollabTopicLocalAccept" "Collab" ["collabNew"]
      -- 449
    , removeField "CollabTopicLocalAccept" "collab"
      -- 450
    , renameField "CollabTopicLocalAccept" "collabNew" "collab"
      -- 451
    , addEntities model_451_collab_remote_accept
      -- 452
    , addFieldPrimRequired "InboxItem" defaultTime "received"
      -- 453
    , addEntities model_453_collab_receive
      -- 454
    , renameUnique "CollabSenderLocal" "UniqueCollabSenderLocal" "UniqueCollabFulfillsInviteLocal"
      -- 455
    , renameUnique "CollabSenderLocal" "UniqueCollabSenderLocalActivity" "UniqueCollabFulfillsInviteLocalInvite"
      -- 456
    , renameField "CollabSenderLocal" "activity" "invite"
      -- 457
    , renameUnique "CollabSenderRemote" "UniqueCollabSenderRemote" "UniqueCollabFulfillsInviteRemote"
      -- 458
    , renameUnique "CollabSenderRemote" "UniqueCollabSenderRemoteActivity" "UniqueCollabFulfillsInviteRemoteInvite"
      -- 459
    , renameField "CollabSenderRemote" "activity" "invite"
      -- 460
    , renameEntity "CollabSenderLocal" "CollabFulfillsInviteLocal"
      -- 461
    , renameEntity "CollabSenderRemote" "CollabFulfillsInviteRemote"
      -- 462
    , removeEntity "CollabRecipLocalReceive"
      -- 463
    , removeEntity "CollabTopicRemoteAccept"
      -- 464
    , removeEntity "CollabTopicRemote"
      -- 465
    , removeEntity "CollabTopicLocalReceive"
      -- 466
    , addFieldRefRequired''
        "CollabTopicLocalRepo"
        (insertEntity Collab466)
        (Just $ \ (Entity collabTemp _) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                CollabTopicLocal466 collabID <-
                    getJust $ collabTopicLocalRepo466Collab topic
                update topicID [CollabTopicLocalRepo466CollabNew =. collabID]

            delete collabTemp
        )
        "collabNew"
        "Collab"
      -- 467
    , addFieldRefRequired''
        "CollabTopicLocalDeck"
        (insertEntity Collab467)
        (Just $ \ (Entity collabTemp _) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                CollabTopicLocal467 collabID <-
                    getJust $ collabTopicLocalDeck467Collab topic
                update topicID [CollabTopicLocalDeck467CollabNew =. collabID]

            delete collabTemp
        )
        "collabNew"
        "Collab"
      -- 468
    , addFieldRefRequired''
        "CollabTopicLocalLoom"
        (insertEntity Collab468)
        (Just $ \ (Entity collabTemp _) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                CollabTopicLocal468 collabID <-
                    getJust $ collabTopicLocalLoom468Collab topic
                update topicID [CollabTopicLocalLoom468CollabNew =. collabID]

            delete collabTemp
        )
        "collabNew"
        "Collab"
      -- 469
    , removeUnique' "CollabTopicLocalRepo" ""
      -- 470
    , renameEntity "CollabTopicLocalRepo" "CollabTopicRepo"
      -- 471
    , removeUnique' "CollabTopicLocalDeck" ""
      -- 472
    , renameEntity "CollabTopicLocalDeck" "CollabTopicDeck"
      -- 473
    , removeUnique' "CollabTopicLocalLoom" ""
      -- 474
    , renameEntity "CollabTopicLocalLoom" "CollabTopicLoom"
      -- 475
    , addUnique' "CollabTopicRepo" "" ["collabNew"]
      -- 476
    , addUnique' "CollabTopicDeck" "" ["collabNew"]
      -- 477
    , addUnique' "CollabTopicLoom" "" ["collabNew"]
      -- 478
    , removeField "CollabTopicRepo" "collab"
      -- 479
    , renameField "CollabTopicRepo" "collabNew" "collab"
      -- 480
    , removeField "CollabTopicDeck" "collab"
      -- 481
    , renameField "CollabTopicDeck" "collabNew" "collab"
      -- 482
    , removeField "CollabTopicLoom" "collab"
      -- 483
    , renameField "CollabTopicLoom" "collabNew" "collab"
      -- 484
    , renameEntity "CollabTopicLocalAccept" "CollabEnable"
      -- 485
    , renameField "CollabEnable" "accept" "grant"
      -- 486
    , addFieldRefRequired''
        "CollabEnable"
        (insertEntity Collab486)
        (Just $ \ (Entity collabTemp _) -> do
            collabs <- selectList [] []
            for_ collabs $ \ (Entity topicID topic) -> do
                CollabTopicLocal486 collabID <-
                    getJust $ collabEnable486Collab topic
                update topicID [CollabEnable486CollabNew =. collabID]

            delete collabTemp
        )
        "collabNew"
        "Collab"
      -- 487
    , removeUnique "CollabEnable" "UniqueCollabTopicLocalAcceptCollab"
      -- 488
    , addUnique' "CollabEnable" "" ["collabNew"]
      -- 489
    , removeField "CollabEnable" "collab"
      -- 490
    , renameField "CollabEnable" "collabNew" "collab"
      -- 491
    , renameUnique "CollabEnable" "UniqueCollabTopicLocalAcceptAccept" "UniqueCollabEnableGrant"
      -- 492
    , removeEntity "CollabTopicLocal"
      -- 493
    , addFieldRefOptional "Repo" Nothing "loom" "Loom"
      -- 494
    , addEntities model_494_mr_origin
      -- 495
    , unchecked $ lift $ do
        tickets <- selectList [] []
        for_ tickets $ \ (Entity ticketID ticket) -> do
            let plain =
                    TL.toStrict . TLB.toLazyText . HED.htmlEncodedText $
                        ticket495Title ticket
            update ticketID [Ticket495Title =. plain]
      -- 496
    , addFieldPrimRequired "Bundle" False "auto"
      -- 497
    , addEntities model_497_sigkey
      -- 498
    , addFieldRefRequired''
        "Forwarding"
        (do ibid <- insert Inbox498
            obid <- insert Outbox498
            fsid <- insert FollowerSet498
            insertEntity $ Actor498 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            fs <- selectKeysList ([] :: [Filter Forwarding498]) []
            for_ fs $ \ forwardingID -> do
                actorIDs <-
                    sequenceA $ map runMaybeT
                        [do fp <- MaybeT $ getValBy $ UniqueForwarderPerson498 forwardingID
                            lift $ person498Actor <$> getJust (forwarderPerson498Sender fp)
                        ,do fg <- MaybeT $ getValBy $ UniqueForwarderGroup498 forwardingID
                            lift $ group498Actor <$> getJust (forwarderGroup498Sender fg)
                        ,do fr <- MaybeT $ getValBy $ UniqueForwarderRepo498 forwardingID
                            lift $ repo498Actor <$> getJust (forwarderRepo498Sender fr)
                        ,do fd <- MaybeT $ getValBy $ UniqueForwarderDeck498 forwardingID
                            lift $ deck498Actor <$> getJust (forwarderDeck498Sender fd)
                        ,do fl <- MaybeT $ getValBy $ UniqueForwarderLoom498 forwardingID
                            lift $ loom498Actor <$> getJust (forwarderLoom498Sender fl)
                        ]
                actorID <-
                    case catMaybes actorIDs of
                        [] -> error "No Forwarder* found!"
                        [a] -> return a
                        _ -> error "Multiple Forwarder* found!"
                update forwardingID [Forwarding498Forwarder =. actorID]

            delete aidTemp
            delete $ actor498Inbox aTemp
            delete $ actor498Outbox aTemp
            delete $ actor498Followers aTemp
        )
        "forwarder"
        "Actor"
      -- 499
    , removeEntity "ForwarderPerson"
      -- 500
    , removeEntity "ForwarderGroup"
      -- 501
    , removeEntity "ForwarderRepo"
      -- 502
    , removeEntity "ForwarderDeck"
      -- 503
    , removeEntity "ForwarderLoom"
      -- 504
    , addFieldRefRequired''
        "LocalMessage"
        (do ibid <- insert Inbox504
            obid <- insert Outbox504
            fsid <- insert FollowerSet504
            insertEntity $ Actor504 "" "" defaultTime ibid obid fsid
        )
        (Just $ \ (Entity aidTemp aTemp) -> do
            ms <- selectList ([] :: [Filter LocalMessage504]) []
            for_ ms $ \ (Entity lmid lm) -> do
                person <- getJust $ localMessage504Author lm
                update lmid [LocalMessage504AuthorNew =. person504Actor person]

            delete aidTemp
            delete $ actor504Inbox aTemp
            delete $ actor504Outbox aTemp
            delete $ actor504Followers aTemp
        )
        "authorNew"
        "Actor"
      -- 505
    , removeField "LocalMessage" "author"
      -- 506
    , renameField "LocalMessage" "authorNew" "author"
      -- 507
    , unchecked $ lift $ do
        mw <- selectFirst ([] :: [Filter Workflow507]) []
        when (isNothing mw) $
            insert_ $
                Workflow507 (text2wfl "dummy507") Nothing Nothing WSPublic
      -- 508
    , addEntities model_508_invite
      -- 509
    , renameEntity "CollabFulfillsInviteLocal" "CollabInviterLocal"
      -- 510
    , renameUnique
        "CollabInviterLocal"
        "UniqueCollabFulfillsInviteLocal"
        "UniqueCollabInviterLocal"
      -- 511
    , renameUnique
        "CollabInviterLocal"
        "UniqueCollabFulfillsInviteLocalInvite"
        "UniqueCollabInviterLocalInvite"
      -- 512
    , renameEntity "CollabFulfillsInviteRemote" "CollabInviterRemote"
      -- 513
    , renameUnique
        "CollabInviterRemote"
        "UniqueCollabFulfillsInviteRemote"
        "UniqueCollabInviterRemote"
      -- 514
    , renameUnique
        "CollabInviterRemote"
        "UniqueCollabFulfillsInviteRemoteInvite"
        "UniqueCollabInviterRemoteInvite"
      -- 515
    , addFieldRefRequired''
        "CollabInviterLocal"
        (do cid <- insert Collab515
            insertEntity $ CollabFulfillsInvite515 cid
        )
        (Just $ \ (Entity cfiidTemp cfiTemp) -> do
            cs <- selectList ([] :: [Filter CollabInviterLocal515]) []
            for_ cs $ \ (Entity inviterID inviter) -> do
                let collabID = collabInviterLocal515Collab inviter
                fulfillsID <- insert $ CollabFulfillsInvite515 collabID
                update inviterID [CollabInviterLocal515CollabNew =. fulfillsID]
            delete cfiidTemp
            delete $ collabFulfillsInvite515Collab cfiTemp
        )
        "collabNew"
        "CollabFulfillsInvite"
      -- 516
    , removeUnique "CollabInviterLocal" "UniqueCollabInviterLocal"
      -- 517
    , removeField "CollabInviterLocal" "collab"
      -- 518
    , renameField "CollabInviterLocal" "collabNew" "collab"
      -- 519
    , addUnique' "CollabInviterLocal" "" ["collab"]
      -- 520
    , addFieldRefRequired''
        "CollabInviterRemote"
        (do cid <- insert Collab520
            insertEntity $ CollabFulfillsInvite520 cid
        )
        (Just $ \ (Entity cfiidTemp cfiTemp) -> do
            cs <- selectList ([] :: [Filter CollabInviterRemote520]) []
            for_ cs $ \ (Entity inviterID inviter) -> do
                let collabID = collabInviterRemote520Collab inviter
                fulfillsID <- insert $ CollabFulfillsInvite520 collabID
                update inviterID [CollabInviterRemote520CollabNew =. fulfillsID]
            delete cfiidTemp
            delete $ collabFulfillsInvite520Collab cfiTemp
        )
        "collabNew"
        "CollabFulfillsInvite"
      -- 521
    , removeUnique "CollabInviterRemote" "UniqueCollabInviterRemote"
      -- 522
    , removeField "CollabInviterRemote" "collab"
      -- 523
    , renameField "CollabInviterRemote" "collabNew" "collab"
      -- 524
    , addUnique' "CollabInviterRemote" "" ["collab"]
      -- 525
    , addFieldRefRequired''
        "CollabRecipLocalAccept"
        (do cid <- insert Collab525
            insertEntity $ CollabFulfillsInvite525 cid
        )
        (Just $ \ (Entity cfiidTemp cfiTemp) -> do
            cs <- selectList ([] :: [Filter CollabRecipLocalAccept525]) []
            for_ cs $ \ (Entity crlaID crla) -> do
                crl <- getJust $ collabRecipLocalAccept525Collab crla
                let cid = collabRecipLocal525Collab crl
                cfiID <- do
                    mcfi <- getBy $ UniqueCollabFulfillsInvite525 cid
                    case mcfi of
                        Nothing -> error "No FulfillsInvite for RecipAccept"
                        Just ent -> pure $ entityKey ent
                update crlaID [CollabRecipLocalAccept525Invite =. cfiID]

            delete cfiidTemp
            delete $ collabFulfillsInvite525Collab cfiTemp
        )
        "invite"
        "CollabFulfillsInvite"
      -- 526
    , addUnique' "CollabRecipLocalAccept" "Invite" ["invite"]
      -- 527
    , addFieldRefRequired''
        "CollabRecipRemoteAccept"
        (do cid <- insert Collab527
            insertEntity $ CollabFulfillsInvite527 cid
        )
        (Just $ \ (Entity cfiidTemp cfiTemp) -> do
            cs <- selectList ([] :: [Filter CollabRecipRemoteAccept527]) []
            for_ cs $ \ (Entity crlaID crla) -> do
                crl <- getJust $ collabRecipRemoteAccept527Collab crla
                let cid = collabRecipRemote527Collab crl
                cfiID <- do
                    mcfi <- getBy $ UniqueCollabFulfillsInvite527 cid
                    case mcfi of
                        Nothing -> error "No FulfillsInvite for RecipAccept"
                        Just ent -> pure $ entityKey ent
                update crlaID [CollabRecipRemoteAccept527Invite =. cfiID]

            delete cfiidTemp
            delete $ collabFulfillsInvite527Collab cfiTemp
        )
        "invite"
        "CollabFulfillsInvite"
      -- 528
    , addUnique' "CollabRecipRemoteAccept" "Invite" ["invite"]
      -- 529
    , removeField "Ticket" "status"
      -- 530
    , addEntities model_530_join
    ]

migrateDB
    :: (MonadSite m, SiteEnv m ~ App)
    => Host -> HashidsContext -> ReaderT SqlBackend m (Either Text (Int, Int))
migrateDB hLocal ctx = runExceptT $ do
    ExceptT $ flip runReaderT (schemaBackend, "") $ runExceptT $ do
        foreigns <- lift findMisnamedForeigns
        unless (null foreigns) $
            throwE $ T.intercalate " ; " (map displayMisnamedForeign foreigns)

    let migrations = changes hLocal ctx
    (,length migrations) <$>
        ExceptT (runMigrations schemaBackend "" 1 migrations)
