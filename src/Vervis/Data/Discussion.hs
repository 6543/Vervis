{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2020, 2022, 2023
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Data.Discussion
    ( CommentTopic (..)
    , commentTopicAudience
    , commentTopicManagingActor
    , Comment (..)
    , parseNewLocalCommentOld
    , parseRemoteCommentOld
    , parseRemoteComment
    , messageRoute
    )
where

import Control.Monad.Trans.Except
import Data.Bitraversable
import Data.Text (Text)
import Data.Time.Clock

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Web.Text
import Yesod.ActivityPub
import Yesod.Actor
import Yesod.FedURI
import Yesod.MonadSite

import qualified Web.ActivityPub as AP
import qualified Yesod.Hashids as YH

import Control.Monad.Trans.Except.Local

import Vervis.Actor
import Vervis.Actor2
import Vervis.Data.Actor
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient

parseCommentIdOld
    :: ( MonadSite m
       , SiteEnv m ~ site
       , YH.YesodHashids site
       , SiteFedURIMode site ~ URIMode
       )
    => Route App
    -> ExceptT Text m (LocalActorBy Key, LocalMessageId)
parseCommentIdOld (PersonMessageR p m) =
    (,) <$> (LocalActorPerson <$> YH.decodeKeyHashidE p "Invalid actor keyhashid")
        <*> YH.decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentIdOld (GroupMessageR g m) =
    (,) <$> (LocalActorGroup <$> YH.decodeKeyHashidE g "Invalid actor keyhashid")
        <*> YH.decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentIdOld (RepoMessageR r m) =
    (,) <$> (LocalActorRepo <$> YH.decodeKeyHashidE r "Invalid actor keyhashid")
        <*> YH.decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentIdOld (DeckMessageR d m) =
    (,) <$> (LocalActorDeck <$> YH.decodeKeyHashidE d "Invalid actor keyhashid")
        <*> YH.decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentIdOld (LoomMessageR l m) =
    (,) <$> (LocalActorLoom <$> YH.decodeKeyHashidE l "Invalid actor keyhashid")
        <*> YH.decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentIdOld _ = throwE "Not a message route"

parseCommentId :: Route App -> ActE (LocalActorBy Key, LocalMessageId)
parseCommentId (PersonMessageR p m) =
    (,) <$> (LocalActorPerson <$> decodeKeyHashidE p "Invalid actor keyhashid")
        <*> decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentId (GroupMessageR g m) =
    (,) <$> (LocalActorGroup <$> decodeKeyHashidE g "Invalid actor keyhashid")
        <*> decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentId (RepoMessageR r m) =
    (,) <$> (LocalActorRepo <$> decodeKeyHashidE r "Invalid actor keyhashid")
        <*> decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentId (DeckMessageR d m) =
    (,) <$> (LocalActorDeck <$> decodeKeyHashidE d "Invalid actor keyhashid")
        <*> decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentId (LoomMessageR l m) =
    (,) <$> (LocalActorLoom <$> decodeKeyHashidE l "Invalid actor keyhashid")
        <*> decodeKeyHashidE m "Invalid LocalMessage keyhashid"
parseCommentId _ = throwE "Not a message route"

data CommentTopic
    = CommentTopicTicket DeckId TicketDeckId
    | CommentTopicCloth LoomId TicketLoomId

commentTopicAudience :: CommentTopic -> (LocalActorBy Key, LocalStageBy Key)
commentTopicAudience (CommentTopicTicket deckID taskID) =
    (LocalActorDeck deckID, LocalStageTicketFollowers deckID taskID)
commentTopicAudience (CommentTopicCloth loomID clothID) =
    (LocalActorLoom loomID, LocalStageClothFollowers loomID clothID)

commentTopicManagingActor :: CommentTopic -> LocalActorBy Key
commentTopicManagingActor = fst . commentTopicAudience

parseCommentTopicOld
    :: (MonadSite m, YH.YesodHashids (SiteEnv m))
    => Route App
    -> ExceptT Text m CommentTopic
parseCommentTopicOld (TicketR dkhid ltkhid) =
    CommentTopicTicket
        <$> YH.decodeKeyHashidE dkhid "Invalid dkhid"
        <*> YH.decodeKeyHashidE ltkhid "Invalid ltkhid"
parseCommentTopicOld (ClothR lkhid ltkhid) =
    CommentTopicCloth
        <$> YH.decodeKeyHashidE lkhid "Invalid lkhid"
        <*> YH.decodeKeyHashidE ltkhid "Invalid ltkhid"
parseCommentTopicOld _ = throwE "Not a ticket/cloth route"

parseCommentTopic
    :: (MonadActor m, StageHashids (ActorEnv m))
    => Route App
    -> ExceptT Text m CommentTopic
parseCommentTopic (TicketR dkhid ltkhid) =
    CommentTopicTicket
        <$> decodeKeyHashidE dkhid "Invalid dkhid"
        <*> decodeKeyHashidE ltkhid "Invalid ltkhid"
parseCommentTopic (ClothR lkhid ltkhid) =
    CommentTopicCloth
        <$> decodeKeyHashidE lkhid "Invalid lkhid"
        <*> decodeKeyHashidE ltkhid "Invalid ltkhid"
parseCommentTopic _ = throwE "Not a ticket/cloth route"

data Comment = Comment
    { commentParent  :: Maybe (Either (LocalActorBy Key, LocalMessageId) FedURI)
    , commentTopic   :: Either CommentTopic FedURI
    , commentSource  :: PandocMarkdown
    , commentContent :: HTML
    }

parseCommentOld
    :: ( MonadSite m
       , SiteEnv m ~ site
       , YH.YesodHashids site
       , YesodActivityPub site
       , SiteFedURIMode site ~ URIMode
       )
    => AP.Note URIMode
    -> ExceptT Text m (Maybe LocalURI, LocalURI, Maybe UTCTime, Comment)
parseCommentOld (AP.Note mluNote luAttrib _aud muParent muContext mpublished source content) = do
    uContext <- fromMaybeE muContext "Note without context"
    topic <- bitraverse parseCommentTopicOld pure =<< parseFedURIOld uContext
    maybeParent <- do
        uParent <- fromMaybeE muParent "Note doesn't specify inReplyTo"
        if uParent == uContext
            then pure Nothing
            else fmap Just . bitraverse parseCommentIdOld pure =<< parseFedURIOld uParent
    return (mluNote, luAttrib, mpublished, Comment maybeParent topic source content)

parseComment
    :: AP.Note URIMode
    -> ActE (Maybe LocalURI, LocalURI, Maybe UTCTime, Comment)
parseComment (AP.Note mluNote luAttrib _aud muParent muContext mpublished source content) = do
    uContext <- fromMaybeE muContext "Note without context"
    topic <- bitraverse parseCommentTopic pure =<< parseFedURI uContext
    maybeParent <- do
        uParent <- fromMaybeE muParent "Note doesn't specify inReplyTo"
        if uParent == uContext
            then pure Nothing
            else fmap Just . bitraverse parseCommentId pure =<< parseFedURI uParent
    return (mluNote, luAttrib, mpublished, Comment maybeParent topic source content)

parseNewLocalCommentOld
    :: AP.Note URIMode -> ExceptT Text Handler (PersonId, Comment)
parseNewLocalCommentOld note = do
    (mluId, luAuthor, maybePublished, comment) <- parseCommentOld note
    verifyNothingE mluId "Note specifies an id"
    authorPersonID <- do
        authorByKey <-
            nameExceptT "Note author" $
                parseLocalActorE =<< parseLocalURI luAuthor
        case authorByKey of
            LocalActorPerson p -> pure p
            _ -> throwE "Author isn't a Person actor"
    verifyNothingE maybePublished "Note specifies published"
    return (authorPersonID, comment)

parseRemoteCommentOld
    :: ( MonadSite m
       , SiteEnv m ~ site
       , YH.YesodHashids site
       , YesodActivityPub site
       , SiteFedURIMode site ~ URIMode
       )
    => AP.Note URIMode
    -> ExceptT Text m (LocalURI, LocalURI, UTCTime, Comment)
parseRemoteCommentOld note = do
    (mluId, luAuthor, maybePublished, comment) <- parseCommentOld note
    luId <- fromMaybeE mluId "Note doesn't specify id"
    published <- fromMaybeE maybePublished "Note doesn't specify published"
    return (luId, luAuthor, published, comment)

parseRemoteComment
    :: AP.Note URIMode
    -> ExceptT Text Act (LocalURI, LocalURI, UTCTime, Comment)
parseRemoteComment note = do
    (mluId, luAuthor, maybePublished, comment) <- parseComment note
    luId <- fromMaybeE mluId "Note doesn't specify id"
    published <- fromMaybeE maybePublished "Note doesn't specify published"
    return (luId, luAuthor, published, comment)

messageRoute :: LocalActorBy KeyHashid -> KeyHashid LocalMessage -> Route App
messageRoute (LocalActorPerson p) = PersonMessageR p
messageRoute (LocalActorGroup g)  = GroupMessageR g
messageRoute (LocalActorRepo r)   = RepoMessageR r
messageRoute (LocalActorDeck d)   = DeckMessageR d
messageRoute (LocalActorLoom l)   = LoomMessageR l
