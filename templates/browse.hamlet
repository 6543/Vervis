$# This file is part of Vervis.
$#
$# Written in 2016, 2018, 2019, 2022 by fr33domlover <fr33domlover@riseup.net>.
$#
$# ♡ Copying is an act of love. Please copy, reuse and share.
$#
$# The author(s) have dedicated all copyright and related and neighboring
$# rights to this software to the public domain worldwide. This software is
$# distributed without any warranty.
$#
$# You should have received a copy of the CC0 Public Domain Dedication along
$# with this software. If not, see
$# <http://creativecommons.org/publicdomain/zero/1.0/>.

<p>
  Vervis is a work-in-progress federated project and repository hosting and
  collaboration platform. Its development is meant to help form the ForgeFed
  specification for project hosting platform federation, and hopefully get
  existing platforms (such as Gitea, Gogs, GitLab CE, etc.) to support it.

<p>
  Vervis is being used for its own development:
  <a href="https://vervis.peers.community/repos/WvWbo">
    Vervis project page

<p>
  Vervis is a <a href="https://peers.community">Peers community</a> project.

<p>
  Vervis is a web application written in the Haskell programming language and
  the Yesod web framework. It's free as in freedom, under AGPLv3. It's being
  developed by fr33domlover, who can be found under this nickname on
  <a href="https://libera.chat">
    Libera Chat
  in the #peers channel.

<p>
  Vervis currently supports Git and Darcs repositories.

<ul>
  <li>
    Vervis web app
    <a href="https://vervis.peers.community/repos/WvWbo">
      source code
    (it's a <a href="http://darcs.net">Darcs</a> repository)
  <li>
    IRC:
    <a href="https://web.libera.chat/#peers">
      #peers @ Libera Chat
  <li>
    Matrix:
    <a href="https://matrix.to/#/#peers:libera.chat">
      #peers:libera.chat
  <li>
    ForgeFed
    <a href="https://forgefed.org">website
    and
    <a href="https://socialhub.activitypub.rocks/c/software/forgefed">forum

<h2>People

<ul>
  $forall (Entity personID person, Entity _ actor) <- people
    <li>
      <a href=@{PersonR $ hashPerson personID}>
        ~#{username2text $ personUsername person} #{actorName actor}

<h2>Teams

<ul>
  $forall (Entity groupID _, Entity _ actor) <- groups
    <li>
      <a href=@{GroupR $ hashGroup groupID}>
        &#{keyHashidText $ hashGroup groupID} #{actorName actor}

<h2>Repos

<ul>
  $forall (Entity repoID _, Entity _ actor) <- repos
    <li>
      <a href=@{RepoR $ hashRepo repoID}>
        ^#{keyHashidText $ hashRepo repoID} #{actorName actor}

<h2>Ticket Trackers

<ul>
  $forall (Entity deckID _, Entity _ actor) <- decks
    <li>
      <a href=@{DeckR $ hashDeck deckID}>
        =#{keyHashidText $ hashDeck deckID} #{actorName actor}

<h2>Merge Request Trackers

<ul>
  $forall (Entity loomID _, Entity _ actor) <- looms
    <li>
      <a href=@{LoomR $ hashLoom loomID}>
        +#{keyHashidText $ hashLoom loomID} #{actorName actor}
