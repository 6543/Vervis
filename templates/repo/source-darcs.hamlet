$# This file is part of Vervis.
$#
$# Written in 2016, 2018, 2019, 2020, 2022
$# by fr33domlover <fr33domlover@riseup.net>.
$#
$# ♡ Copying is an act of love. Please copy, reuse and share.
$#
$# The author(s) have dedicated all copyright and related and neighboring
$# rights to this software to the public domain worldwide. This software is
$# distributed without any warranty.
$#
$# You should have received a copy of the CC0 Public Domain Dedication along
$# with this software. If not, see
$# <http://creativecommons.org/publicdomain/zero/1.0/>.

$# $maybe (s, j, w, sw) <- mproject
$#   <p>
$#     Belongs to project
$#     <a href=@{ProjectR (sharerIdent s) (projectIdent j)}>
$#       $maybe name <- projectName j
$#         #{name}
$#       $nothing
$#         #{prj2text $ projectIdent j}
$#
$#   ^{personNavW $ sharerIdent s}
$#
$#   ^{projectNavW j w sw (sharerIdent s) (projectIdent j)}

<p>#{actorDesc actor}

$# ^{personNavW user}

<div>
  <span>
    [[ 🗃
    <a href=@{RepoR repo}>
      ^#{keyHashidText repo} #{actorName actor}
    ]] ::
  <span>
    <a href=@{RepoInboxR repo}>
      [📥 Inbox]
  <span>
    <a href=@{RepoOutboxR repo}>
      [📤 Outbox]
  <span>
    <a href=@{RepoFollowersR repo}>
      [🐤 Followers]
  <span>
      [🤝 Collaborators]
  <span>
    <a href=@{RepoCommitsR repo}>
      [🛠 Changes]
  $maybe loomID <- repoLoom repository
    <span>
      <a href=@{LoomClothsR $ hashLoom loomID}>
        [🧩 Patches]

^{followButton}

$if not $ null looms
  <h2>Enable patch tracking
  <ul>
    $forall (loomID, actor) <- looms
      <li>
        Loom
        <a href=@{LoomR $ hashLoom loomID}>
          +#{keyHashidText $ hashLoom loomID} #{actorName actor}
        wants to link with this repo
        ^{buttonW POST "Link" $ RepoLinkR repo $ hashLoom loomID}

<h2>Clone

<p>
  HTTPS:
  <code>darcs clone @{RepoR repo}
<p>
  SSH:
  <code>darcs clone USERNAME@#{renderAuthority host}:#{keyHashidText repo}

<h2>Tags

<p>TODO

<div>
  $forall (piece, piecePath) <- dirs
    <a href=@{RepoSourceR repo piecePath}>#{piece}
    / #

$case sv
  $of SourceFile (FileView name body)
    <h2>#{name}
    ^{body}
  $of SourceDir (DirectoryView mname ents mreadme)
    <h2>#{fromMaybe "Files" mname}
    $if null ents
      <p>No files!
    $else
      <table>
        $forall DirEntry type' name <- ents
          <tr>
            <td>
              $case type'
                $of TypeBlob
                  🗎
                $of TypeTree
                  🗀
            <td>
              <a href=@{RepoSourceR repo (dir ++ [name])}>
                #{name}
    $maybe (readmeName, readmeWidget) <- mreadme
      <h2>#{readmeName}
      ^{readmeWidget}

<div>
  ^{buttonW POST "Delete this repo" (RepoDeleteR repo)}

<div>
  <a href=@?{(RepoR repo, [("prettyjson","true")])}>
    [See repo JSON]
